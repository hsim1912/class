#include <iostream>

using namespace std;
int main()
{
	//Variable declaration

	int degree1, degree2, min1, min2, sec1, sec2;
	int sumDegree, sumMin, sumSec;

	// Request input of angles

	cout << "Enter first angle (deg min sec): ";
	cin >> degree1;
	cin >> min1;
	cin >>sec1;
	cout << "Emter second angle (deg min sec): ";
	cin >> degree2;
	cin >> min2;
	cin >> sec2;

	// Calculate sums

	sumDegree = degree1 + degree2;
	sumMin = min1 + min2;
	sumSec = sec1 + sec2;

	// Begin conversion to higher units, if needed

	if (sumSec > 60)
	{
		sumSec = sumSec % 60;
		sumMin = sumMin++;
	}
	
	if (sumSec == 60)
	{
		sumSec = 0;
		sumMin = sumMin++;
	}
	
	if (sumMin > 60)
	{
		sumMin = sumMin % 60;
		sumDegree = sumDegree++;
	}

	if (sumMin == 60)
	{
		sumMin = 0;
		sumDegree = sumDegree++;
	}

	// Display summed angles.

	cout << degree1 << unsigned char (248);
	cout << min1 << "'";
	cout << sec1 << "''";
	cout << "+ ";
	cout << degree2 << unsigned char (248);
	cout << min2 << "'";
	cout << sec2 << "''";
	cout << "=" << sumDegree << unsigned char (248);
	cout << sumMin << "'"; 
	cout << sumSec << "''";
	
	return 0;
}