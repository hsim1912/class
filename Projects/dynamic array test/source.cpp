#include <iostream>
#include <iomanip>
#include <cctype>
#include <fstream>


using namespace std;

#ifndef  __FUNCTION__H__
#define __FUNCTION__H__
typedef unsigned char pixel;

struct image
{
    int rows;
    int cols;
    pixel **redgray;	// handles red channel or grayscale
    pixel **green;
    pixel **blue;
};


#endif

int main()
{
	int **ptr = NULL;
	int row = 20;
	int col = 20;
	int i = 0;
	int j = 0;
	char choice = 'a';

	image.rows = 20;
	if (ptr == NULL)
	{
		cout << "error at dimension 1" << endl;
		exit (2);
	}
	for (i ; i< row; i++)
	{
		ptr[i] = new (nothrow) int [col];
	}
	i = 0;

	for (i; i< row; i++)
	{

		for (j; j<col; j++)
		{
			ptr[i][j] = rand()%50;
			
		}
		j = 0;

	}

	while (choice != 'q')
	{
	cout << "select row to check: " << endl;
	cin >> i;
	cout << "select column to check: " << endl;
	cin >> j;

	cout << ptr[i][j];

	cout << "\nrun again?";
	
	cin >> choice;
	
	}

	for (i = 0; i< row; i ++)
	{
		delete ptr[i];
	}
	delete ptr;


	return 0;
}