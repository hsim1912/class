This book is a wonderful read, especially for anyone interested in doing
either professional or volunteer home care. The clarity, simplicity of 
style, and evident empathy for the complexities of both giving and 
receiving care distinguish it among a host of literature focused on AIDS. 
Each story is a little gem, and a testimony to the need for and 
satisfactions of visiting the sick.

This collection of ten stories chronicles the distraction of human love 
in its many guises. These stories especially focus on the complexity 
of the relationship between parents and their adult children. 
The need to connect with others seems threatened at times by how 
little understanding we truly have of one another. Although the 
characters in these stories seemed overwhelmed by loneliness, shame
and regret, compassion often provides them some comfort.
