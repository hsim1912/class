/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the basic information for mylist.cpp
 * and prog2.cpp.  It should be included with mylist.cpp and prog2.cpp.
 ****************************************************************************/

#include "mylist.h"



/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function uses a count provided by main to determine
 * if usage has been set correctly.  Should the count of arguments passed be 
 * any number other than three (program name, dictionary, target to spellcheck)
 * it can be assumed that the user has provided incorrect arguments, so a usage
 * menu will be presented.
 *
 * @param[in] (value) int count: a simple integer representing the number of
 * arguments from command prompt.
 *
 * @calls[out] - usage_menu() will be called if it has been determined that the
 * number of arguments is incorrect.  This should assist the user with running
 * the program.
 *
 * @returns - True: if the number of arguments is 3.
 * @returns - False: if the number of arguments is not 3.
 *
 ******************************************************************************/


bool argcheck(int count,char *dic,char *read)
{
	char ext1[25], ext2[25];
	string dictionary = dic;
	string read_file = read;
	if (count != 3)	// usage check
	{
		usage_menu();
		return false;
	}
	string_grab( dictionary, 5, ext1);
	string_grab( read_file, 5, ext2);
	dictionary.resize(dictionary.size());
	read_file.resize(dictionary.size());
	if (dictionary == ".txt" && read_file == ".txt")
	{
		return true;
	}
	usage_menu();
	return false;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function will close the files readme, readme_new, 
 * and dictionary.  While freeing the nodes, the dictionary file will be 
 * written out with the current words in every node, so the new dictionary will
 * be in alphabetical order.
 *
 * @param[in] (reference) ifstream readme: Original input that was spell 
 * checked.  This will be closed out of use here.
 * @param[in] (reference) ofstream readme_new: Modified file after spell
 * checking.  This too will be closed here.
 * @param[in] (reference) fstream dictionary: will be closed and re-opened
 * for writing out the new words as well as originals read thru from 
 * initiation.
 * @param[in] (value) mylist wordlist[]: used to retreive words from the arrays
 * of nodes and pull words directly for write out and deletion.
 * @param[in] (value) string dic: Used so the output dictionary after being
 * closed can be re-opened and then written out to.
 *
 * @calls[out] - mylist::retrieve for grabing specific words from the nodes
 * @calls[out] - mylist::remove for deletion after writting out the words from
 * the nodes.
 *
 * @returns - none.
 *
 ******************************************************************************/
void close(ifstream &readme, ofstream &readme_new, fstream &dictionary, 
	mylist wordlist[], string dic)
{
	int i;
	string word;
	readme.close();
	readme_new.close();
	dictionary.close();
	dictionary.open(dic, ios::out | ios::trunc);
		for (i = 0; i < 26; i ++)
		{
			while ( wordlist[i].retrieve(word, 0))
			{
				dictionary << word << endl;
				wordlist[i].remove(word);
			}

		}
	dictionary.close();
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function walks through each index in the array of
 * the wordlist and has the class provide a number for how many individual 
 * nodes exist within each section.
 *
 * @param[in] (value) wordlist[]: used to check count of nodes per index
 * (first letter of the word in the alphabet)
 *
 * @calls[out] - mylist::getcount used to calculate number of nodes in each
 * array.
 *
 * @returns - none.
 *
 ******************************************************************************/
void count( mylist wordlist[])
{
	int count = 0;
	int i;
	for (i = 0; i< 26; i++)
	{
		count += wordlist[i].get_count();
	}

	cout << count << " Records read-in" << endl;
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Validates the argument passed through main and uses it to
 * open a file for dictionary.
 *
 * @param[in] (reference) string arg: used for the argument containing the name
 * of the dictionary file.
 * @param[in/out] (reference) fstream dictionary: used to open the dictionary
 * into the name dictionary and return it back to main for further 
 * manipulation.
 *
 * @calls[out] - none.
 *
 * @returns - True if dictionary opens correctly.
 * @returns - False should dictionary not open. And prompts a message.
 *
 ******************************************************************************/
bool open_dict(string &arg, fstream &dictionary)
{
	char open[20];
	strcpy(open, arg.c_str());
	dictionary.open( open, ios::in | ios:: out | ios::app );
	if (!dictionary)
	{
		cout << "Dictionary not found or not openable.  Please validate and" 
			<< "try again." << endl;
		return false;
	}	
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * 
 *
 * @param[in] :
 *
 * @calls[out] -
 *
 * @returns - 
 *
 ******************************************************************************/
bool open_readme(string &read, ifstream &readme, ofstream &readme_new)
{
	char open[20];
	int size = read.size();
	strcpy(open, read.c_str());
	readme.open(open, ios::in);
	if (!readme)
	{
		cout << "Error opening readme. Please check disk space and access." 
			<< endl;
		return false;
	}
	read.erase (size - 4);
	read.append (".new");
	strcpy(open, read.c_str());
	readme_new.open( open, ios::out | ios::trunc);
	if (!readme_new)
	{
		cout << "Error opening new readme. Please check disk space and access."
			<< endl;
		return false;
	}
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function reads the words from the specified 
 * dictionary into an array of nodes aranges by the first letter of the word.
 * before reading the word in, the words will be converted to lowercase.
 *
 * @param[in] (reference) fstream dict: the file used to hold all of the words
 * to be used as a dictionary for spell checking.
 * @param[in] (value) wordlist[]: used for cataloging and inserting words
 * into nodes for later use of the dictionary.
 *
 * @calls[out] - mylist::insert - used to create nodes for dictionary words.
 *
 * @returns - none.
 *
 ******************************************************************************/
void read_dictionary(fstream &dict, mylist wordlist[])
{
	int index;
	string word;
	char first_letter[1];
	char punct;
	while (!dict.eof())
	{
		dict >> word;
		word.copy(first_letter, 1);
		first_letter[0] = tolower(first_letter[0]);
		word.replace (0, 1, first_letter, 1);
		index = int (first_letter[0]) - int ('a');
		
		wordlist[index].insert(word);
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function will read in every word from the original
 * readme file, and will save punctuation for later use should it exist.  The
 * words read in will be compaired to the words in the dictionary file, and
 * should the word not be in the dictionary, this function will prompt the user
 * to see if the spelling is correct.  If not the function will receive new 
 * spelling.  In either case the new word will be added into the dictionary, 
 * and the new readme will be written out.
 *
 * @param[in] (reference) ifstream readme: used for initial file to be spell
 * checked.
 * @param[in] (reference) ofstream readme_new: used for the write out after
 * spell checking has been completed on each word.
 * @param[in] (reference) fstream dict: dictionary file used for spell checking
 * of old file.  Will be added to with any new words found.
 * @param[in] (value) mylist wordlist[]: used as indexed search for inserting
 * and finding specific words.
 *
 * @calls[out] - mylist::insert - for adding new words into dictionary.
 * @calls[out] - mylist::find - for finding if the word currently exists in
 * the dictionary.
 *
 * @returns - none.
 *
 ******************************************************************************/
void spell_check (ifstream &readme, ofstream &readme_new, fstream &dict, mylist wordlist[])
{
	string word, word_copy, word_new;
	int index;
	int new_words=0;
	char punct, choice, first_letter[1], cstr_word[20], upper;
	while (!readme.eof())
	{
	readme >> word;
	word_copy = word;
	word_new = word_copy;
	word.copy(first_letter, 1);
	first_letter[0] = tolower(first_letter[0]);
	word.replace (0, 1, first_letter, 1);
	index = int (first_letter[0]) - int ('a');
	if (ispunct(word.c_str()[word.size() -1]))
	{
		punct = word_copy.c_str()[word_copy.size() -1];
		word.resize (word.size() - 1);
		word_copy = word;
		word_new = word_copy;
	}
	else punct = 'a';
		if (!wordlist[index].find(word))
	{
		cout << "Word not found in Dictionary.  Is " << word <<
			" spelled correctly? (y/n)" << endl;
		cin >> choice;
		new_words++;
		choice = tolower(choice);
		if (choice == 'y')
			wordlist[index].insert(word);
		else 
		{
				cout << "What is the correct spelling of " << word << "?"<< endl;
				cin >> word_new;
				wordlist[index].insert(word_new);
		}
	}
	word_new.resize(word_new.size());
	if (ispunct((int) punct))
	{
		readme_new << word_new << punct << " ";	
	}
	else
	{
		readme_new << word_new << " ";	
	}
	}
	cout << new_words << " New words written." << endl;
	return;
}
void string_grab(string &word, int number, char word_arr[])
{
	int i;
	for (i = 1; i < number; i++)
	{
		word_arr[i] = word.c_str()[word.size() -i];
	}
	while (i < number)
	{
		i++;
	}
	word_arr[i] = '\0';
	word_arr = strrev(word_arr);
	word = word_arr;
	word.resize(number -1);
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Outputs correct usage information to the user.  
 *
 * @param[in] : none.
 *
 * @calls[out] - none.
 *
 * @returns - none.
 *
 ******************************************************************************/
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\... prog2.exe [dictionary file] [readme file]" << endl << endl;
	cout << "Both files must be in txt extensions" << endl;
	return;
}