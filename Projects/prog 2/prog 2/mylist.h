/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the basic information for function.cpp, mylist.cpp
 * and prog2.cpp.  It should be included with function.cpp, mylist.cpp
 * and prog2.cpp.
 ****************************************************************************/

#include <string>
#include <cctype>
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;
/*******************************************************************************
 *          Constant Variables, defines, structures, classes and Enums
 ******************************************************************************/



class mylist
{
	public:		// consult with Schrader as to why {} is needed on constructor
				// Destructors
		mylist()
		{
			headptr = NULL;	// set to null for empty list
		}
		~mylist()
		{
		
		}
		bool insert(string &word);
		bool remove(string word);
		bool find( string word );
		bool retrieve( string &word, int loc );
		bool is_empty();
		int get_count();

	private:

		struct node
		{
			string word;
			node *next;
		};
		node *headptr;
};

/*******************************************************************************
 *                         Function Prototypes
 ******************************************************************************/



bool argcheck(int count,char *dic,char *read);
void close(ifstream &readme, ofstream &readme_new, fstream &dictionary, 
	mylist wordlist[], string dic);
void count( mylist wordlist[]);
bool open_dict(string &arg, fstream &dictionary);
bool open_readme(string &read, ifstream &readme, ofstream &readme_new);
void print(mylist wordlist[]);
void read_dictionary(fstream &dict, mylist wordlist[]);
void spell_check (ifstream &readme, ofstream &readme_new, fstream &dict, 
	mylist wordlist[]);
void string_grab(string &word, int number, char word_arr[]);
void usage_menu();