/*************************************************************************//**
 * @file 
 *
 * @brief This file contains the main function which will take comands from
 * the comand line and interpret them as arguments for spell checking of a
 * text file.
 *
 * @mainpage Program 2 - Text-file Spell Checker
 * 
 * @section course_section CSC 250
 *
 * @authors Alex Wulff (part I) & Andrew Rossow (part II)
 * 
 * @date February 29th, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program will use a specified text dictionary to test for
 * correct spelling against a file to be corrected.  The file cross checked
 * with the spelling from the dictionary will be re-written to a file with the
 * same base name, but with a '.new' extension.  Should any word found in the
 * file to be checked be not in the dictionary, the word will be presented to
 * the user for confirmation of the spelling.  The word can here be corrected
 * manually.  Should the spelling be correct or not, the word will hence forth
 * be added to the dictionary, to reduce further prompting for the user.
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog2.exe dictionary.txt file_to_be_spell_checked.txt 
   d:\> c:\bin\Prog2.exe 
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug spell_check function has difficulty with numbers being in the list to
 * spell check.
 * 
 * @todo Fix spell_check function to handle integer interpretation
 * 
 * @par Modifications and Development Timeline: 
 * @verbatim 
 * Date          Modification 
 * ------------  -------------------------------------------------------------- 
 *
 *	2/27/12		- Worked on class calls and initiation.
 *
 *	3/1/12		- Checked and validated command line arguments.
 *
 *	3/5/12		- Created class insert function.
 *
 *	3/5/12		- Worked on additional class funcitons.
 *
 *	3/7/12		- Finalized class functions and supplimental functions.
 *
 *  3/8/12		- Created documentation for this program.
 *
 * @endverbatim
 *
 *****************************************************************************/



#include "mylist.h"


/**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This is the starting point to the program.  It will take commands from the 
 * command promt or an IDE's debug functions.  The program will take these for
 * specification of what dictionary file to use and which file to spell check.
 * The program will go on to read in the dictionary into a series of nodes in
 * alphabetical order.  Once complete, the program will check these words 
 * against the words found in the file being spell checked, and if needed will
 * prompt the user to confirm spelling and add any new words to the dictionary
 * upon completion, while removing nodes from memory.
 *
 * @param[in] argc - the number of arguments from the command prompt.
 * @param[in] argv - a 2d array of characters containing the arguments.
 *
 * @returns 0 - upon completion of spell check as successful.
 * @returns 1 - if the arguments are invalid.
 * @returns 2 - dictionary file can not be opened.
 * @returns 3 - file to spell check and the new file fail to be opened.
 *
 *****************************************************************************/

int main(int argc, char **argv)
{
	mylist wordlist[26];
	string dic = argv[1];
	string read = argv[2];
	fstream dict;			// dictionary file
	ifstream readme;
	ofstream readme_new;
	if (!argcheck(argc, argv[1], argv[2]))	// verify arguments
		exit (1);
	if (!open_dict(dic, dict))	//verify opening of dictionary
		exit (2);
	if (!open_readme(read, readme, readme_new)) // verify readme opens
		exit (3);
	read_dictionary(dict, wordlist);	// read in dictionary
	
	count( wordlist);		// count number of entries in dictionary

	spell_check (readme, readme_new, dict, wordlist);	//do spellchecking

	close(readme, readme_new, dict, wordlist, dic);

	return 0;
}