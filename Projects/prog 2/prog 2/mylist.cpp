/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the basic information for function.cpp and 
 * prog2.cpp.  It should be included with function.cpp and prog2.cpp.
 ****************************************************************************/

#include "mylist.h"

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function of class Mylist inserts a specific word into
 * a node, at location determined by it's place in the alphabet.
 *
 * @param[in] (reference) string word: a word passed in to be of the "word"
 * section of the node.
 *
 * @calls[out] - none.
 *
 * @returns - True if placement is possible.
 * @returns - False if new word can not be placed in to a node.
 *
 ******************************************************************************/
bool mylist::insert(string &word)
{
	node *temp;
	temp = new (nothrow) mylist::node;
	node *current;
	current = new (nothrow) mylist::node;
	node *previous;
	previous = new (nothrow) mylist::node;
	current = headptr;
	previous = headptr;
	temp->word = word;
	if ( mylist::is_empty() )
	{
		temp->next = NULL;
		headptr = temp;
		return true;
	}
	while( current->next != NULL)
	{
		previous = current;
		current = current->next;
		if (current->word.compare(temp->word) >=0)
		{
			previous->next = temp;
			temp->next = current;
			return true;
		}
	}
	current -> next = temp;
	temp -> next = NULL;
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function deletes nodes as specified by the word 
 * section of the node.  In the deletion, this function ensures that the nodes
 * remain intact as a list by forwarding the next section of the node to the
 * node following the one to be deleted.
 *
 * @param[in] (value) string word : used to determine which node needs to be
 * deleted.
 *
 * @calls[out] - none.
 *
 * @returns - True if deletion is successful.
 * @returns - False if deletion is not possible.
 *
 ******************************************************************************/
bool mylist::remove(string word)
{
	node *current = new (nothrow) mylist::node;
	node *previous = new (nothrow) mylist::node;
	current = headptr;
	previous = headptr;

			while (current != NULL && current->word != word)
	{
		previous = current;
		current = current ->next;
	}
	if (current == NULL)
	return false;
	if (current->word == word)
	{
		previous = current->next;
		headptr = previous;
		delete current;
	}
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function of class mylist is used to search nodes
 * for the word in the "word" section of the node and return true if the word
 * is found, and false if not.
 *
 * @param[in] (value) string word: Specific word for comparison of nodes in 
 * linked-list.
 *
 * @calls[out] - none.
 *
 * @returns - True if the word passed in matches a particular "word" section
 * of node.
 * @returns - False if the word passed in matches no node's "word" section.
 *
 ******************************************************************************/
bool mylist::find( string word )
{
	node *temp = new (nothrow) mylist::node;
	temp = headptr;
	while( temp != NULL && word.compare(temp->word) >=0)
	{
		if (temp->word.compare(word) == 0)
			return true;
		else temp = temp->next;
	}
	return false;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function of class mylist accepts an empty word and an
 * integer for loctation that will be moved into this temporarily empty word
 * return it back to the function that called it.
 *
 * @param[in] (reference) string word: used for capture of word at specific 
 * location and will be returned to whatever called this function.
 * @param[in] (value) int loc: used as a specific marker for a point at which
 * to find a word in the list of nodes.
 *
 * @calls[out] - none.
 *
 * @returns - True if word at specified location is found and can be returned.
 * @returns - False if the word is not found.
 *
 ******************************************************************************/
bool mylist::retrieve( string &word, int loc )
{
	node *temp = new (nothrow) mylist::node;
	temp = headptr;
	int count = 0;
	while ( temp != NULL && count != loc )
	{
		temp = temp->next;
		count ++;
	}
	if (temp == NULL)
		return false;
	word = temp->word;
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function of class mylist checks to see if the list of
 * nodes is currently empty.
 *
 * @param[in] : none.
 *
 * @calls[out] - none.
 *
 * @returns - True if the list is empty.
 * @returns - False if the list contains at least one word.
 *
 ******************************************************************************/
bool mylist::is_empty()
{
	node *temp = new (nothrow) mylist::node;
	temp = headptr;
	if (temp == NULL)
		return true;
	return false;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: this function of class mylist will count every node within
 * the linked list and will terminate when the "next" section of the nodes lead
 * to a null memory location
 *
 * @param[in] : none.
 *
 * @calls[out] -none.
 *
 * @returns - int of count of nodes that lead to non-null nodes.
 *
 ******************************************************************************/
int mylist::get_count()
{
	node *temp = new (nothrow) mylist::node;
	temp = headptr;
	int count = 0;
	while ( temp != NULL)
	{
		temp = temp->next;
		count++;
	}
	return count;
}