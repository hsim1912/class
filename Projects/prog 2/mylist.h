/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the basic information for function.cpp, mylist.cpp
 * and prog2.cpp.  It should be included with function.cpp, mylist.cpp
 * and prog2.cpp.
 ****************************************************************************/
#ifndef  _MYLIST_H
#define _MYLIST_H
#include <string>
#include <cctype>
#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;
/*******************************************************************************
 *          Constant Variables, defines, structures, classes and Enums
 ******************************************************************************/

/*!
 * @brief mylist class used to house the linked list as well as starting
 * pointer "headptr" as well as a few useful functions.
 *
 * @details This class also houses a structure called "node", which will
 * contain a string contained in section 'word' and a location to the next
 * node in the list.
 */

class mylist
{
	public:		
/*************************************************************************//** 
 * @author Alex Wulff 
 * 
 * @par Description: 
 * The constructor will allow for you to initialize the class, specifying that
 * the initial or "headptr" is set to a null value.  This will be used to 
 * determine where the list ends.
 * 
 ****************************************************************************/
		mylist()
		{
			headptr = NULL;	// set to null for empty list
		}
		mylist( mylist l[], int size)
		{
			node * headptr, * temp, *src;
			headptr = NULL;
			headptr->next = temp;

			src = l->headptr;
			src = src->next;

			while (src->next != NULL)
			{
				temp = new (nothrow) node; 
				temp = src;
				temp = temp->next;
				src = src->next;
			}
		}

/*************************************************************************//** 
 * @author Alex Wulff 
 * 
 * @par Description: 
 * The destructor will initiate a temporary node, and having set it to headptr,
 * temp will be used the nodes that might still exist in the list, should 
 * the list be determined as not empty.
 * 
 ****************************************************************************/
		~mylist()
		{
			if (is_empty())
			{
				return;
			}
			node *temp = new (nothrow) mylist::node;
			temp = headptr;
			while (temp != NULL)
			{
				remove(temp->word);
			}
			delete temp;
		}

		bool operator+ (string &word);
		mylist& operator= (const mylist &l1);

		bool insert(string &word);
		bool remove(string word);
		bool find( string word );
		bool retrieve( string &word, int loc );
		bool is_empty();
		int get_count();

	private:

/*!
 * @brief custom structure named "node" which will hold two values.  The
 * first being the word that will be sorted by alphabetical precidence and
 * the second being the memory address to the next word in the list.
 */

		struct node
		{
			string word;
			node *next;
		};
		node *headptr;
};
#endif