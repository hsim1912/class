/*-------------------------------------------------------------------   
   Program: lab8_2.cpp
   Author: <Fill in your name>
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description
     This program determines the weight of a person on Mars based on
     the force of gravity on this planet.  On Mars, the force of 
     gravity is 0.39 times as strong as on Earth. That is, someone 
     who weighs 100 pounds on Earth, he or she would weigh only 39 
     pounds on Mars.


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <iomanip>
using namespace std;

void computeWeight( double &weight, int choice);

/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         none
*********************************************************************/



int main()
{
    double weight;
    int choice;


    cout << fixed << showpoint << setprecision(2);
    cout << "Enter your weight: ";
    cin >> weight;

    while (weight > 0)
    {
        
		cout << "Select a planet (1: Mars, 2: Mercury, 3: Venus): " << weight << endl;
        cin >> choice;
		while (choice != 1 && choice != 2 && choice != 3)
		{
			cout << "The planet number is invalid.  Select a valid planet: ";
			cin >> choice;
		}
		
		computeWeight (weight , choice);

		cout << "Your weight on selected planet is: " << weight << endl;

		
		cout << "\nEnter your weight: ";
        cin >> weight;
    }

	return 0;
}

void computeWeight( double &weight, int choice)
{

	if (choice == 1)
		weight = weight * .39;

	if (choice == 2)
		weight = weight * .38;

	if (choice == 3)
		weight = weight * .78;

	return;
	
}