/*-------------------------------------------------------------------   
   Program: lab8_1.cpp
   Author: <Fill in your name>
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description
     This program call the function named dateConvert that receives
     a long integer of the form mmddyyyy and an integer n.  The 
     function returns the year (yyyy) if n is 1, or the month/day 
     combination (mmdd) if n is 2.  


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <iomanip>
#include <cctype>
using namespace std;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
int dateConvert( long, char );


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         dateConvert()
*********************************************************************/

int main()
{
	long date;
    char m;
    
    cout << "Enter a date (mmddyyyy): ";
    cin >> date;

    while (date >= 0)
    {
        cout << "Select M, D, or Y: ";
        cin >> m;

		if (m == 'Y' || m == 'y')

			cout << "The information requested is: " <<  setfill('0') << setw(4) << dateConvert(date, m) << endl;

		else cout << "The information requested is: " <<  setfill('0') << setw(2) << dateConvert(date, m) << endl;

        cout << "\nEnter a date (mmddyyyy): ";
        cin >> date;
    }

    cout << "\nProgram ended. Bye!" << endl;

	return 0;
}

/********************************************************************
   Function:      dateConvert
   Description:   returns the year or mmdd stored in a long int of 
     the form mmddyyyy
   Parameters:    date: long integer
                  n: short
   Returns:       int
   Called by:	main
   Calls:		none
*********************************************************************/
int dateConvert ( long date, char n )
{
    int value;


    if (n == 'D' || n == 'd')
        value = (date / 10000) % 100;
	if (n == 'M' || n == 'm')
		value = date / 1000000;
	if (n == 'Y' || n == 'y')
		value = date % 10000;
    if (n != 'D' && n != 'M' && n != 'Y' && n != 'd' && n != 'm' && n != 'y')
	{
        value = 0;
	}

    return value;
}