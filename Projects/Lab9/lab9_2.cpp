//  CSC 150  Lab 6g Solution
//  Dice Rolling Simulation
//  Val Manes   10/21/04
//  vnm 10/30/06  modified show_roll() to include array size parameter
/*-------------------------------------------------------------------
Program: lab9_2.cpp
Author: <Fill in your name >
Course: CSC 150 - <Fill in lab section number>
Instructor: <Fill in lab TA's name>
Date: < Fill in today's date>
Description:  
This program simulates the rolling of two dice, recording the 
totals for trial runs of 1000 rolls. The results of those rolls
are then displayed. The number of times to run the simulation is
entered by the user at program start.

---------------------------------------------------------------------
Includes
---------------------------------------------------------------------*/

#include <iostream>
#include <ctime>
#include <cstdlib>	
#include <iomanip>

using namespace std;

void roll_dice( int arr[] );
void show_rolls( int arr[] );

/********************************************************************
Function:      main
Description:   Program entry/exit point, prompts for inputs, 
performs calculations, displays results
Parameters:    none
Returns:       0
Called by:     none
Calls:         none
*********************************************************************/
int main()
{
    //Declare array to store counters in indexes 2-12, 0 & 1 unused


    int series;
    int num_runs;
	int roll[1000] = {0};

    srand(int(time(NULL)) ); //seed randomizer

    cout << "How many trials to perform: ";
    cin >> num_runs;

    if( num_runs <= 0 )
    {
        cout << "You selected to quit with no trials." << endl;
		exit (0);
    }

    else
    {
        for( series = 0; series < num_runs; series++ )
        {
            roll_dice(roll);

            cout << "  Trial number " << series + 1 << endl;
            show_rolls(roll);
        }
    }

    return 0;
}

/********************************************************************
Function:      roll_dice
Description:   Simulates rolling pair of dice and records values.
               Resets array that stores totals before beginning rolls.
Parameters:    int rolls[]    array to store score counts
               int num_rolls  number of rolls to be simulated
Returns:       void
Called by:     main()
Calls:         none
*********************************************************************/
void roll_dice( int arr[] )
{
    //declare local variables
	int i;

    //reset counters to 0 - remember that arrays are passed by value
    //so results of last run of the function would still be in array.
	for (i=0 ; i<1000 ; i++)
		arr [i] = 0;

	//run the simulation - loop for as many times as required
    for (i=0 ; i<1000 ; i++)
	//generate two random numbers between 1 and 6, inclusive, 
	//add them together and increment the value in the counter 
    //array at that index
	arr[i] += (rand() % 6 + 1) + (rand() % 6 +1);
}


/********************************************************************
Function:      show rolls
Description:   Displays the results of dice roll simulation
Parameters:    int rolls[]    array to store score counts
Returns:       void
Called by:     main()
Calls:         none
*********************************************************************/
void show_rolls( int arr[] )
{
    int i;
	int two = 0; 
	int three = 0;
	int four = 0;
	int five = 0; 
	int six = 0;
	int seven = 0;
	int eight = 0;
	int nine = 0;
	int ten = 0;
	int eleven = 0;
	int twelve = 0;
    cout << "Roll    Frequency" << endl;

    //loop through the counter array, only displaying the values
    //at indexes that represent valid totals of two dice
   for (i=0 ; i <1000 ; i++)
   {
	   if (arr[i] == 2)
		   two++;
	   else if (arr[i] ==3)
		   three++;
	   else if (arr[i] ==4)
		   four++;
	   else if (arr[i] ==5)
		   five++;
	   else if (arr[i] ==6)
		   six++;
	   else if (arr[i] ==7)
		   seven++;
	   else if (arr[i] ==8)
		   eight++;
	   else if (arr[i] ==9)
		   nine++;
	   else if (arr[i] ==10)
		   ten++;
	   else if (arr[i] ==11)
		   eleven++;
	   else if (arr[i] ==12)
		   twelve++;
   }
	   cout << "2    " << two << endl << "3    " << three << endl << "4    " << four << endl << "5    " << five << endl << "6    " << six << endl << "7    " << seven << endl << "8    " << eight << endl << "9    " << nine << endl << "10   " << ten << endl <<"11   " << eleven << endl << "12   " << twelve << endl;
}
