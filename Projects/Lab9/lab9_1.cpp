/*-------------------------------------------------------------------
   Program: lab9_1.cpp
   Author: <Fill in your name >
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description:  
     This program declares two 1-D arrays, natrevenue and intrevenue 
	 that keep the number of products a company sold nationally 
	 and internationally on each quarter of 2009.  It reads the 
	 revenue for each quarter, computes and displays the total revenue. 
	 This program does not validate the input.


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
using namespace std;


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         none
*********************************************************************/

int addRevenue(int arr[]);
int main()
{
	// Variable declaration
	int natRevenue[4] = {0};
	int intRevenue[4] = {0};
	int totalRevenue = 0;
	int total;
	int i;

	//Read in revenue amounts
	cout << "Enter national revenue for 4 quarters: ";
	for ( i = 0; i < 4; i++ )
		cin >> natRevenue[i];
	cout << "Enter international revenue for 4 quarters: ";
	for ( i=0; i < 4; i++ )
		cin >> intRevenue[i];

	//Add revenue
	for (i = 0; i < 4; i++)
		totalRevenue += natRevenue[i] + intRevenue[i];

	// Display total revenue
	
	cout << "\nTotal national revenue: " << addRevenue(natRevenue);
	cout << "\nTotal international revenue: " << addRevenue (intRevenue);
	cout << "\nTotal corporate revenue: " << totalRevenue << endl;
	return 0;
}
int addRevenue(int arr[])
{
	int i;
	int total = 0;
	for (i = 0; i < 4; i++)
			total += arr[i];
	return total;
}