#include <iostream>

#define Pi = 3.141;

using namespace std;


int main()
{
	
	int x, y;

	cout << "Enter x value" << endl;
	cin >> x;

	cout << "Enter y value" << endl;
	cin >> y;
	
	cout << "x = " << x << endl;
	cout << "y = " << y << endl; 

	return 0;
}