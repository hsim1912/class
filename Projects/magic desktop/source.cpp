#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

class mDesk
{

	mDesk()
	{
		node *heaptr = NULL;
	}

	~mDesk()
	{

	}
public:
	bool mDesk::add_paper(int size)
	{
		node *temp = new (nothrow) mDesk::node;
		temp = headptr;
		temp->size = size;
		node *current = new (nothrow) mDesk::node;
		current = headptr;
		node *previous = new (nothrow) mDesk::node;
		previous = headptr;
		if (size >= remaining_space())
			return false;
		else
		{
			if (temp == NULL)
			{
				headptr->next = temp;
				temp->next = NULL;
				return true;
			}
			while (current->next != NULL)
			{
				previous = current;
				current = current ->next;
			}
			current ->next = temp;
			temp ->next = NULL;
		}
		return true;
	}
	bool mDesk::remove_paper(int size)
	{
		node *temp = headptr;
		node *current = headptr; 
		node *previous = headptr;
		if (temp ->size == size)
		{
			headptr->next = temp->next;
			delete temp;
		}
		else
		{
			if (temp == NULL)
			{
				return false;
			}
			while (current ->next != NULL && current->size != size)
			{
				previous = current;
				current = current ->next;
			}
			// remember this
			if (current == NULL)
				return false;

			previous->next = current->next;
			delete current;
			return true;
		}
		return false;
	}
	int mDesk::remaining_space()
	{
		int desk_size = 36*60;
		node *temp = new (nothrow) mDesk::node;
		temp = headptr;
		if (headptr == NULL)
			return desk_size;
		while (temp->next != NULL)
		{	
			desk_size -= temp->size;
			temp = temp->next;
		}
		return desk_size;
	}

private:
	struct node
	{
		int size;
		node *next;
	};
	node *headptr;

};

int main()
{
	char choice;
	int width, height;
	int size;

	do
	{
		cout << "Main Menu\n";
		cout << "Add paper to desk top: a\n";
		cout << "Remove paper from desktop: r\n";
		cout << "Check remaining space: c\n";
		cout << "Quit: q\n";

		choice = tolower(choice);

		if (choice == 'a')
		{
				cout << "Enter width and height of paper:";
				cin >> width >> height;
				size = width * height;
				if (!mDesk::add_paper(size))
					cout << "not enough space.\n";
				else cout << "done.\n";
		}
		if (choice == 'r')
		{
				cout << "Enter width and height of paper:";
				cin >> width >> height;
				size = width * height;
				if (!mDesk::remove_paper(size))
					cout << "not enough space.\n";
				else cout << "done.\n";
		}
		if (choice == 'c')
		{
			cout << mDesk::remaining_space() << " square inches remain.\n";
		}
	}while(choice != 'q');



	return 0;
}
