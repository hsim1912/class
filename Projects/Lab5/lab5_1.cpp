/*-------------------------------------------------------------------
   Program: lab5_1.cpp
   Author: <Fill in your name >
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description:  
     This program recommends to a player whether or not to play tennis 
     if the current weather satisfies the condition shown in the flowchart
     that describes the conditions.  This program compiles but it
     does not work correctly and it is incomplete.  Please fix it.

---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <iomanip>
using namespace std;


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         none
*********************************************************************/
int main()
{
    int temp;
    int humidity;
    int wind;
    int isCloudy;

    cout << "Enter values for temp, humidity, wind and isCloudy: ";
    cin >> temp >> humidity >> wind >> isCloudy;  
    //note, this chained input method is not recommended for production use

  
    if (isCloudy)
    {
        if (humidity > 60)
        {
            cout << "Recommendation: Don't Play" << endl;
            return 0;
        }

		if (wind < 10)
		{
			cout << "Recommendation: Play" << endl;
			return 0;
		}

		cout << "Recommendation: Don't play" << endl;
		return 0;

    }
    
	
        if (temp > 60 && temp < 95)
        {
            cout << "Recommendation: Play" << endl;
            return 0;
        }

		cout << "Recommendation: Don't play" << endl;
	
    return 0;
}


  /*
     * ANSWER THE FOLLOWING QUESTIONS:

     * What integer *value* is considered false by an if-statement? __0____

     * What integer *values* are considered true by an if-statement? __!0_____ 

     * In if-statements, do negative values produce true, false or an error? ___True_____

  */