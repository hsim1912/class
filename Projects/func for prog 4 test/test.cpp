#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>

char month_name[12][20]= {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

using namespace std;
void all_highs ( int high[][40] );
void all_lows (int low[][40] );
void calc_stats(int high[][40], int low[][40]);
void display_stats( int high[][40], int low[][40], int month);
int read_data( ifstream &fin, int high[][40], int low[][40] );
bool open_input(ifstream &fin);

int write_data(ofstream &fout, int high[][40], int low [][40]);

void main_menu( int high[][40], int low[][40]);
void yearly_high_low(int high[][40], int low[][40]);

int main()
{
	const int sizeMonth = 15;
	const int sizeDay = 40;

	int low[sizeMonth][sizeDay];
	int high[sizeMonth][sizeDay];

	ifstream fin;
	ofstream fout;

	if (open_input (fin))
	{
		cout << "Can not access file for input.  Please check for corruption." << endl;
		exit (1);
	}

	cout << setprecision(2) << showpoint << fixed;
	
	fout.open("sorted_temps.txt");

	if (read_data( fin, high, low) != 365)
	{
		cout << " Data provided is corrupt. Incorrect amount of records." << endl;
		fin.close();
		exit (1);
	}

	calc_stats(high, low);
	
	main_menu(high, low);

	 fin.close();

	 cout << write_data( fout, high, low) << "records written.";
	 fout.close();
	
	return 0;
}
void all_highs ( int high[][40])
{
	int i = 0;
	for ( i; i <12; i++)
	{
		cout <<"\n" << left << setw(12) << month_name[i] << right << setw(10) << "Highest:" << setw(6) << high[i][37] << setw(10) << "Lowest:" << setw(6) << high[i][38] << setw(15) << "Average:" << high[i][39] << endl;
	}
	return;
}
void all_lows ( int low[][40])
{
	int i = 0;
	for ( i; i <12; i++)
	{
		cout <<"\n" << left << setw(12) << month_name[i] << right << setw(10) << "Highest:" << setw(6) << low[i][37] << setw(10) << "Lowest:" << setw(6) << low[i][38] << setw(15) << "Average:" << low[i][39] << endl;
	}
	return;
}
void calc_stats(int high[][40], int low[][40])
{
	int i = 0;
	int j = 0;
	double avgLow = 0;
	double avgHigh = 0;
	double sumLow;
	double sumHigh;

	for ( i ; i <12; i++)
	{
	int minLow = low[i][3];
	int minHigh = high[i][3];
	int maxLow = low[i][3];
	int maxHigh = high[i][3];
	sumHigh = 0;
	sumLow = 0;
		if ( i == 1 )
		{


			for (j ; j< 28 ; j++)
			{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])
			{
				minHigh = high[i][j];
			}
			if (minLow > low[i][j])
			{
				minLow = low[i][j];
			}
			if (maxHigh < high[i][j])
			{
				maxHigh = high[i][j];
			}
			if (maxLow < low[i][j])
			{
				maxLow = low[i][j];
			}
			}
		avgHigh = sumHigh / 28;
		avgLow = sumLow / 28;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}

		else if ( i == 0 || i == 2 || i == 4 || i == 6 || i == 7 || i == 9 || i == 11 )
		{

			for (j ; j < 31 ; j ++)
		{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])
			{
				minHigh = high[i][j];
			}
			if (minLow > low[i][j])
			{
				minLow = low[i][j];
			}
			if (maxHigh < high[i][j])
			{
				maxHigh = high[i][j];
			}
			if (maxLow < low[i][j])
			{
				maxLow = low[i][j];
			}
		}
		avgHigh = sumHigh / 31;
		avgLow = sumLow / 31;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}
		else if ( i == 3 || i == 5 || i ==  8 || i== 10)
		{

			for (j ; j < 30 ; j ++)
			{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])
			{
				minHigh = high[i][j];
			}
			if (minLow > low[i][j])
			{
				minLow = low[i][j];
			}
			if (maxHigh < high[i][j])
			{
				maxHigh = high[i][j];
			}
			if (maxLow < low[i][j])
			{
				maxLow = low[i][j];
			}
		}
		avgHigh = sumHigh / 30;
		avgLow = sumLow / 30;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}
			
		j=0;
		
	}
	return;
}
void display_stats( int high[][40], int low[][40], int month)
{
	cout << "\n" << month_name[month] << endl << "Highs" << endl;
	cout << "Highest: "<< setw(6) << high[month][37] << " Lowest: " << setw(6) << high[month][38] << " Average: " << setw(6) << high[month][39] << endl;
	cout <<  endl << "Lows" << endl;
	cout << "Highest: "<< setw(6) << low[month][37] << " Lowest: " << setw(6) << low[month][38] << " Average: " << setw(6) << low[month][39] << endl;
	return;
}
void main_menu( int high[][40], int low[][40] )
{
	char selection;
	int month;
		
	do
	{
		cout << "\nTemperature Data Viewer" << endl; 
		cout << setw(23) << setfill('-') << "-"  << endl << setfill(' ');
		cout << "A. View all months' Highs" << endl << "B. View all months' Lows" << endl;
		cout << "C. View single month's stats" << endl << "D. View yearly high and low" << endl;
		cout << "Q. Quit program" << endl << "Option: ";

		cin >> selection;

		tolower(selection);
		
	if (selection == 'a')
	{
		cout << endl;
		all_highs ( high );
	}
	if (selection == 'b')
	{
		cout << endl;
		all_lows( low );
	}
	if (selection == 'c')
	{
		cout << "Select month (1 -12): ";
		cin >> month;
		while (month < 1 || month > 12)
		{
			cout << endl << "Please select as 1 for January, 2 for Feburary, etc. (1-12): ";
			cin >> month;
		}
		month -=1;
		display_stats (high, low, month);
		cout << endl;
	}
	
	if (selection == 'd')
	{
		cout << endl;
		yearly_high_low(high, low);
	}
}while ( selection != 'q' );
	cout << "writing records, then exiting." << endl;
	return;
}
bool open_input(ifstream &fin)
{

	fin.open("rand_temps.txt");
	
	if (!fin)
	{
		return true;
	}
	return false;
}
int read_data( ifstream &fin, int high[][40], int low[][40] )
{
int highTemp, lowTemp, i, j, count = 0;

	int testCount;

	fin >> i;
	fin >> j;
	fin >> highTemp;
	fin >> lowTemp;

	testCount = 365;

	high[i-1][j-1] = highTemp;
	low[i-1][j-1] = lowTemp;

	count++;

while ( testCount != count )
{

	fin >> i;
	fin >> j;
	fin >> highTemp;
	fin >> lowTemp;

	high[i-1][j-1] = highTemp;
	low[i-1][j-1] = lowTemp;
	count++;
		
}

return count;

}
int write_data(ofstream &fout, int high[][40], int low [][40])
{
	int i = 0;
	int j = 0;
	int count = 0;
	for ( i; i <12; i++)
	{
		
		
		if ( i == 1 )
		{

		for (j ; j< 28 ; j++)
		{
			fout << i + 1 << " " << j+1 << " " << high[i][j] << " " << low[i][j] << endl;
			count ++;
		}
		}

		else if ( i == 0 || i == 2 || i == 4 || i == 6 || i == 7 || i == 9 || i == 11 )
		{
			for (j ; j < 31 ; j ++)
			{
			fout << i + 1 << " " << j+1 << " " << high[i][j] << " " << low[i][j] << endl;
			count ++;	
			}
		}
		else if ( i == 3 || i == 5 || i ==  8 || i== 10)
		{
			for (j ; j < 30 ; j ++)
			{
			fout << i + 1 << " " << j+1 << " " << high[i][j] << " " << low[i][j] << endl;
			count ++;	
			}
		}
		
			
		j=0;
		
	}
		return count;
}
void yearly_high_low(int high[][40], int low[][40])
{
	int min = low[0][38];
	int max = high[0][37];
	int i = 0;
	int j = 0;
	int indexMonthLow = 0;
	int indexDayLow = 0;
	int indexMonthHigh = 0;
	int indexDayHigh = 0;
	int firstHighDay = 0;
	int firstHighIndex = 0;
	int firstLowDay = 0;
	int firstLowIndex = 0;


	for (i ; i < 12 ; i++)
	{
		if (min > low[i][38])
		{
			min = low[i][38];
			indexMonthLow = i;
		}
		if (max < high [i][37])
		{
			max = high [i][37];
			indexMonthHigh = i;
		}
	}

	for (j=0 ; j < 31 ; j++)
	{
		if (low[indexMonthLow][j] == min)
		{
			indexDayLow = j;
		
		if (firstLowDay == 0)
		{
			firstLowDay = indexDayHigh;
			firstLowIndex = j;
		}
		}
		if (high[indexMonthHigh][j] == max)
		{
			indexDayHigh = j;
				if (firstHighDay == 0)
		{
			firstHighDay = indexDayHigh;
			firstHighIndex = j;
		}
		}
	}
	if ( indexDayHigh != firstHighIndex && indexDayLow != firstLowDay )
	{
		cout << "Highest temperature was on " << month_name[indexMonthHigh] << " " << firstHighIndex + 1 << " and " << indexDayHigh+1 << " and the temperature was " << high[indexMonthHigh][indexDayHigh] << endl;
		cout << "Lowest temperature was on " << month_name[indexMonthLow] << " " << firstLowIndex + 1 << " and " <<indexDayLow+1  <<" and the temperature was " << low[indexMonthLow][indexDayLow] << endl;
	}
	else if ( indexDayHigh != firstHighIndex)
	{
		cout << "Highest temperature was on " << month_name[indexMonthHigh] << " " << firstHighIndex + 1 << " and " << indexDayHigh+1 << " and the temperature was " << high[indexMonthHigh][indexDayHigh] << endl;
		cout << "Lowest temperature was on " << month_name[indexMonthLow] << " " <<indexDayLow+1 << " and the temperature was " << low[indexMonthLow][indexDayLow] << endl;
		return;
	}
	else if (indexDayLow != firstLowDay)
	{
		cout << "Highest temperature was on " << month_name[indexMonthHigh] << " " <<indexDayHigh+1 << " and the temperature was " << high[indexMonthHigh][indexDayHigh] << endl;
		cout << "Lowest temperature was on " << month_name[indexMonthLow] << " " << firstLowIndex + 1 << " and " <<indexDayLow+1  <<" and the temperature was " << low[indexMonthLow][indexDayLow] << endl;
		return;
	}
	else
	{
		cout << "Highest temperature was on " << month_name[indexMonthHigh] << " " <<indexDayHigh+1 << " and the temperature was " << high[indexMonthHigh][indexDayHigh] << endl;
		cout << "Lowest temperature was on " << month_name[indexMonthLow] << " " <<indexDayLow+1 << " and the temperature was " << low[indexMonthLow][indexDayLow] << endl;
		return;
	}
}