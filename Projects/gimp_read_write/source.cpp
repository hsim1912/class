#include "function.h"


int main(int argc, char **argv)
{
char open[20];
char close[20];
image pic;
ifstream fin;
ofstream fout;
int type;
int current = 0;
int r = 0;
int c = 0;
string s1, s2, s3;
char manip_option, output_option, convert;

cout << "open what?"<< endl;
cin >> open;

cout << "name it what?" << endl;
cin >> close;
fin.open(open, ios::in);
if (!fin)
{
	cout << "Error opening" << endl;
	exit (1);
}

fout.open(close, ios::out);
if (!fout)
{
	cout << "Error closing" << endl;
	exit (1);
}

type = type_check(fin,current, r, c);

cout << type << endl;

if (type == 5 || type == 6)
{
	fin.close();
	fin.open(open, ios::binary);
}

set_pic_size( pic, r, c);
if (type == 3 || type == 2)
read_ascii(fin, current, pic);
else read_binary(fin, current, pic);
cout << "COMPLETE READ IN!" << endl;
if (type == 3 || type == 2)
write_ascii(fout, pic, type);
else
{
	write_bin_header(fout, pic, current, type);
	fout.close();
	cout << "convert?" << endl;
	cin >> convert;
	if (convert == 'y')
	{
		fout.open(close, ios::app);
		write_ascii(fout, pic, type);
	}
	else
	{
	fout.open(close, ios::binary| ios::app);
	write_binary(fout, pic, current, type);
	}
}
	
	fin.close();
	fout.close();

	return 0;
}
void set_pic_size(image &pic, int row, int column)
{
	int i = 0;
	int j = 0;
	pic.redgray = new (nothrow) unsigned char * [row];
	pic.green = new (nothrow) unsigned char * [row];
	pic.blue = new (nothrow) unsigned char * [row];

if (pic.redgray == NULL || pic.green == NULL || pic.blue == NULL)
{
	cout << "ERROR WITH ALLOCATING IMAGE SPACE." << endl;
	exit (-1);
}

for (i = 0; i < row ; i ++)
{
	pic.redgray[i] = new (nothrow) unsigned char [column];
	pic.green[i] = new (nothrow) unsigned char [column];
	pic.blue[i] = new (nothrow) unsigned char [column];
}
pic.rows = row;
pic.cols = column;
	return;
}
void read_ascii(ifstream &fin, int position, image &pic)
{
	int r,g,b;
	int i= 0;
	int j = 0;
	fin.seekg(position, ios::beg);
	fin.ignore (4);
	for (i; i<pic.rows; i++)
	{
		for(j=0; j<pic.cols; j++)
		{
			fin >> r;
			fin >> g;
			fin >> b;
			pic.redgray[i][j] = (unsigned char) r;
			pic.green[i][j] = (unsigned char) g;
			pic.blue[i][j] = (unsigned char) b;
		}
	}
		return;
}
void read_binary(ifstream &fin, int position, image &pic)
{
	unsigned char r, g, b;
	int count= 0;
	int i= 0;
	int j = 0;
	fin.seekg(position, ios::beg);
	for (i; i<pic.rows; i++)
	{
		for(j=0; j<pic.cols; j++)
		{
			
			fin.read((char *) &r, sizeof(unsigned char));
			fin.read((char *) &g, sizeof(unsigned char));
			fin.read((char *) &b, sizeof(unsigned char));
			pic.redgray[i][j] = r;
			pic.green[i][j] = g;
			pic.blue[i][j] = b;
			count += 3;
			/*cout << (int)pic.redgray[i][j] << " ";
			cout << (int)pic.green[i][j] << " ";
			cout << (int)pic.blue[i][j] << endl;
			if (i == 83 && j == 70)
			cout << "here " << i << " "<< j<< endl;*/
		}
		
	}
	cout << count << "Read in" << endl;
		return;
}
int type_check(ifstream &fin, int &current, int &r, int &c)
{
	string type;
	char test[5];
	string comment;
	char test2[5];
	char test3[5];
	fin >> type;
	fin.ignore();
	if(fin.peek() == '#')
	getline(fin, comment, '\n') ;
	fin >> test2;
	fin >> test3;
	
	while(atoi (test) != 255)
	{
		fin >> test;
	}

	current = fin.tellg();
	r = atoi(test2);
	c = atoi(test3);
	fin.clear();
	if (type.compare ("P2") == 0)
	{
		return 2;
	}
	if (type.compare ("P3") == 0)
	{
		return 3;
	}
	if (type.compare ("P5") == 0)
	{
		return 5;
	}
	if (type.compare ("P6") == 0)
	{
		return 6;
	}
	
	return 0;
}
void write_ascii(ofstream &fout, image &pic, int magic_num)
{		
	int i, j;
	fout << "P"<< magic_num << endl;
	fout << "#" << "removed orriginal comments" << endl;
	fout << pic.rows << " " << pic.cols << endl << "255" << endl;
		if (magic_num == 2)
		{
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout << (int) pic.redgray[i][j] << " ";			
				}
				fout << endl;
				j = 0;
			}
		}
			if (magic_num == 3 || magic_num== 6)
		{
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout << (int) pic.redgray[i][j] << " ";
				fout << (int) pic.green[i][j] << " ";
				fout << (int) pic.blue[i][j] << " ";
				fout << endl;
				}
				fout << "break";
				j = 0;
			}
		}
			return;
	}

void write_bin_header(ofstream &fout, image &pic, int &current, int magic_num)
{
	if (magic_num == 5)
	{
		fout << "P5" << endl;
		fout << "# removed orriginal comments" << endl;
		fout << pic.rows << " " << pic.cols << endl;
		fout << "255" << endl;
	}
	if (magic_num == 6)
	{
		fout << "P6" << endl;
		fout << "# removed orriginal comments" << endl;
		fout << pic.rows << " " << pic.cols << endl;
		fout << "255" << endl;
	}
	current = fout.tellp();
	return;
}
void write_binary(ofstream &fout, image &pic, int current, int magic_num)
{
	int i,j, count;
	count =0;
		if (magic_num == 5)
		{
			fout.seekp(current, ios::beg);
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout.write ((char*) &pic.redgray[i][j], sizeof(char));
				}
				j = 0;
			}
		}
			if (magic_num == 6)
		{
			fout.seekp(current, ios::beg);
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout.write ((char*) &pic.redgray[i][j], sizeof(char));
				fout.write ((char*) &pic.green[i][j], sizeof(char));
				fout.write ((char*) &pic.blue[i][j], sizeof(char));
				count += 3;
				}
				j = 0;
//				if (i == 84)
	//				cout << "100" << endl;
			}
			cout << count << "Records written." << endl;
		}
	return;
}