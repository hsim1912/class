#include "function.h"

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will test the arguments sent into main from command prompt. 
 * Using argc, the function will use strings to compare the values passed in 
 * for correct usage.
 *
 * @param[in] (value) arguments:  an integer base for the count of arguments, 
 * passed this way to reduce input being modified more than needed.
 * @param[in] (reference) *argv[]: the array of arguments at command level 
 * passed in as an array to be further modified to individual strings.
 * @param[in/out] (reference) s1, s2, s3: These strings are empty at compile time,
 * but depending upon the argc, might need multiple to further handle options 
 * of usage.
 *
 * @calls[out] usage_menu: if validation of arguments should fail, a usage menu 
 * will be presented to the user, then return false to main.
 * @returns - True: should the conditions be met of correct argument count with
 * correct arguments.
 * @returns - False: should the conditions of argument count or the arguments
 * themselves be invalid.
 *
 ******************************************************************************/
bool arg_check( int arguments, char *argv[], string &s1, string &s2, string &s3)		
	// check number of arguments in argc and test for correct
	// usage.
{
	int b;
	if (arguments < 4 || arguments > 6)
		{
		usage_menu();
		return false;
		}
	if (arguments == 4)
	{
		s1 = argv[1];
		if (s1.compare ("-oa") !=0 || s1.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
	if (arguments == 5)
	{
		s1 = argv[1];
		if ( s1.compare ("-n") !=0 || s1.compare ("-p") !=0 || s1.compare ("-s") !=0
			|| s1.compare ("-g") !=0 || s1.compare ("-c") !=0)
		{
		usage_menu();
		return false;
		}
		s2 = argv[2];
		if (s2.compare ("-oa") !=0 || s2.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
		if (arguments == 6)
	{
		s1 = argv[1];
		if ( s1.compare ("-n") !=0 || s1.compare ("-p") !=0 || s1.compare ("-s") !=0
			|| s1.compare ("-g") !=0 || s1.compare ("-c") !=0)
		{
		usage_menu();
		return false;
		}
		s2 = argv[2];
		b = atoi (argv[2]);
		if ( b < 0 || b > 255)
		{
			usage_menu();
			return false;
		}
		s3 = argv[3];
		if (s3.compare ("-oa") !=0 || s3.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to brighten the
 * image by an intiger value
 *
 * @param[in/out] (reference) pic:  Structure image will be passed by reference
 * so the modifications made will be saved when returning to main for writing.
 * @param[in] (value) brighten: an integer that will be used for the 
 * brightening of the image given that every value is increased by this int.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void bright(image &pic, int brighten, int row, int column)
{
	int i = 0;
	int j = 0;
	for (i; i<row; i++)				// set for loop walk thru to take absolute value
	{								// of 255 - current color value and check to keep between
		for (j; j<column; j++)		// 0 and 255.
		{
			if (brighten + (int) pic.redgray[i][j] > 255)
				pic.redgray[i][j] = (char) 255;
			else if (brighten + (int) pic.redgray[i][j] < 0)
				pic.redgray[i][j] = 0;
			else
			pic.redgray[i][j] = (char) (brighten + (int) pic.redgray[i][j]);
			if (brighten + (int) pic.green[i][j] > 255)
				pic.green[i][j] = (char) 255;
			else if (brighten + (int) pic.green[i][j] < 0)
				pic.green[i][j] = 0;
			else
			pic.green[i][j] = (char) (brighten + (int) pic.green[i][j]);
			if (brighten + (int) pic.blue[i][j] > 255)
				pic.blue[i][j] = (char) 255;
			else if (brighten + (int) pic.blue[i][j] < 0)
				pic.blue[i][j] = 0;
			else
			pic.blue[i][j] = (char) (brighten + (int) pic.blue[i][j]);
		}
		j = 0;
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to adjust the
 * contrast of the image by averaging adjacent pixel values.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
  * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void contrast(image &pic, image &pic2, int row, int column)
{
	int i= 0;
	int j= 0;
	int contrast_scale;
	int min = 255;
	int max = 0;
		for (i; i<row; i++)				//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<column; j++)
		{
			pic2.redgray[i][j] = .3*(int)pic.redgray[i][j] + .6*(int)pic.green[i][j] 
			+ .1*(int)pic.blue[i][j];
			if ((int) pic2.redgray[i][j] > max)
			{
				max = (int) pic2.redgray[i][j];
			}
			if ((int) pic2.redgray[i][j] < min)
			{
				min = (int) pic2.redgray[i][j];
			}
		}
		j = 0;
	}
	contrast_scale = 255.0 / (max - min);

		for (i = 0; i<row; i++)				//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j = 0; j<column; j++)
		{
			pic2.redgray[i][j] = contrast_scale * (int) pic2.redgray[i][j];
		}
	}		
		
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to balance the 
 * current aspects of the image (red, green, blue) and construct a gray scale
 * version.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
  * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void gray(image &pic, image &pic2, int row, int column)
{
	int i= 0;
	int j= 0;
		for (i; i<row; i++)				//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<column; j++)
		{
			pic2.redgray[i][j] = .3*(int)pic.redgray[i][j] + .6*(int)pic.green[i][j] 
			+ .1*(int)pic.blue[i][j];
		}
		j = 0;
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to determine how
 * to proceed with the desired modifications and calls there from.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
 * @param[in] (value) option: this character will be used to set a switch
 * for which modification needs to be made.
 * @param[in] (value) brighten: this integer is used only in an instance of
 * brightening the photo.  Should it need to be passed, it will be ready
 * to do so.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] will call the following functions based on switch:
 * negate(), bright(), sharpen(), smooth(), grey(), contrast(),
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void manip(image &pic, image &pic2, char option, int brighten, int row, int column)
{    
	int choice = (int) option;

	switch (choice)
	{
		case (int) 'n':
			negate(pic, row, column);
			break;
		case (int) 'b':
			bright (pic, brighten, row, column);
			break;
		case (int) 'p':
			sharpen(pic, pic2, row, column);
			break;
		case (int) 's':
			smooth(pic, pic2, row, column);
			break;
		case (int) 'g':
			gray(pic, pic2, row, column);
			break;
		case (int) 'c':
			contrast(pic, pic2, row, column);
			break;
		default:
			return;
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will take a string for the potential values of manipulation 
 * to be used on the image, and will cataloge them for further use.
 *
 * @param[in] (reference) option:  this string will contain the options 
 * given at run-time, and having been validated, will run the comparison
 * and assign a value for return
 *
 * @calls[out] none.
 *
 * @returns - a single character that will be used for later evaluation on which
 * modification is desired.
 * @returns - will exit if an error should occur and was not caught previously
 * yeilding an exit code of 3.
 *
 ******************************************************************************/
char manip_options(string option)
{
	if ( option.compare("-n") == 0)
		return 'n';
	if ( option.compare("-b") == 0)
		return 'b';
	if ( option.compare("-p") == 0)
		return 'p';
	if ( option.compare("-s") == 0)
		return 's';
	if ( option.compare("-g") == 0)
		return 'g';
	if ( option.compare("-c") == 0)
		return 'c';
	usage_menu();
	exit (3);
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will invert each value in the image's structure to be the
 * opposite value (255 - current value) to produce an inverted color spectrum.
 *
 * @param[in/out] (reference) pic: the structure which will be modified to
 * produce the inverted colorscheme.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void negate( image &pic, int row, int column )
{
	int i = 0;
	int j = 0;
	for (i; i<row; i++)				//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<column; j++)
		{
			pic.redgray[i][j] = (char) (255 - (int) pic.redgray[i][j]);
			pic.green[i][j] = (char) (255 - (int) pic.green[i][j]);
			pic.blue[i][j] = (char) (255 - (int) pic.blue[i][j]);
		}
		j = 0;
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to open an input
 * file for reading.
 *
 * @param[in/out] (reference) fin: file for input of ifstream, that will be
 * opened and tested with the name given from comandline.
 * @param[in/out] (reference) string1: used to carry the name that will be 
 * opened by fin.open.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 * @returns - exits with code of 1 should fin not open and prompts error.
 *
 ******************************************************************************/
void open_input(ifstream &fin, string &string1 )
{

	fin.open( string1 );
	
	if (!fin)
	{
		cout << "Failed to open " << string1 << ". Please validate and try " <<
		cout << "again.";
		exit (1);
	}
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to open an output
 * file for writing.
 *
 * @param[in/out] (reference) fout: file for output of ofstream, that will be
 * opened and tested with the name given from comandline.
 * @param[in/out] (reference) s1: string used to carry the name that will be 
 * opened by fout.open.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 * @returns - exits with code of 1 should fin not open and prompts error.
 *
 ******************************************************************************/
void open_output(ofstream &fout, string s1, char option)
{
	if (option == 'b')
	fout.open ( s1, ios::out| ios::binary| ios::trunc );
	else fout.open ( s1, ios::out| ios::trunc );
	if (!fout)
	{
		cout << "Failed to open " << s1 << ". Please validate and try again.";
		exit (1);
	}

	return;
}
char output_options(string option)
{
	if ( option.compare("-oa") == 0)
		return 'a';
	if ( option.compare("-ob") == 0)
		return 'b';
	return 'n';
}
void set_pic_size(image &pic, int row, int column)
{
	int i = 0;
	int j = 0;
	pic.redgray = new (nothrow) unsigned char * [row];
	pic.green = new (nothrow) unsigned char * [row];
	pic.blue = new (nothrow) unsigned char * [row];

if (pic.redgray == NULL || pic.green == NULL || pic.blue == NULL)
{
	cout << "ERROR WITH ALLOCATING IMAGE SPACE." << endl;
	exit (-1);
}

for (i = 0; i < row ; i ++)
{
	pic.redgray[i] = new (nothrow) unsigned char [column];
	pic.green[i] = new (nothrow) unsigned char [column];
	pic.blue[i] = new (nothrow) unsigned char [column];
}
	return;
}
void sharpen(image &pic, image &pic2, int row, int column)
{
	int i = 1;
	int j = 1;
	for (i; i<row - 1; i++)				//set for loop walk thru to take absolute adjacent
	{									// pixels, subtract their value and multiply by 5 to sharpen 
		for (j; j<column - 1; j++)
		{
			pic2.redgray[i][j] = (char) ( 5 * ((int) pic.redgray[i][j]) - 
			(int)pic.redgray[i-1][j] - (int)pic.redgray[i][j-1] - (int)pic.redgray[i][j+1]
			- (int)pic.redgray[i+1][j]);
			pic2.green[i][j] = (char) ( 5 * ((int) pic.green[i][j]) - 
			(int)pic.green[i-1][j] - (int)pic.green[i][j-1] - (int)pic.green[i][j+1]
			- (int)pic.green[i+1][j]);
			pic2.blue[i][j] = (char) ( 5 * ((int) pic.blue[i][j]) - 
			(int)pic.blue[i-1][j] - (int)pic.blue[i][j-1] - (int)pic.blue[i][j+1]
			- (int)pic.blue[i+1][j]);
		}
		j = 0;
	}
	for (i = 0; i < row; i++)			// do boundary copy from old to new.
	{
			pic2.redgray[i][0] = pic.redgray[i][0];
			pic2.redgray[i][column - 1] = pic.redgray[i][column - 1];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[i][column - 1] = pic.green[i][column - 1];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[i][column - 1] = pic.blue[i][column - 1];
	}
	for (i = 0; i< column; i++)
	{
			pic2.redgray[0][i] = pic.redgray[0][i];
			pic2.redgray[row - 1][i] = pic.redgray[row - 1][i];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[row - 1][i] = pic.green[row - 1][i];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[row - 1][i] = pic.blue[row - 1][i];
	}
		return;
}
void smooth(image &pic, image &pic2, int row, int column)
{
	int i = 1;
	int j = 1;
	for (i; i<row - 1; i++)				//set for loop walk thru to take absolute adjacent
	{									// pixels, subtract their value and multiply by 5 to sharpen 
		for (j; j<column - 1; j++)
		{
			pic2.redgray[i][j] = (char) (((int)pic.redgray[i][j] + (int) 
			pic.redgray[i-1][j-1] + (int)pic.redgray[i-1][j] + (int)pic.redgray[i-1][j+1]
			+ (int)pic.redgray[i][j-1] + (int)pic.redgray[i][j+1] +
			(int)pic.redgray[i+1][j-1] + (int)pic.redgray[i+1][j] + 
			(int)pic.redgray[i+1][j+1])/9.0);
			pic2.green[i][j] = (char) (((int)pic.green[i][j] + (int) 
			pic.green[i-1][j-1] + (int)pic.green[i-1][j] + (int)pic.green[i-1][j+1]
			+ (int)pic.green[i][j-1] + (int)pic.green[i][j+1] +
			(int)pic.green[i+1][j-1] + (int)pic.green[i+1][j] + 
			(int)pic.green[i+1][j+1])/9.0);
			pic2.blue[i][j] = (char) (((int)pic.blue[i][j] + (int) 
			pic.blue[i-1][j-1] + (int)pic.blue[i-1][j] + (int)pic.blue[i-1][j+1]
			+ (int)pic.blue[i][j-1] + (int)pic.blue[i][j+1] +
			(int)pic.blue[i+1][j-1] + (int)pic.blue[i+1][j] + 
			(int)pic.blue[i+1][j+1])/9.0);
		}
		j = 0;
	}
	for (i = 0; i < row; i++)			// do boundary copy from old to new.
	{
			pic2.redgray[i][0] = pic.redgray[i][0];
			pic2.redgray[i][column - 1] = pic.redgray[i][column - 1];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[i][column - 1] = pic.green[i][column - 1];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[i][column - 1] = pic.blue[i][column - 1];
	}
	for (i = 0; i< column; i++)
	{
			pic2.redgray[0][i] = pic.redgray[0][i];
			pic2.redgray[row - 1][i] = pic.redgray[row - 1][i];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[row - 1][i] = pic.green[row - 1][i];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[row - 1][i] = pic.blue[row - 1][i];
	}

	return;
}
int type_check(ifstream &fin, int &current, int &r, int &c)
{
	string type;
	string test;
	char test2[5];
	char test3[5];
	fin >> type;
	fin.ignore();
	getline(fin, test, '\n') ;
	fin >> test2;
	fin >> test3;

	current = fin.tellg();

	r = atoi(test2);
	c = atoi(test3);
	fin.clear();
	if (type.compare ("P2") == 0)
	{
		return 2;
	}
	if (type.compare ("P3") == 0)
	{
		return 3;
	}
	if (type.compare ("P5") == 0)
	{
		return 5;
	}
	if (type.compare ("P6") == 0)
	{
		return 6;
	}
	
	return 0;
}

void read_ascii(ifstream &fin, int position, image &pic)
{
	int r,g,b;
	int i= 0;
	int j = 0;
	fin.seekg(position, ios::beg);
	fin.ignore (4);
	for (i; i<pic.rows; i++)
	{
		for(j=0; j<pic.cols; j++)
		{
			fin >> r;
			fin >> g;
			fin >> b;
			pic.redgray[i][j] = (unsigned char) r;
			pic.green[i][j] = (unsigned char) g;
			pic.blue[i][j] = (unsigned char) b;
		}
	}
		return;
}
void read_binary(ifstream &fin, int position, image &pic)
{
	unsigned char r,g,b;
	int i= 0;
	int j = 0;
	fin.seekg(position + 4*(sizeof(char)), ios::beg);
	for (i; i<pic.rows; i++)
	{
		for(j=0; j<pic.cols; j++)
		{
			fin.read((char *)&r, sizeof(char));
			fin.read((char *)&g, sizeof(char));
			fin.read((char *)&b, sizeof(char));
			pic.redgray[i][j] = r;
			pic.green[i][j] = g;
			pic.blue[i][j] = b;

		}
	}
		return;
}
/*void read_type(ifstream &file, int type, int row, int column,int position,image &pic)
{
	switch (type)
{
case 2:
	read_ascii(file, row, column, position, pic);
	break;
case 3:
	read_ascii(file, row, column, position, pic);
	break;
case 5:
	read_binary(file, row, column, position, pic);
	break;
case 6:
	read_binary(file, row, column, position, pic);
	break;	
}

}
*/
void usage_menu()
{
		cout << "\nProg1 usage:\n\nC:\...Prog1.exe [options] -o[ab]";
		cout << "New_image_name Existing_image.ppm" << endl << endl;
		cout << "Requirements: -o[ab].  -oa for ascii output or -ob for binary.\n\n";
		cout << "Options:\nOption Code:" << setw(20) << "Option Name:" << endl;
		cout << setw(5) << "-n\t\t\t Negate" << endl;
		cout << setw(5) << "-b #\t\t\t Brighten by # (as integer)" << endl;
		cout << setw(5) << "-p\t\t\t Sharpen" << endl;
		cout << setw(5) << "-s\t\t\t Smooth" << endl;
		cout << setw(5) << "-g\t\t\t Grayscale" << endl;
		cout << setw(5) << "-C\t\t\t Contrast to average" << endl;

		cout << "\nExamples: \n\nExample 1: Prog1.exe -oa balls ColorBalls1.ppm\n";
		cout << "Example 2: Prog1.exe -g -ob hearts BleedingHearts2.ppm\n";
		cout << "Example 3: Prog1.exe -b 34 -ob balloon balloons1.ppm\n";

		return;
}
void write_ascii(ofstream &fout, image &pic, int magic_num)
{		
	int i, j;
	fout << "P"<< magic_num << endl;
	fout << "#" << "removed orriginal comments" << endl;
	fout << pic.rows << " " << pic.cols << endl << "255" << endl;
		if (magic_num == 2)
		{
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout << (int) pic.redgray[i][j] << " ";			
				}
				fout << endl;
				j = 0;
			}
		}
			if (magic_num == 3)
		{
			for (i=0; i<pic.rows; i++)
			{
				for ( j = 0; j< pic.cols; j ++)
				{	
				fout << (int) pic.redgray[i][j] << " ";
				fout << (int) pic.green[i][j] << " ";
				fout << (int) pic.blue[i][j] << " ";
				fout << endl;
				}
				j = 0;
			}
		}
			return;
	}
void write_binary(ofstream &fout, image &pic,int row,int column,int magic_num)
{
	int i,j;
	char magic = (char) magic_num;
		fout.write("P",sizeof(char)); fout.write((char*) magic, sizeof (int));
		fout.write("# removed orriginal comments", sizeof(char) * strlen("# removed orriginal comments"));
		fout.write((char*)row, sizeof(char));  fout.write((char*)column, sizeof(char));
		if (magic_num == 6)
		{
			for (i=0; i<row; i++)
			{
				for ( j = 0; j< column; j ++)
				{	
				fout.write ((char*)pic.redgray[i][j], sizeof(char));
				}
				j = 0;
			}
		}
			if (magic_num == 5)
		{
			for (i=0; i<row; i++)
			{
				for ( j = 0; j< column; j ++)
				{	
				fout.write ((char*)pic.redgray[i][j], sizeof(char));
				fout.write ((char*)pic.green[i][j], sizeof(char));
				fout.write ((char*)pic.blue[i][j], sizeof(char));
				}
				j = 0;
			}
		}
	return;
}
/*void write_pic(image &pic, char output_option, char manip, ofstream &fout, int row, int column)
{
	int magic_num = 6;	// establish magic number.
	if (output_option == 'a'&& manip == 'g')
		magic_num = 3;
	if (output_option == 'a' && manip !='g')
		magic_num = 2;
	if (output_option == 'b' && manip == 'g')
		magic_num = 5;
		// switch on it for binary or ascii
	if ( magic_num == 2 || magic_num == 5)
	{
		write_ascii(fout, pic, row, column, magic_num);
	}
	if ( magic_num == 5 || magic_num == 6)
	{
		write_binary(fout, pic, row, column, magic_num);
	}
	return;
}*/