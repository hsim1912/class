#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>



#ifndef  __FUNCTION__H__
#define __FUNCTION__H__
typedef unsigned char pixel;

struct image
{
    int rows;
    int cols;
	//char comment[80];
    pixel **redgray;	// handles red channel or grayscale
    pixel **green;
    pixel **blue;
};


#endif


using namespace std;

bool arg_check( int arguments, char *argv[], string &s1, string &s2, string &s3);
void bin_header(ofstream &fout, image &pic, int magic_num, int &current);
void bright(image &pic, int brighten, int row, int column);
void get_possition(ifstream &fin, int &current, int r, int c);
void manip(image pic, image pic2, char option, int brighten, int row, int column);
char manip_options(string option);
void negate( image &pic, int row, int column );
void open_input(ifstream &fin, string string1 );
void open_output(ofstream &fout, string s1);
char output_options(string option);
void set_pic_size(image &pic, int row, int column);
void sharpen(image &pic, image &pic2, int row, int column);
void smooth(image pic, image pic2, int row, int column);
int type_check(ifstream &fin, int &current, int &r, int &c);
void read_ascii(ifstream &fin, int position, image &pic);
void read_binary(ifstream &fin, int position, image &pic);
void read_type(ifstream file, int type, int row, int column,int position,image pic);
void usage_menu();
void write_pic(image pic, char output_option, char manip, ofstream fout, int row, int column);
void write_ascii(ofstream &fout, image &pic, int magic_num);
void write_binary(ofstream &fout, image &pic, int current, int magic_num);
void write_bin_header(ofstream &fout, image &pic, int &current, int magic_num);