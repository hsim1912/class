#include <iostream>
#include <fstream>
#include <string>


using namespace std;


int main()
{
	int i, num;
	ofstream pages, time;
	pages.open ("pages.rand");
	time.open ("arrival.rand");

	if (!pages || !time)
		exit (10);

	for (i = 0; i< 1000000; i++)
	{
		num = (rand() % 70) +1;
		if (num <10)
			num += 10;

		time << num << endl;

	}
	for (i = 0; i< 1000000; i++)
	{
		num = (rand() % 25) +1;
		if (num <15)
			num += 15;

		pages << num << endl;
	}
	time.close();
	pages.close();

	return 0;
}