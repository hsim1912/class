#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

struct node
{
	string word;
	node *next;
};


void add_word(string &word, node *&tailptr);
void print_words(node *&tailptr);
void remove_words(node *&tailptr);


int main()
{
	node *tailptr = NULL;
	char choice;
	int count = 0;
	string word;
	//tailptr->next = tailptr;
	do
	{
		cout << "\nEnter Word: ";
		cin >> word;

		add_word(word, tailptr);

		cout << "\nAnother Word?: (y/n)";
		cin >> choice;
		choice = tolower(choice);

		count++;
	}while (choice != 'n');


	cout << endl << count << " words read in.\n";

	print_words(tailptr);

	remove_words(tailptr);
	return 0;
}

void add_word(string &word, node *&tailptr)
{
	node *temp = new (nothrow) node;
	temp->word = word;

	node *current = new (nothrow) node;
	node *previous = new (nothrow) node;
	if (tailptr == NULL)
	{
		temp->next = temp;
		tailptr = temp;
		return;
	}

	current = tailptr->next;
	previous = tailptr->next;
	if (temp->word.compare(current->word) <0)
	{
		temp->next = current;
		tailptr->next = temp;
		return;
	}
	
	while (current != tailptr && current->word.compare(temp->word) < 0)
	{
		previous = current;
		current = current->next;
	}
	if (current == tailptr && temp->word.compare(current->word) > 0)
	{
		temp->next = tailptr->next;
		tailptr->next = temp;
		tailptr = temp;
		return;
	}
		temp->next = current;
		previous->next = temp;
		return;
}
	
void print_words(node *&tailptr)
{
	node *temp = new (nothrow) node;
	temp = tailptr;
	if (tailptr == NULL)
	{
		cout << "Empty list";
		return;
	}
	temp = temp->next;
	do
	{
		cout << temp->word << " ";
		temp = temp->next;
	}while (temp != tailptr);
	cout << temp->word << endl;
	return;
}
void remove_words(node *&tailptr)
{
	node *temp = new (nothrow) node;
	node *current = new (nothrow) node;
	node *previous = new (nothrow) node;

	temp = tailptr;
	current = tailptr;
	previous = tailptr;

	if (tailptr->next == NULL)
		return;

	if (temp->next->next == tailptr)
	{
		temp = temp->next;
		delete temp;
		tailptr->next = tailptr;
		return;
	}
	while (current->next != tailptr)
	{
		previous = current;
		current = current->next;
	}
	previous->next = tailptr;
	delete current;

	return;
}