#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cctype>
#include <cstring>

using namespace std;
void selection_sort (int num[], int size);
void bubble (int num[], int size);
void write_out_number(ofstream &fout, int number[], int size);

int main(int argc, char *argv[])
{
	ifstream fin1;
	ifstream fin2;

	int size1;
	int size2;
	int i = 0;
	int r,l,m;

	int number1[100];
	int number2[100];

	if (argc != 3)
	{
		cout << "invalid number of arguments.   Run as 'program, text input, numbers 1, numbers 2";
		exit (1);
	}

	fin1.open (argv[1]);
	fin2.open (argv[2]);

	while (fin1 >> number1[i])
		i++;
	size1 = i;
	i = 0;

	while (fin2 >> number2[i])
		i++;
	size2 = i;

	cout << "entries from file 2 are: " << size1 << endl;

	cout << "entries from file 3 are: " <<  size2 << endl;

	ofstream fout;
	ofstream fout2;
	fout.open ("1.txt");
	fout2.open ("2.txt");

	bubble( number1, size1 );
	bubble( number2, size2 );

	int selection = 0;
	
	int found = -1;

	while (selection !=3)
	{
		cout << "search for an integer?" << endl << " 1 - file 1. 2 - file 2. 3 - quit." << endl;
	
	cin >> selection;

	if (selection == 1)
	{
		cout << "choose an int: " << endl;
		cin >> selection;
		r = size1 - 1;
		l = 0;
		m = (r+l) / 2;
		
		while (r > l)
		{
			if (selection > number1[m])
			{
				l = m+1;
				m = (r+l)/2;
			}
			if (selection == number1[m])
			{
				cout << " found at index " << m << endl;
				found = 1;
				break;
			}
			if (selection < number1[m])
			{
				r = m-1;
				m = (r+l)/2;
			}

		}
		if (found < 0)
			cout << "number not found in listed file." << endl;
	}

	if (selection == 2)
	{
		cout << "choose an int: " << endl;
		cin >> selection;
	r = size2 - 1;
	l = 0;
	m = (r+l) / 2;
		while (r > l)
		{
			if (selection > number2[m])
			{
				l = m+1;
				m = (r+l)/2;
			}
			if (selection == number2[m])
			{
				cout << " found at index " << m << endl;
				found = 1;
				break;
			}
			if (selection < number2[m])
			{
				r = m-1;
				m = (r+l)/2;
			}

		}
		if (found < 0)
			cout << "number not found in listed file." << endl;
	}
	}

	write_out_number(fout, number1, size1);
	write_out_number(fout2, number2, size2);

	fin1.close();
	fin2.close();
	fout.close();
	fout2.close();
	
	return 0;
}

void write_out_number(ofstream &fout, int number[], int size)
{
	int i = 0;
	
	while (i < size)
	{
		fout << number[i] << " ";
		i++;
	}
	return;
}
void bubble (int num[], int size)
{
	int i, j, temp;
	bool swap = false;
	for (i = 0 ; i < size-1 ; i ++)
	{
		for (j=0 ; j < size-1 ; j++)
		{
			if (num[j] > num[j+1])
			{
				temp = num[j+1];
				num[j + 1] = num[j];
				num[j] = temp;
				swap = true;
			}
		}
		if (!swap)
		return;
	}

	return;
}
void selection_sort (int num[], int size)
{
	int i, j, index;
	int temp;
	for ( i = 0 ; i < size - 1 ; i++)
	{
		index = i;
		for (j = i+1; j < size ; j++)
		{
		if ( num[j] < num[index] )
			index = j;
		}
			temp = num[index];
			num[index] = num[i];
			num[i] = temp;
		}

	return;

}