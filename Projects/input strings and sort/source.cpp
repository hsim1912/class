#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cctype>
#include <cstring>

using namespace std;
void write_out_number(ofstream fout, int number[]);

int main(int argc, char *argv[])
{
	ifstream fin1;
	ifstream fin2;

	int size1;
	int size2;
	int i = 0;

	int number1[100];
	int number2[100];

	if (argc != 3)
	{
		cout << "invalid number of arguments.   Run as 'program, text input, numbers 1, numbers 2";
		exit (1);
	}

	fin1.open (argv[1]);
	fin2.open (argv[2]);

	while (fin1 >> number1[i])
		i++;
	size1 = i;
	i = 0;

	while (fin2 >> number2[i])
		i++;
	size2 = i;

	cout << "entries from file 2 are: " << size1 << endl;

	cout << "entries from file 3 are: " <<  size2 << endl;

	ofstream fout;
	ofstream fout2;
	fout.open ("1.txt");
	fout2.open ("2.txt");

	write_out_number(fout, number1);
	write_out_number(fout2, number2);

	fin1.close();
	fin2.close();
	fout.close();
	fout2.close();
	return 0;
}

void write_out_number(ofstream fout, int number[])
{
	int i = 0;
	
	while (number[i])
	{
		fout << number[i];
		i++;
	}
	return;
}