/*-------------------------------------------------------------------
   Program: lab4_1.cpp
   Author: <Alex Wulff>
   Course: CSC 150 - <03>
   Instructor: <Fill in lab TA's name>
   Date: <9/29/11>
   Description:  This program compiles but it does not solve the
     problem described below.  Please fix it.

	 The program should determine whether is OK or not to spray 
     pesticide from an airplane.  Pesticide can be sprayed if the 
     temperature is at least 70F and the wind speed is as most 10 
     miles per hour.  The program accepts two floating point numbers 
     for the temperature and wind speed; then the program assigns the
     value true or false to the variable sprayIsOk according with the 
     above spray conditions.  Finally, the program displays 
                 "It is OK to spray now."
     if sprayIsOk is true.  Otherwise, it displays 
                 "It is NOT OK to spray now."
    

---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
using namespace std;


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         none
*********************************************************************/
int main()
{
   double temperature;      // current temperature
   double windSpeed;        // current wind speed
   bool sprayIsOk = true;   // spray condition value: T or F

   cout << "Enter temperature: "; 
   cin >> temperature;

   cout << endl << "Enter wind speed: ";
   cin >> windSpeed;

   if (temperature >= 70.00 && windSpeed <= 10)
	   sprayIsOk = true;
   else sprayIsOk = false;


   if (sprayIsOk) 
       cout << "It is OK to spray now.\n";

   else cout << "It is Not OK to spray now.\n";

return 0;
}
