#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	// request 3 sides and assign them to variables
	double a, b, c, s, area;

	cout << "Enter the length of side A: ";
	cin >> a;

	cout << "Enter the length of side B: ";
	cin >> b;

	cout << "Enter the length of side C: ";
	cin >> c;

	// check to verify that all sides do make a triangle a,b,c > 0 and (side 1)^2 <= (side 2)^2 + (side 3)^2

	if ( a <= 0 || a*a > (b*b + c*c))
	{
		cout << "The input values do not describe a triangle.";
		return 0;
	}


	if ( b <= 0 || b*b > (a*a + c*c))
	{
		cout << "The input values do not describe a triangle.";
		return 0;
	}

	if ( c <= 0 || c*c > (b*b + a*a))
	{
		cout << "The input values do not describe a triangle.";
		return 0;
	}

	s = (a+b+c)/2;

	area = sqrt(s * (s-a) * (s-b) * (s-c));

	cout << "Triangle with sides " << a << ", " << b << ", " << c << " has an area of " << area << endl;

	
	return 0;
}