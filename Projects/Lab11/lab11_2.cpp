/*--------------------------------------------------------------------
  Program: lab11_2.cpp
  Author: <Fill in your name >
  Course: CSC 150 - <Fill in lab section number>
  Instructor: <Fill in lab TA's name>
  Date: < Fill in today's date>
  Description: 
    This program when completed, should read each line of a C++ 
	file, count and display the number of lines with the C++
    reserved words in the set {if, while, switch, for}.  Assume 
    only one reserved word per line and 100 characters per line.
	The filename being examined	is read from the keyboard.

    Read through the code, fill in code or information expected by
    comment lines enclosed in angles < >, use the function
    findReservedWord(), and then compile and run the program.


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
int findReservedWord(char word[], char line[]);


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         findReservedWord()
*********************************************************************/
int main()
{
	char choice[20] = {'\0'};
	
	char word[4][7] = {"if", "while", "switch", "for"}; // reserved words
    char filename[25];      // store filename
    char line[101] ="";     // input line
    ifstream fin;           // open input file
    int cnt[4] = {0};       // counter for each reserved word
    int i;                  // loop variable


    // < Prompt for a file >
	cout << "Select Target Input File" << endl;
	cin >> choice;

	
    // < Open and check if files can be opened >
 
	fin.open(choice);


	if(!fin)
	{
		cout << "can not open file" << choice << ". Exiting program." << endl;
		exit (1);
	}

    // Process each line stored in the input file
    while (fin.getline(line, 101))
    {
        // < Select each reserved word, call findReservedWord, >
        // < and count it if it is found in a line >
		for (i = 0 ; i < 4 ; i++)
		{
			cnt[i] += findReservedWord (word[i], line);
		}
		
    }

    // < Display the counter for each word >

	for (i = 0 ; i < 4 ; i++)
	{
		cout << "Word < " << word[i] << left << " > ";
		cout << right << "    \t" << "appears in " << cnt[i] << " lines." << endl;
	}



    // < Close file >
 fin.close();

    return 0;
}


/********************************************************************
   Function: findReservedWord()
   Description:
      This function returns 1 if a reserved word is in a line, else 0.
   Parameters:    word[]: array of characters
				  line[]: line of a file being examined
   Return value:  count: int
   Called by:     main()
   Calls:         none
*********************************************************************/
int findReservedWord(char word[], char line[])
{
    int len1 = strlen(line); // compute line length
    int len2 = strlen(word); // compute word length
    int cnt = 0;             // count number of lines w/word
    int i, j;                // looping variables

    i = 0; 
	// Scan each line character
    while (i < len1)
    {
		// Check if word is in the line
        for (j = 0; j < len2; j++)
        {
            if (line[i] == word[j])
                i++;
            else
                break;
        }
		// If word is in the line then return 1
        if (j == len2)
            return 1;
        else // point to next character in the line
            i++;
    }

    return 0;

}
