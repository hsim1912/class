/*-------------------------------------------------------------------
   Program: code11_2.cpp
   Description:  
     This program displays the digits of a 4-digit number.
---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <cmath>
using namespace std;


/********************************************************************
   Function:      main
*********************************************************************/
int main()
{

    int number, temp, digit;
    int i = 0;

    // Get number
    cout << "Enter a 4-digit number: ";
    cin >> number;
        
    if (number > 9999)
        cout << "The number has more than 4 digits." << endl;
    else if (number < 1000)
        cout << "The number has less than 4 digits." << endl;
    else
        for (i = 4; i > 0; i--)
        {
            temp = int(pow(10.0, i-1));
            digit = number / temp; 
            number = number - digit * temp;
            cout << digit << "  ";
        }

    cout << "\nProgram exiting!" << endl;

	return 0;
}
