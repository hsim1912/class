/*--------------------------------------------------------------------
  Program: lab11_1.cpp
  Author: <Cracker Samson >
  Course: CSC 150 - <Fill in lab section number>
  Instructor: <Fill in lab TA's name>
  Date: < Fill in today's date>
  Description: 
    This program corrects a common typing error which is inserting 
    two spaces after a period when you should only insert one space. 
    The program reads each line from the input file input11_1.txt, 
    and replaces all double spaces after a period by a single 
    period. The modified lines are written onto the file 
    output11_1.txt. It assumes that each line has a maximum of 
    250 characters. 


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <fstream>
using namespace std;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
void fixTypingError(char [], char ch [][1]);


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         fixTypingError()
*********************************************************************/
int main()
{
	char dot[4][1] = {'.', ',', ';', ':'};                  // character usually followed by 2 spaces
    char line[251] = "";            // input line
    ifstream fin;                   // input file
    ofstream fout;                  // output file

    //open and test files
    fin.open( "input11_1.txt" ); 
    fout.open("output11_1.txt");
    if(!fin || !fout)
    {
        cout << "Unable to open input/output files." << endl;
        return -1;
    }

    // Process each line stored in the input file
    while (fin.getline(line, 251))
    {
      // Remove extra space after a period or dot
        fixTypingError(line, dot);
        // Save it in the output file
        fout << line << endl;
        // Display the corrected line
        cout << line << endl;

    }

    // Close files
    fin.close();
    fout.close();

    return 0;

}


/********************************************************************
   Function: fixTypingError
   Description:
      This function removes the extra space after a punctuation
      character (e.g., a period) in a line.
   Parameters:    line[]: array of characters
				  ch: a single punctuation character
   Return value:  void
   Called by:     main()
   Calls:         none
*********************************************************************/
void fixTypingError(char line[], char ch[][1])
{
    int len = strlen(line); // compute line length
    int i, j;               // looping variables
    char temp[251] = "";    // temporary storage

    // Save line into temporary storage
    strcpy (temp, line);
    i = 0;
    // Check every character but the last
    while (i < len - 1)
    {
		// If dot was found followed by 2 spaces ...
        if ( (temp[i] == ch[0][0] || temp[i] == ch[1][0] || temp[i] == ch[2][0] || temp[i] == ch[3][0]) && temp[i+1] == ' ' && temp[i+2] == ' ')
        {
            // then remove the extra space
            for (j = i+2; j < len - 1; j++)
                temp[j] = temp[j+1];
            len--;
            temp[len] = '\0';

        }
        else // dot followed by 2 spaces was not found
		i++;

	}

	
    // Modified line is saved back in line array
    strcpy(line, temp);
}

