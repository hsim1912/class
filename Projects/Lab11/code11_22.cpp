// Program that reads a file with a list of unsorted integers and
// sort them with a bubble (or selection) sort.

#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;

const int SIZE = 100;

void bubble_sort(int [], int);
void selection_sort(int [], int n);

int main( int argc, char *argv[] )
{
   int list[SIZE];
   int counter = 0;
   int value;
   int i;
   ifstream fin;

   if(argc != 2)
   {
      cout << "Program requires a file name.";
      cout << endl;
      return -1;
   }

   fin.open(argv[1]);
   if(!fin)
   {
      cout << "Error opening input file " << argv[1] << endl;
      return -1;
   }

   while(fin >> value)
   {
      list[counter] = value;
      counter++;
   }

   cout << "Original array: " << endl;
   for(i = 0; i < counter; i++)
      cout << setw(3) << list[i];
   cout << endl;

   bubble_sort(list,counter);
   // selection_sort(list, counter);

   cout << "Sorted array: " << endl;
   for(i = 0; i < counter; i++)
      cout << setw(3) << list[i];
   cout << endl;

   fin.close();
   return 0;
}

// bubble sort routine
void bubble_sort(int a[], int n)
{
   int i,j;
   int temp;
   int swap_made;

   for( i = 0; i < n; i++ )
   {
      swap_made = 0;
      for( j=0; j < n-i-1; j++ )
      {
         if ( a[j] > a[j+1] )
         {
            temp   = a[j];
            a[j]   = a[j+1];
            a[j+1] = temp;
            swap_made = 1;
         }
      }

      if( !swap_made )
         return;
   }
}


// selection sort
void selection_sort(int a[], int n)
{
   int i, j;
   int temp;
   int swap_index;

   for( i=0; i < (n - 1); i++ )
   {
      swap_index = i;
      for(j = i + 1; j < n; j++ )
      {
         if ( a[j] < a[swap_index] )
            swap_index = j;
      }
      temp = a[swap_index];
      a[swap_index] = a[i];
      a[i] = temp;
   }
}

