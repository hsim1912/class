/*************************************************************************//**
 * @file 
 *
 * @brief This file contains the main function which will take comands from
 * the comand line and interpret them as arguments for spell checking of a
 * text file. It also contains the function for reading in the dictionary file
 * as well as the function for reading in the file to be spell checked. It 
 * contains the function that goes through and actually spell checks the file.
 * It also contains the function that closes the files, the function that 
 * checks the command line arguments, and the function that prints the usage 
 * menu if there is an error with one of read ins or the command arguments.
 *
 * @mainpage Program 2 - Text-file Spell Checker
 * 
 * @section course_section CSC 250
 *
 * @authors Alex Wulff (part I) & Andrew Rossow (part II)
 * 
 * @date February 29th, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program will use a specified text dictionary to test for
 * correct spelling against a file to be corrected.  The file cross checked
 * with the spelling from the dictionary will be re-written to a file with the
 * same base name, but with a '.new' extension.  Should any word found in the
 * file to be checked be not in the dictionary, the word will be presented to
 * the user for confirmation of the spelling.  The word can here be corrected
 * manually.  Should the spelling be correct or not, the word will hence forth
 * be added to the dictionary, to reduce further prompting for the user.
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog2.exe dictionary.txt file_to_be_spell_checked.txt 
   d:\> c:\bin\Prog2.exe dictionary.txt file_to_be_spell_checked.txt
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug new output file prints last word twice and doesn't put in the new
 *      lines.
 * 
 * @todo Fix output file problem
 * 
 * @par Modifications and Development Timeline: 
 * @verbatim 
 * Date          Modification 
 * ------------  -------------------------------------------------------------- 
 *
 *	3/12/12		- Started looking into the STL list class.
 *
 *	3/15/12		- Converted the read dictionary function to implement the STL
 *				  list class and began converting the spell checker.
 *
 *	3/16/12		- Converted the close function to use the STL list class and 
 *				  continued trying to fix the error with the spell checker 
 *				  function so that it wouldn't cause the program to crash.
 *
 *	3/17/12		- Fixed the spell checker and altered it so that it would add
 *				  words into the dictionary at the index of the new word rather
 *				  than the index of the mispelled word. 
 * 
 *	3/18/12		- Met with Alex and finalized code on Prog2b.
 *
 *  3/19/12		- Documented code and submitted program
 *
 * @endverbatim
 *
 *****************************************************************************/
/*****************************************************************************
|								THE INCLUDES								 *
*****************************************************************************/
#include <iostream>
#include <list>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

/******************************************************************************
|							FUNCTION PROTOTYPES 							  |
******************************************************************************/
void read_dictionary( fstream &dict, list < string > thelists []);
void spell_check (ifstream &readme, ofstream &readme_new, 
				  fstream &dict, list<string>thelists[]);
void close(ifstream &readme, ofstream &readme_new, fstream &dictionary, 
		  list<string>thelists[], string dic);
bool open_readme(string &read, ifstream &readme, ofstream &readme_new);
bool open_dict(string &arg, fstream &dictionary);
bool argcheck(int count,char *dic,char *read);
void string_grab(string &word, int number, char word_arr[]);
void usage_menu();

/**************************************************************************//**
 * @author Andrew Rossow
 *
 * @par Description:
 * This is the starting point to the program.  It will take commands from the 
 * command promt or an IDE's debug functions.  The program will take these for
 * specification of what dictionary file to use and which file to spell check.
 * The program will go on to read in the dictionary into a series of lists
 * predifined by the STL list class alphabetical order.  Once complete, the 
 * program will check these words against the words found in the file being 
 * spell checked, and if needed will prompt the user to confirm spelling and 
 * add any new words to the dictionary upon completion, while freeing the lists
 * that were created to hold the dictionary words.
 *
 * @param[in] argc - the number of arguments from the command prompt.
 * @param[in] argv - a 2d array of characters containing the arguments.
 *
 * @returns 0 - upon completion of spell check as successful.
 * @returns 1 - if the arguments are invalid.
 * @returns 2 - dictionary file can not be opened.
 * @returns 3 - file to spell check and the new file fail to be opened.
 *
 *****************************************************************************/

int main ( int argc, char **argv )
{
	list < string > thelists [26];
	string dic = argv[1];
	string read = argv[2];
	fstream dict;			// dictionary file
	ifstream readme;		// file to be spell checked
	ofstream readme_new;	// new spell checked file
	fstream dictionary;

	if (!argcheck(argc, argv[1], argv[2]))	// verify arguments
		exit (1);
	if (!open_dict(dic, dict))	//verify opening of dictionary
		exit (2);
	if (!open_readme(read, readme, readme_new)) // verify readme opens
		exit (3);

	cout << "Spell checking your file. "<< endl; 
	read_dictionary( dict, thelists );

	spell_check(readme, readme_new, dict, thelists );

	cout << "Writing new files." << endl;
	close( readme, readme_new, dictionary, thelists, dic);

	cout << "Program is finished spell checking";

	return 0;
}

/*!**************************************************************************//**
 * @author Andrew Rossow & Alex Wulff
 *
 * @par Description: This function reads the words from the specified 
 * dictionary into an array of STL lists aranged by the first letter of the
 * word. Before reading the word in, the words will be converted to lowercase.
 *
 * @param[in]  dict: the file used to hold all of the words
 * to be used as a dictionary for spell checking.
 * @param[in]  thelists: STL list class' list that holds the words read in from
 * dictionary file.
 *
 * @returns - none.
 *
 ******************************************************************************/
void read_dictionary( fstream &dict, list < string > thelists [])
{	
	int index;
	string word;
	char first_letter[1];

	// executes while the dictionary file contains more words
	while ( dict >> word )
	{
		// looks at the first letter of the dictionary word to get the
		// appropriate indexing for the list
		
		word.copy( first_letter, 1 );
		first_letter[0] = tolower(first_letter[0]);
		word.replace (0, 1, first_letter, 1 );
		index = int (first_letter[0]) - int ('a');


		// uses the STL list class push_front to insert the word at the 
		// beginning of the list for that alphabet index and sorts that list
		// after the insert
		thelists[index].push_front(word);
		thelists[index].sort();
	}
}

void spell_check (ifstream &readme, ofstream &readme_new, fstream &dict, list<string>thelists[])
{
	string word, word_copy, word_new;
	int index;
	int new_words=0;
	char punct, choice, first_letter[1];
	bool found;

	// checks the text file while it is not at the end
	readme >> word;
	do
	{
		found = false;

	if (isdigit(word[0]))
	{
		readme_new << word << " ";	//check for common whitespace
		if (readme.peek() == '\n')
			readme_new << '\n';
		if (readme.peek() == '\t')
			readme_new << '\t';
	}

	else 
	{
		word_copy = word;
		word_new = word_copy;
		word.copy(first_letter, 1);
		first_letter[0] = tolower(first_letter[0]);
		word.replace (0, 1, first_letter, 1);
		index = int (first_letter[0]) - int ('a');
		if (ispunct(word.c_str()[word.size() -1]))
		{
			punct = word_copy.c_str()[word_copy.size() -1];
			word.resize (word.size() - 1);
			word_copy = word;
			word_new = word_copy;
		}
				else punct = 'a';
		
		list<string>::iterator temp;
	
		// goes through list of the appropriate index and compares it with the
		// word being spell checked
		for( temp = thelists[index].begin(); temp != thelists[index].end(); ++ temp )
		{
			if (word.compare(*temp) == 0)
			{
				found = true;
				break;
			}
		}
		// if the word being spell check wasn't found in the dictionary file
	if ( found != true )	
	{
		cout << "Possible misspelling.  Is " << word <<
			" spelled correctly? (y/n)" << endl;
		cin >> choice;
		new_words++;
		choice = tolower(choice);
	   
		while (choice != 'n' && choice != 'y')
        {
            cout << "Word not found in Dictionary.  Is " << word <<
            " spelled correctly? (y/n)" << endl;
            cin >> choice;
			choice = tolower(choice);
        }

		if (choice == 'y')
		{
			thelists[index].push_front(word);
			thelists[index].sort();
		}
		if ( choice == 'n' )
		{
				cout << "What is the correct spelling of " << word << "?"<< endl;
				cin >> word_new;
				word_copy = word_new;
				word_new.copy(first_letter, 1);
				first_letter[0] = tolower(first_letter[0]);
				index = int (first_letter[0]) - int ('a');
				thelists[index].push_front(word_new);
				thelists[index].sort();
		}
	}
	word_new.resize(word_new.size());
	if (ispunct((int) punct))
	{
		readme_new << word_new << punct << " ";	
	}
	
	else
	{
		readme_new << word_new << " ";	
	}
	if (readme.peek() == '\n')
			readme_new << '\n';
	if (readme.peek() == '\t')
			readme_new << '\t';
	}
	}while (readme >> word);

	cout << new_words << " New words written." << endl;
	
	// makes sure there is no duplicates in the dictionary file
	thelists[index].unique();
}

/*!**************************************************************************//**
 * @author Alex Wulff & Andrew Rossow
 *
 * @par Description: This function will close the files readme, readme_new, 
 * and dictionary.  While freeing the nodes, the dictionary file will be 
 * written out with the current words in every node, so the new dictionary will
 * be in alphabetical order.
 *
 * @param[in]  readme: Original input that was spell 
 * checked.  This will be closed out of use here.
 * @param[in]  readme_new: Modified file after spell
 * checking.  This too will be closed here.
 * @param[in]  dictionary: will be closed and re-opened
 * for writing out the new words as well as originals read thru from 
 * initiation.
 * @param[in]  thelists: used to retreive words from the STL lists
 * and pull words directly for write out and deletion.
 * @param[in]  dic: Used so the output dictionary after being
 * closed can be re-opened and then written out to.
 *
 *
 * @returns - none.
 *
 ******************************************************************************/
void close(ifstream &readme, ofstream &readme_new, fstream &dictionary, 
		   list<string>thelists[], string dic)
{
	int i;
	string word;

	// closes the files
	readme.close();
	readme_new.close();
	dictionary.close();
	
	//opens the dictionary file for output and writes the new dictionary
	dictionary.open(dic, ios::out | ios::trunc);
		for (i = 0; i < 26; i ++)
		{
			// makes sure that it doesn't try to print out an empty list
			while ( !thelists[i].empty() )
			{
				dictionary << thelists[i].front() << endl;
				thelists[i].pop_front();
				thelists[i].unique();
			}
		}
	dictionary.close();
	return;
}

/*!**************************************************************************//**
 * @authors Alex Wulff & Andrew Rossow
 *
 * @par Description: Using a string designated as the file name to be spell 
 * checked, this function will open the file and check for varification of 
 * a successful open.  Should it fail a binary response will be sent back to
 * the function that called it.
 *
 * @param[in] read: String containing the name of the file to be read in and
 * spell checked.
 * @param[in] readme: ifstream to be the object the program will reference for
 * the spell checking process.
 * @param[in] readme_new: Ofstream to hold the spellchecked file and to write
 * out the new data upon completion
 *
 * @returns - True: if the files open correctly
 * @returns - False: should the file to be opened not be found or could not
 * be opened.
 *
 ******************************************************************************/
bool open_readme(string &read, ifstream &readme, ofstream &readme_new)
{
	char open[20];
	int size = read.size();
	strcpy_s(open, read.c_str());
	readme.open(open, ios::in);

	// checks if the readme file is open
	if (!readme)
	{
		cout << "Error opening readme. Please check disk space and access." 
			<< endl;
		return false;
	}
	read.erase (size - 4);
	read.append (".new");
	strcpy_s(open, read.c_str());
	readme_new.open( open, ios::out | ios::trunc);

	// makes sure the new output file is open
	if (!readme_new)
	{
		cout << "Error opening new readme. Please check disk space and access."
			<< endl;
		return false;
	}
	return true;
}

/*!**************************************************************************//**
 * @authors Alex Wulff & Andrew Rossow
 *
 * @par Description: Validates the argument passed through main and uses it to
 * open a file for dictionary.
 *
 * @param[in]  arg: used for the argument containing the name
 * of the dictionary file.
 * @param[in]  dictionary: used to open the dictionary into the name dictionary
 *  and return it back to main for further manipulation.
 * @param[out]  dictionary: used to open the dictionary into the name dictionary
 *  and return it back to main for further manipulation.
 *
 * @returns - True if dictionary opens correctly.
 * @returns - False should dictionary not open. And prompts a message.
 *
 ******************************************************************************/
bool open_dict(string &arg, fstream &dictionary)
{
	char open[20];
	strcpy_s(open, arg.c_str());
	dictionary.open( open, ios::in | ios:: out | ios::app );
	if (!dictionary)
	{
		cout << "Dictionary not found or not openable.  Please validate and" 
			<< "try again." << endl;
		return false;
	}	
	return true;
}

/*!**************************************************************************//**
 * @author Alex Wulff & Andrew Rossow
 *
 * @par Description: This function uses a count provided by main to determine
 * if usage has been set correctly.  Should the count of arguments passed be 
 * any number other than three (program name, dictionary, target to spellcheck)
 * it can be assumed that the user has provided incorrect arguments, so a usage
 * menu will be presented.
 *
 * @param[in] count: a simple integer representing the number of
 * arguments from command prompt.
 * @param[in] dic: dynamically sized array of characters, containing the user's
 * choice of dicitionary file.
 * @param[in] read: dynamically sized array of characters, containing the 
 * user's requested file to perform the spell checking on.
 *
 * @returns - True: if the number of arguments is 3, and both files are in a
 * ".txt" extension.
 * @returns - False: if the number of arguments is not 3, or the files are
 * in a non-compatable format.
 *
 ******************************************************************************/
bool argcheck(int count,char *dic,char *read)
{
	char ext1[25], ext2[25];
	string dictionary = dic;
	string read_file = read;

	// makes sure command line arguments are correct
	if (count != 3)	
	{
		usage_menu();
		return false;
	}
	string_grab( dictionary, 5, ext1);
	string_grab( read_file, 5, ext2);
	dictionary.resize(dictionary.size());
	read_file.resize(dictionary.size());

	// makes sure both files have the appropriate extension
	if (dictionary == ".txt" && read_file == ".txt")
	{
		return true;
	}

	//prints the usage menu if the file extensions are not correct
	usage_menu();
	return false;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Outputs correct usage information to the user.  
 *
 * @param[in] : none.
 *
 * @returns - none.
 *
 ******************************************************************************/
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\\... prog2.exe [dictionary file] [readme file]" << endl << endl;
	cout << "Both files must be in txt extensions" << endl;
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Uses a specified number to extract the last characters of
 * a string.
 *
 * @param[in]  word : used for initial string and return to
 * user after processing.
 * @param[out]  word : returned modified value after processing.
 * @param[in]  number: the specified number of characters to take off
 * the end of the string.
 *
 * @returns - none.
 *
 ******************************************************************************/
void string_grab(string &word, int number, char word_arr[])
{
	int i;
	for (i = 1; i < number; i++)
	{
		word_arr[i] = word.c_str()[word.size() -i];
	}
	while (i < number)
	{
		i++;
	}
	word_arr[i] = '\0';
	word_arr = _strrev(word_arr);
	word = word_arr;
	word.resize(number -1);
	return;
}