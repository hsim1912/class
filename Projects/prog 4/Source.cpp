/***************************************************************************//**
 * @file prog4.cpp - Temerature sorts and reasignments
 *
 * @mainpage Program 4 - Temerature sorts and reasignments
 *
 * @section course_section Course Information
 *
 * @author - Alex Wulff
 *
 * @date - 12/09/11
 *
 * @par Instructor:
 *         Dr. Schrader
 *
 * @par Course:
 *         CSC 150 � Section 5 � 3:00 pm
 *
 * @par Location:
 *         McLaury - Room 302
 * 
 * @section program_section Program Information
 * 
 * @details - This program reads in random days and temperatures from an 
 * argument.  From there, the program places these temps into two arrays (one 
 * for high for the day one for low).  It then runs computations on the highs 
 * and lows, and runs a menu for easy access to the data.  Upon completion, it 
 * writes the information to a file (specified by the opening arguments).
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      None 
 *
 * @par Usage:
   @verbatim 
   c:\> prog3.exe, inputfile.txt, outputfile.txt
   d:\> c:\bin\prog3.exe, inputfile.txt, outputfile.txt
   @endverbatim
 *
 * @par Input and output files 
 * Declared within as fin and fout respectively.
 * These files are actually opened with names passed into as arguments using
 * argv[].
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @bug I have not assigned any method of rounding on the averages.  
 * It currently is simply integer devision.
 *
 ******************************************************************************/


/*******************************************************************************
 *             Constant Variables, Defines and Enums
 ******************************************************************************/
/*!
 * @brief added to avoid MS VC++ C string library warnings
 */
#define _CRT_SECURE_NO_DEPRECATE

/*!
 * @brief added to avoid MS VC++ C string library warnings
 */
#define _CRT_NONSTDC_NO_DEPRECATE

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <cctype>

using namespace std;

/*!
 * @brief Name of months defined globaly for easy access.
 */

char month_name[12][20]= {"January", "February", "March", "April", "May", 
	"June", "July", "August", "September", "October", "November", "December"};

void all_highs ( int high[][40] );
void all_lows (int low[][40] );
void calc_stats(int high[][40], int low[][40]);
void display_stats( int high[][40], int low[][40], int month);
void main_menu( int high[][40], int low[][40]);
bool open_input(ifstream &fin, char string1[]);
int read_data( ifstream &fin, int high[][40], int low[][40] );
int write_data(ofstream &fout, int high[][40], int low [][40]);
void yearly_high_low(int high[][40], int low[][40]);

 /*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This is the starting point to the program.  It will retreive a couple
 * of arguments from comand prompt, test to make sure 3 total commands are listed. 
 * After this, the program then uses each argument to name input and output
 * files, which are tested in a function called open_input.  If failure to open occurs, 
 * open_input returns false, so main can notify the user of possible non-existance of input.
 * After opening and testing for success of opeining the files with a function 
 * called read_data; two 2d arrays are then loaded with the information gathered from the
 * input file, using the first two integers as indexes, and finaly returns the number
 * read in to be compared to 365 in main.  If the input is not exactly 365, the program
 * Will report the data as corrupt and exit.  Presuming success, main then opens a 
 * non-valued function that modifies both the high and low data to calculate and assign
 * values for the highest, lowest and average.  After modifying the information, a
 * menu is called to offer easy to read statistics on the yearly data arrays.  When the 
 * menu is canceled, main outputs to the screen the results of write_data which
 * counts the amount of entries written.
 *
 * @param[in] (value) argc - the number of arguments from the command prompt.
 * @param[in] (reference) argv[1] - name of input file.
 * @param[in] (reference) argv[2] - name of output file.
 *
 * @returns 1 incorrect number of arguments
 * @returns 2 can not open input file
 * @returns 3 incorrect number of days (corrupt input)
 * @returns 4 failed write out
 *
 ******************************************************************************/


int main(int argc, char *argv[])
{
	const int sizeMonth = 15;	// Define constant integers for my arrays.
	const int sizeDay = 40;

	if (argc !=3)				// Consult and verify number of arguments.
	{
			cout << "Incorrect amount of arguments. Please re-run as program,";
			cout << "input, output. Exiting...";
			exit (1);
	}

	int low[sizeMonth][sizeDay];	// Create two arrays that when kept parallel will show High and 
	int high[sizeMonth][sizeDay];	// Low for specified month and dau (indexed).

	ifstream fin;					// Assign input and output file names.
	ofstream fout;

	if (open_input (fin, argv [1] ))	// Test for opening the input file with argument 1
	{
		cout << "Can not access file for input.  Please check for corruption." << endl;
		exit (2);
	}

	fout.open( argv[2] );				// Use argument 2 as name of output file... open it.

	if (read_data( fin, high, low) != 365)	// Read in data and verify number of entries. 
	{										// If entries do not equal 365.  Report and exit.
		cout << " Data provided is corrupt. Incorrect amount of records." << endl;
		fin.close();
		exit (3);
	}

	calc_stats(high, low);	// Calculate highest, lowest, and average of highs and lows
							// and store these to the end of the arrays.
	main_menu(high, low);	// Pass the data through for further manipulation.

	 fin.close();			// Close the input as it will not be needed hence forth.

	 if (write_data( fout, high, low) != 365)
	 {
		 cout << "Error writing file!";
		 exit (4);
	 }
	 fout.close();			// Close the output as completed.
	
	return 0;				
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function walks through each month pulling the last three indexes of each
 * column and displays them.  These values are for the highest, lowest and 
 * average of the highs in the month.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that will have 
 * the highs, lows, and average of highs in the month.
 *
 * @returns - nothing
 *
 ******************************************************************************/

void all_highs ( int high[][40])
{
	int i = 0;
	for ( i; i <12; i++)	// Walk through outputting the highs, lows and averages for the months.
	{
		cout <<"\n" << left << setw(12) << month_name[i] << right << setw(10); 
		cout << "Highest:" << setw(6) << high[i][37] << setw(10) << "Lowest:"; 
		cout << setw(6) << high[i][38] << setw(15) << "Average:"; 
		cout << high[i][39] << endl;
	}
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function walks through each month pulling the last three indexes of each
 * column and displays them.  These values are for the highest, lowest and 
 * average of the lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * will have the highs, lows, and average of lows in the month.
 *
 * @returns - nothing
 *
 ******************************************************************************/
void all_lows ( int low[][40])
{
	int i = 0;
	for ( i; i <12; i++)	// Walk through outputting the highs, lows and averages for the months.
	{
		cout <<"\n" << left << setw(12) << month_name[i] << right << setw(10); 
		cout << "Highest:" << setw(6) << low[i][37] << setw(10) << "Lowest:"; 
		cout << setw(6) << low[i][38] << setw(15) << "Average:" << low[i][39] << endl;
	}
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function walks through each month pulling the values for highs and lows
 * and runs them to see what is highest, lowest, and computes averages for each
 * array.  After completed the new values are put into the array in the last
 * three locations, per row.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * will have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * will have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @returns - nothing
 *
 ******************************************************************************/

void calc_stats(int high[][40], int low[][40])
{
	int i = 0;				// Assign variabls so averages, highests and lowests for 
	int j = 0;				// each month can be filled.
	double avgLow = 0;
	double avgHigh = 0;
	double sumLow;
	double sumHigh;

	for ( i ; i <12; i++)	// Walk through for each month.
	{
	int minLow = low[i][3];
	int minHigh = high[i][3];
	int maxLow = low[i][3];
	int maxHigh = high[i][3];
	sumHigh = 0;
	sumLow = 0;
		if ( i == 1 )		// Walk through for February.
		{
			for (j ; j< 28 ; j++)
			{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])	// comapre minimum highs
			{
				minHigh = high[i][j];	// assign
			}
			if (minLow > low[i][j])		// compare minimum lows
			{
				minLow = low[i][j];		// assign
			}
			if (maxHigh < high[i][j])	// compare max highs
			{
				maxHigh = high[i][j];	// assign
			}
			if (maxLow < low[i][j])		// compare max lows
			{
				maxLow = low[i][j];		// assign
			}
			}		// find averages, and then assign values into array
		avgHigh = sumHigh / 28;
		avgLow = sumLow / 28;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}
		else if ( i == 0 || i == 2 || i == 4 || i == 6 || 
			i == 7 || i == 9 || i == 11 )
		{				// Walk through for all 31 day months.
			for (j ; j < 31 ; j ++)
		{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])	// comapre minimum highs
			{
				minHigh = high[i][j];	// assign
			}
			if (minLow > low[i][j])		// compare minimum lows
			{
				minLow = low[i][j];		// assign
			}
			if (maxHigh < high[i][j])	// compare max highs
			{
				maxHigh = high[i][j];	// assign
			}
			if (maxLow < low[i][j])		// compare max lows
			{
				maxLow = low[i][j];		// assign
			}
		}		// find averages, and then assign values into array
		avgHigh = sumHigh / 31;
		avgLow = sumLow / 31;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}
		else if ( i == 3 || i == 5 || i ==  8 || i== 10)
		{					// Walk through for 30 day months
			for (j ; j < 30 ; j ++)
			{
			sumHigh += high[i][j];
			sumLow += low[i][j];
			if (minHigh > high[i][j])	// comapre minimum highs
			{
				minHigh = high[i][j];	// assign
			}
			if (minLow > low[i][j])		// compare minimum lows
			{
				minLow = low[i][j];		// assign
			}
			if (maxHigh < high[i][j])	// compare max highs
			{
				maxHigh = high[i][j];	// assign
			}
			if (maxLow < low[i][j])		// compare max lows
			{
				maxLow = low[i][j];		// assign
			}
		}		// find averages, and then assign values into array
		avgHigh = sumHigh / 30;
		avgLow = sumLow / 30;
		high[i][39] = avgHigh;
		high[i][38] = minHigh;
		high[i][37] = maxHigh;
		low[i][39] = avgLow;
		low[i][38] = minLow;
		low[i][37] = maxLow;
		}
		j=0;
	}
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function walks through each month's values both high and low, and finds 
 * the lowest low, the highest high and reports the values of each with the days
 * on which these occur.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @returns - nothing
 *
 ******************************************************************************/

void display_stats( int high[][40], int low[][40], int month)
{			// simple display of months and the stats therefor
	cout << "\n" << month_name[month] << endl << "Highs" << endl;
	cout << setw(6) << "Highest: "<< setw(6) << high[month][37];
	cout << setw(6) << " Lowest: " << setw(6) << high[month][38];
	cout << setw(6) << " Average: " << setw(6) << high[month][39] << endl;
	cout <<  endl << "Lows" << endl;
	cout << setw(6) << "Highest: "<< setw(6) << low[month][37];
	cout << setw(6) << " Lowest: " << setw(6) << low[month][38];
	cout << setw(6) << " Average: " << setw(6) << low[month][39] << endl;
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function creates a menu that allows a user to choose between options of 
 * viewing all month's highs, all month's lows, viewing the stats of a single month,
 * viewing the year's stats, or quit.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * have the highs for the month.  Used for further passing for function use due to
 * the scope.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * have the lows for the month.  Used for further passing for function use due to
 * the scope.
 *
 * @returns - nothing
 *
 ******************************************************************************/

void main_menu( int high[][40], int low[][40] )
{		//Declare variables
	char selection;
	int month;
		
	do
	{
		cout << "\nTemperature Data Viewer" << endl; 
		cout << setw(23) << setfill('-') << "-"  << endl << setfill(' ');
		cout << "A. View all months' Highs" << endl;
		cout << "B. View all months' Lows" << endl;
		cout << "C. View single month's stats" << endl;
		cout << "D. View yearly high and low" << endl;
		cout << "Q. Quit program" << endl << "Option: ";

		cin >> selection;

		selection = tolower (selection);	// convert any uppercase to lower
		
	if (selection == 'a')
	{
		cout << endl;
		all_highs ( high );
	}
	if (selection == 'b')
	{
		cout << endl;
		all_lows( low );
	}
	if (selection == 'c')
	{
		cout << "Select month (1 -12): ";
		cin >> month;
		while (month < 1 || month > 12)		// Error check for month
		{
			cout << endl;
			cout << "Please select as 1 for January, 2 for Feburary, etc. (1-12): ";
			cin >> month;
		}
		month -=1;
		display_stats (high, low, month);
		cout << endl;
	}
	
	if (selection == 'd')
	{
		cout << endl;
		yearly_high_low(high, low);
	}
}while ( selection != 'q' );		// loop until quit is requested.
	cout << "writing records, then exiting." << endl;
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function creates a menu that allows a user to choose between options of 
 * viewing all month's highs, all month's lows, viewing the stats of a single month,
 * viewing the year's stats, or quit.
 *
 * @param[in] (reference) string1[] - this string is the target of our input as
 * defined from command prompt.
 *
 * @returns true  - if read-in is a fail.
 * @returns false - if the read-in is successful.
 *
 ******************************************************************************/

bool open_input(ifstream &fin, char string1[] )
{

	fin.open( string1 );
	
	if (!fin)
	{
		return true;
	}
	return false;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function takes two empty arrays and fills them with the data from the 
 * input file.  The read in is done as: month, day, high and low.  
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @returns integer of number of entries read in.
 *
 ******************************************************************************/

int read_data( ifstream &fin, int high[][40], int low[][40] )
{		// declare locals.  Keep var names same as main for easy use.
int highTemp, lowTemp, i, j, count = 0;
		// start first read in.
	fin >> i;
	fin >> j;
	fin >> highTemp;
	fin >> lowTemp;

	high[i-1][j-1] = highTemp;
	low[i-1][j-1] = lowTemp;
	count++;

	while (fin >> i >> j )	// As long as we can read in, continue to assign values to arrays
{

	fin >> highTemp;
	fin >> lowTemp;

	high[i-1][j-1] = highTemp;
	low[i-1][j-1] = lowTemp;
	count++;
}
return count;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function takes two filled arrays with high and low data.  These values 
 * will be stepped through and output to a text file in the format read in.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @returns integer of number of entries read in.
 *
 ******************************************************************************/

int write_data(ofstream &fout, int high[][40], int low [][40])
{		// assign local counting variables
	int i = 0;
	int j = 0;
	int count = 0;
	for ( i; i <12; i++)
	{
		if ( i == 1 )
		{

		for (j ; j< 28 ; j++)	// write out for February
		{
			fout << i + 1 << " " << j+1 << " ";
			fout << high[i][j] << " " << low[i][j] << endl;
			count ++;
		}
		}

		else if ( i == 0 || i == 2 || i == 4 || i == 6 || 
			i == 7 || i == 9 || i == 11 )
		{		// write out for 31 day months
			for (j ; j < 31 ; j ++)
			{
			fout << i + 1 << " " << j+1 << " " << high[i][j];
			fout << " " << low[i][j] << endl;
			count ++;	
			}
		}
		else if ( i == 3 || i == 5 || i ==  8 || i== 10)
		{	// write out for 30 day months
			for (j ; j < 30 ; j ++)
			{
			fout << i + 1 << " " << j+1 << " " << high[i][j];
			fout << " " << low[i][j] << endl;
			count ++;	
			}
		}		
		j=0;
	}
		return count;	// count for amount of successful writes.
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function walks through the yearly values (both high and low), and finds 
 * the lowest low, the highest high and reports the values of each with the months
 * days on which these occur.
 *
 * @param[in] (reference) high[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @param[in] (reference) low[][40] - A two dimensional array of integers that
 * have the highs for the month and room for the high, lows, and average
 * of lows in the month.
 *
 * @returns - nothing
 *
 ******************************************************************************/

void yearly_high_low(int high[][40], int low[][40])
{		// assign counting variables
	int min = low[0][38];
	int max = high[0][37];
	int i = 0;
	int j = 0;
	int indexMonthLow = 0;
	int indexDayLow = 0;
	int indexMonthHigh = 0;
	int indexDayHigh = 0;
	int firstHighDay = 0;
	int firstHighIndex = 0;
	int firstLowDay = 0;
	int firstLowIndex = 0;


	for (i ; i < 12 ; i++)
	{		// Run through for comparisons and assignments
		if (min > low[i][38])
		{
			min = low[i][38];
			indexMonthLow = i;
		}
		if (max < high [i][37])
		{
			max = high [i][37];
			indexMonthHigh = i;
		}
	}

	for (j=0 ; j < 31 ; j++)
	{
		if (low[indexMonthLow][j] == min)
		{
			indexDayLow = j;
		
		if (firstLowDay == 0)
		{
			firstLowDay = indexDayHigh;
			firstLowIndex = j;
		}
		}
		if (high[indexMonthHigh][j] == max)
		{
			indexDayHigh = j;
				if (firstHighDay == 0)
		{
			firstHighDay = indexDayHigh;
			firstHighIndex = j;
		}
		}
	}
		cout << "Highest temperature was on " << month_name[indexMonthHigh];
		cout << " " << firstHighIndex + 1;
		cout << " and the temperature was " << high[indexMonthHigh][indexDayHigh];
		cout << endl;
		cout << "Lowest temperature was on " << month_name[indexMonthLow];
		cout << " " << firstLowIndex + 1;
		cout << " and the temperature was " << low[indexMonthLow][indexDayLow] << endl;
		return;
	}