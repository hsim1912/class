#include <iostream>

using namespace std;
	// Function of main is to receive 3 var, produce a sum and average, print the smallest and largest
int main()
{
	//Declare var
	double num1, num2, num3;
	double max, min, sum;
	double avg;

	//request input from user

	cout << "Enter value for first number:" << endl;
	cin >> num1;

	cout << "Enter value for second number:" << endl;
	cin >> num2;

	cout << "Enter value for thrid number:" << endl;
	cin >> num3;

	//Calculate sum and display

	sum = num1 + num2 + num3;

	cout << "your sum is:" << sum << endl;

	// Calculate average and display

	avg = (num1 + num2 + num3) / 3.0;

	cout << "your average is:" << avg << endl;

	// define a minimum and determine lowest number

	min = num1;
	max = num1;

	if (num1 > num2)
		min = num2;

	if (min > num3)
		min = num3;

	//define max and determine it



	if (num1 < num2)
		max = num2;

	if (max < num3)
		max = num3;

	// display minimums and maximums.

	cout << "Your minimum number is:" << min << endl;

	cout << "Your maximum number is:" << max << endl;

	return 0;
}