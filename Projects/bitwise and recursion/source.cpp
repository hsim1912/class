#include <iostream>
#include <cctype>
#include <iomanip>
#include <string>

using namespace std;


int compare(char s1[], char s2[], int index, int diff);
int main()
{
	char s1[20] = "\0";
	char s2[20] = "\0";
	int index = 0;
	int difference = 0;
	cin >> s1 >> s2;
	
	difference = compare (s1,s2,index,difference);

	cout << difference;

	return 0;
}
int compare(char s1[], char s2[], int index, int diff)
{
	if ( diff == 0 && s1[index] == '\0' )
		return 0;

	diff = (int) s1[index] - (int) s2[index];

	diff += compare(s1, s2, index + 1, diff);

	return diff;
}