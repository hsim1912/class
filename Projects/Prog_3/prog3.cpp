/*************************************************************************//**
 * @file 
 *
 * @brief This file contains all functions used to create an XML document
 * that displays files and directories as well as file creation and 
 * modification dates.
 *
 * @mainpage Program 3 - Xml Directory Listings and You!
 * 
 * @section course_section CSC 250
 *
 * @authors Alex Wulff
 * 
 * @date March 21, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program take an option from the user for date (created, 
 * modified, both or none), a directory and a specified pattern and will
 * scan directories and files starting at the given location and out put
 * the location of files to an xml document for easy reading.
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog3.exe [option] directory (pattern) 
   d:\> c:\bin\c:\> Prog3.exe [option] directory (pattern)
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug System regulated and inaccessable directories cause the scanning
 * to crash
 * 
 * @todo restructure switch upon rec_write rather than 4 functions.
 * 
 * @par Modifications and Development Timeline: 
 * @verbatim 
 * Date          Modification 
 * ------------  -------------------------------------------------------------- 
 *
 *	3/21/12		- Created project and started argument check functions.
 *
 *	3/22/12		- Finished open check for xml and started recusive planning.
 *
 *	3/28/12		- Created initial write for xml opening lines and first 
 *				  recursive call.
 *
 *	4/1/12		- Continued to work on recusive calls and created dated 
 *				  versions to be used.
 *
 *	4/2/12		- Created switch for date specification.
 *
 *	4/3/12		- Finished recursive calls and finalized pattern detection.
 *
 *	4/4/12		- Documented code and debugged for argument checking and 
 *				  pattern detections.
 *
 *	4/5/12		- Finalized documentation and debugging.
 *
 * @endverbatim
 *
 *****************************************************************************/


#include "direct.h"
#include "io.h"
#include <time.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

using namespace std;

bool argcheck(int argc, char **argv, char &option, string &directory, string
	&pattern);
bool open_xml(ofstream &fout);
void pattern_option_switch(ofstream &fout, string directory, 
	string &pattern, char option);
void rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle);
void cdate_rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle);
void mdate_rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle);
void bdate_rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle);
void usage_menu();
bool word_check(string word, string ext, char *file);
void write_xml(ofstream &fout, string pattern, string pattern2, 
	string directory, char option);


/**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This is the starting point to the program.  It will take commands from the 
 * command promt or an IDE's debug functions.  The program will take these for
 * specification of what directory to begin browsing in, an option should the
 * user specify and a pattern to compare the files to.  The pattern will be
 * broken into the name desired and the extension associated with the name.
 * After completion, a recursive routine is called that cycles through 
 * directories and prints the matching files (and times if specified) to an XML
 * document called dir.xml.
 *
 * @param[in] argc - the number of arguments from the command prompt.
 * @param[in] argv - a 2d array of characters containing the arguments.
 *
 * @returns 0 - upon completion of spell check as successful.
 * @returns 1 - if the arguments are invalid.
 * @returns 2 - if xml does not open correctly.
 *
 *****************************************************************************/

int main( int argc, char **argv)
{
	char option;
	string directory, pattern;
	ofstream fout;
	
	if (!argcheck(argc, argv, option, directory, pattern))
		exit (1);
	if (!open_xml(fout))
		exit (2);
	pattern_option_switch(fout, directory, pattern, option);
	
	fout.close();
	return 0;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function uses a count provided by main to determine
 * if usage has been set correctly.  Should the count of arguments passed be 
 * any number other than three or four (program name, possible option, 
 * a directory to scan, and a pattern),it can be assumed that the user has 
 * provided incorrect arguments, so a usage menu will be presented.
 *
 * @param[in] argc: a simple integer representing the number of
 * arguments from command prompt.
 * @param[in] **argv: dynamically sized array of characters, containing the 
 * user's arguements as entered at command prompt.
 * @param[in] option: a character to be assigned a value (B,C, or M) to 
 * coorespond with the user's preference of date out put in the xml file.
 * @param[out] option: a character to be assigned a value (B,C, or M) to 
 * coorespond with the user's preference of date out put in the xml file.
 * @param[in] directory: a string holding the location for the directory to be
 * the "top level" for the desired scan of files.
 * @param[in] pattern: a selection of characters chosen from the user to 
 * represent what type of file is desired in the xml file or what the name
 * without extension should contain.
 *
 * @returns - True: if the number of arguments is 3 or 4, and the option is 
 * either entered correctly, or is set-able to an empty value.
 * @returns - False: if the number of arguments is not 3 or 4, or the option
 * for date has not been given propperly.
 *
 ******************************************************************************/
bool argcheck(int argc, char **argv, char &option, string &directory, string
	&pattern)
{
	char *choice;
	if (argc != 3 && argc != 4)	// usage check
	{
		usage_menu();
		return false;
	}

	pattern = argv[argc -1];
	directory = argv[argc -2];

	if (argc == 4)
	{	// check option request
		choice = argv[argc - 3];
		option = choice[1];
		choice[1] = toupper(choice[1]);
		if (choice[0] != '-')
		{
			usage_menu();
			return 0;
		}
		if (choice[1] != 'B' && choice[1] != 'C' && choice[1] != 'M')
		{
			usage_menu();
			return 0;
		}
		
	}
	else option = '*';

		return 1;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function is an adaptation of the rec_write function,
 * which is modified only slightly to contain a different set of write options,
 * specifically this version will output the date created and modified of the
 * files as found.
 * The function creates multiple recursive calls to itself to create a 
 * directory "tree" within the xml file, and provides the files within
 * each directory and a closing tag as it exits.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[in] *dir: dynamically sized array of characters, initially containing
 * the user's choice of a start location, then the location of the next 
 * directory or a file, depending on what is found first (alphabetically).
 * @param[in] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[out] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[in] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[out] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[in] all_files: _finddata_t object used to help circulate through
 * the file structure.  Passed through so only a single instance must be 
 * created.
 * @param[in] dir_handle: intptr_t handle used to define current directory.
 *
 * @returns - None
 *
 ******************************************************************************/

void bdate_rec_write(ofstream &fout, char *dir, string pattern, string pattern2, 
	_finddata_t all_files, intptr_t dir_handle)
{
	char *temp, *loc, timeCreate[50], timeMod[50];
	int index;
	string name, extension;
					// will toggle to be -1 if both extension and name
					// are null for pattern, 0 if name is void for
					// pattern and 1 if extension. 2 for combos.
	
	if (_chdir(dir) ==0)
	{
		temp = _getcwd(NULL, 0);
		loc = _getcwd(NULL, 0);

				dir_handle = _findfirst("*.*", &all_files);
				do
				{
					temp = all_files.name;
					loc = _getcwd(NULL, 0);

					if ( temp[0] != '.' && temp[1] != '.')
					{
	// capture both name and extension of file for compare against pattern
					name = all_files.name;
					extension = all_files.name;
					index = name.find(".");
					ctime_s(timeCreate, _countof(timeCreate), 
						&all_files.time_create);
					ctime_s(timeMod, _countof(timeMod), 
						&all_files.time_access);
					if(index > 1)
					{
					name.resize(index);
					extension = extension.substr(index+1);
					}	
						if (all_files.attrib &_A_SUBDIR)
						{
						fout << "<folder name=\"" << loc <<"\\" << temp  
							<< "\\\" >" << endl;
						bdate_rec_write(fout, temp, pattern, pattern2, 
							all_files, dir_handle);
						fout << "<\/folder>" << endl;
						(_chdir("..") ==0);
						}
						// check that the file should be printed
						else if (word_check(pattern2, pattern, temp))
						{
							fout << "<file name=\"" << temp 
								<< "\" DateCreated=\"" << timeCreate 
								<< "\" DateModified=\"" << timeMod << "\" \/>" 
								<< endl;
						}
					}
				}while (_findnext (dir_handle, &all_files) == 0);
			}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function is an adaptation of the rec_write function,
 * which is modified only slightly to contain a different set of write options,
 * specifically this version will output the date created of the files as 
 * found.
 * The function creates multiple recursive calls to itself to create a 
 * directory "tree" within the xml file, and provides the files within
 * each directory and a closing tag as it exits.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[in] *dir: dynamically sized array of characters, initially containing
 * the user's choice of a start location, then the location of the next 
 * directory or a file, depending on what is found first (alphabetically).
 * @param[in] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[out] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[out] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[in] all_files: _finddata_t object used to help circulate through
 * the file structure.  Passed through so only a single instance must be 
 * created.
 * @param[in] dir_handle: intptr_t handle used to define current directory.
 *
 * @returns - None
 *
 ******************************************************************************/
void cdate_rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle)
{
	char *temp, *loc, timeCreate[50], timeMod[50];
	int index;
	string name, extension;
					// will toggle to be -1 if both extension and name
					// are null for pattern, 0 if name is void for
					// pattern and 1 if extension. 2 for combos.
	
	if (_chdir(dir) ==0)
	{
		temp = _getcwd(NULL, 0);
		loc = _getcwd(NULL, 0);

				dir_handle = _findfirst("*.*", &all_files);
				do
				{
					temp = all_files.name;
					loc = _getcwd(NULL, 0);

					if ( temp[0] != '.' && temp[1] != '.')
					{
	// capture both name and extension of file for compare against pattern
					name = all_files.name;
					extension = all_files.name;
					index = name.find(".");
					ctime_s(timeCreate, _countof(timeCreate), 
						&all_files.time_create);
					ctime_s(timeMod, _countof(timeMod), 
						&all_files.time_access);
					if(index > 1)
					{
					name.resize(index);
					extension = extension.substr(index+1);
					}	
						if (all_files.attrib &_A_SUBDIR)
						{
						fout << "<folder name=\"" << loc <<"\\" << temp  
							<< "\\\" >" << endl;
						cdate_rec_write(fout, temp, pattern, pattern2, 
							all_files, dir_handle);
						fout << "<\/folder>" << endl;
						(_chdir("..") ==0);
						}
					 // check that the file should be printed
						else if (word_check(pattern2, pattern, temp))
						{
							fout << "<file name=\"" << temp 
								<< "\" DateCreated=\""	<< timeCreate 
								<< "\" \/>" << endl;
						}
					}
				}while (_findnext (dir_handle, &all_files) == 0);
			}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function takes an output file name that has not been
 * set to a particular file and opens it to the name dir.xml.  This function
 * will also check for success and return values to determine as much as well
 * as an error message should the file fail to open.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 *
 * @returns - True: should the file be opened successfuly.
 * @returns - False: should the file not be open.
 *
 ******************************************************************************/

bool open_xml(ofstream &fout)
{
	fout.open("dir.xml", ios::out | ios::trunc);

	if (!fout)
	{
		cout << "ERROR creating directory table!" << endl;
		return 0;
	}
	
	return 1;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function is an adaptation of the rec_write function,
 * which is modified only slightly to contain a different set of write options,
 * specifically this version will output the date modified of the files as
 * found.
 * The function creates multiple recursive calls to itself to create a 
 * directory "tree" within the xml file, and provides the files within
 * each directory and a closing tag as it exits.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[in] *dir: dynamically sized array of characters, initially containing
 * the user's choice of a start location, then the location of the next 
 * directory or a file, depending on what is found first (alphabetically).
 * @param[in] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[out] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[in] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[out] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[in] all_files: _finddata_t object used to help circulate through
 * the file structure.  Passed through so only a single instance must be 
 * created.
 * @param[in] dir_handle: intptr_t handle used to define current directory.
 *
 * @returns - None
 *
 ******************************************************************************/
void mdate_rec_write(ofstream &fout, char *dir, string pattern, 
	string pattern2, _finddata_t all_files, intptr_t dir_handle)
{
	char *temp, *loc, timeCreate[50], timeMod[50];
	int index;
	string name, extension;
					// will toggle to be -1 if both extension and name
					// are null for pattern, 0 if name is void for
					// pattern and 1 if extension. 2 for combos.
	
	if (_chdir(dir) ==0)
	{
		temp = _getcwd(NULL, 0);
		loc = _getcwd(NULL, 0);

				dir_handle = _findfirst("*.*", &all_files);
				do
				{
					temp = all_files.name;
					loc = _getcwd(NULL, 0);

					if ( temp[0] != '.' && temp[1] != '.')
					{
		// capture both name and extension of file for compare against pattern
					name = all_files.name;
					extension = all_files.name;
					index = name.find(".");
					ctime_s(timeCreate, _countof(timeCreate), 
						&all_files.time_create);
					ctime_s(timeMod, _countof(timeMod), 
						&all_files.time_access);
					if(index > 1)
					{
					name.resize(index);
					extension = extension.substr(index+1);
					}	
						if (all_files.attrib &_A_SUBDIR)
						{
						fout << "<folder name=\"" << loc <<"\\" << temp  
							<< "\\\" >" << endl;
						mdate_rec_write(fout, temp, pattern, pattern2, 
							all_files, dir_handle);
						fout << "<\/folder>" << endl;
						(_chdir("..") ==0);
						}
					 // check that the file should be printed
						else if (word_check(pattern2, pattern, temp))
						{
							fout << "<file name=\"" << temp 
								<< "\" DateModified=\"" << timeMod << "\" \/>" 
								<< endl;
						}
					}
				}while (_findnext (dir_handle, &all_files) == 0);
			}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function is an adaptation of the rec_write function,
 * which is modified only slightly to contain a different set of write options,
 * specifically this version will output only the file names (no option 
 * selected).
 * The function creates multiple recursive calls to itself to create a 
 * directory "tree" within the xml file, and provides the files within
 * each directory and a closing tag as it exits.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[in] directory: a string that initially contains the user's choice 
 * of a start location.  This will be passed on for write purposes.
 * @param[in] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This will be broken into pattern
 * (the extension) and pattern2 (the name).
 * @param[out] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This will be broken into pattern
 * (the extension) and pattern2 (the name).
 * @param[in] option: a character determining if dates are needed.  Will be 
 * passed through.
 *
 * @returns - None
 *
 ******************************************************************************/
void pattern_option_switch(ofstream &fout, string directory, string &pattern, 
	char option)
{
	int spot, spot2;
	string pattern2;	// before extention sting.
	spot = pattern.find("*");
	spot2 = pattern.rfind("*");

	if (spot == string::npos)
	{						// if no pattern designated, proceed for all.
		pattern = "*";
		pattern2 = "*";
		write_xml(fout, pattern, pattern2, directory, option);// call from here
	}
	if (spot > 0)				// make pattern2 name, and pattern extension;
	{
		pattern2 = pattern.substr(0,spot);
		if (spot != spot2)
			pattern = pattern.substr(spot2);
		else pattern = pattern.substr(spot);	
		write_xml(fout, pattern, pattern2, directory, option);
		// call from here
	}
	if (spot == 0)
	{
		pattern2 = "*";// if only extension, void name.
		spot = pattern.find(".");
		pattern = pattern.substr(spot);
		if (pattern.compare(".*") == 0)
		{
			pattern = "*";
			write_xml(fout, pattern, pattern2, directory, option);
			// call from here
		}
		else 
		{
			if (spot != spot2)
			pattern = pattern.substr(spot2);
		else pattern = pattern.substr(1);
			write_xml(fout, pattern, pattern2, directory, option);
			// call from here
		}
	}
	
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function is an adaptation of the rec_write function,
 * which is modified only slightly to contain a different set of write options,
 * specifically this version will output only the file names (no option 
 * selected).
 * The function creates multiple recursive calls to itself to create a 
 * directory "tree" within the xml file, and provides the files within
 * each directory and a closing tag as it exits.
 *
 * @param[in] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[out] fout: the output file used by the system as it writes the xml
 * direcotry.
 * @param[in] *dir: dynamically sized array of characters, initially containing
 * the user's choice of a start location, then the location of the next 
 * directory or a file, depending on what is found first (alphabetically).
 * @param[in] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[out] pattern: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing. This is the extension part of
 * the pattern specification.
 * @param[in] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[out] pattern2: the original pattern as defined by the user will
 * be passed through the function in order to to determine if the file found
 * matches the desired criterea for printing.  This is the name of the file
 * from the pattern specification.
 * @param[in] all_files: _finddata_t object used to help circulate through
 * the file structure.  Passed through so only a single instance must be 
 * created.
 * @param[in] dir_handle: intptr_t handle used to define current directory.
 *
 * @returns - None
 *
 ******************************************************************************/
void rec_write(ofstream &fout, char *dir, string pattern, string pattern2, 
	_finddata_t all_files, intptr_t dir_handle)
{
	char *temp;
	char *loc;
	int index, offsetName;
	string timeMod, timeCreate;
	string name, extension;

	offsetName = pattern2.find("*");
	if (offsetName == 0)
		offsetName++;

							// will toggle to be -1 if both extension and name
							// are null for pattern, 0 if name is void for
							// pattern and 1 if extension. 2 for combos.
	if (_chdir(dir) ==0)
	{
		temp = _getcwd(NULL, 0);
		loc = _getcwd(NULL, 0);

				dir_handle = _findfirst("*.*", &all_files);
				do
				{
					temp = all_files.name;
					loc = _getcwd(NULL, 0);

					if ( temp[0] != '.' && temp[1] != '.')
					{
		// capture both name and extension of file for compare against pattern
					name = all_files.name;
					extension = all_files.name;
					index = name.find(".");
					if(index > 1)
					{
					name.resize(index);
					extension = extension.substr(index+1);
					}	
						if (all_files.attrib &_A_SUBDIR)
						{
						fout << "<folder name=\"" << loc <<"\\" << temp  
							<< "\\\" >" << endl;
						rec_write(fout, temp, pattern, pattern2, all_files, 
							dir_handle);
						fout << "<\/folder>" << endl;
						(_chdir("..") ==0);
						}
					 // check that the file should be printed
						else if (word_check(pattern2, pattern, temp))
						{
							fout << "<file name=\"" << temp << "\" \/>" 
								<< endl;
						}
					}
				}while (_findnext (dir_handle, &all_files) == 0);
			}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Outputs correct usage information to the user.  
 *
 * @param[in] : none.
 *
 * @returns - none.
 *
 ******************************************************************************/
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\\... prog3.exe [option] <directory> pattern" << endl << endl;
	cout << "!!The directory should be the full path!!" << endl;
	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This funciton compares the pattern after it has been 
 * broken into the name of the file and extensions desired to the files found
 * and will return true if there is a match and false otherwise.
 *
 * @param[in] word: the name of the file specified as part of the pattern.
 * @param[in] ext: the extension of the file as specified in the pattern.
 * @param[in] *file: character string that will be used for the comparisons.
 *
 * @returns - True: should the word and extension criterea match the file.
 * @returns - False: should no match be found.
 *
 ******************************************************************************/
bool word_check(string word, string ext, char *file)
{
	int spotW, spotE, inWord, inExt;
	string name, newWordFront, newExtFront, newWordEnd, newExtEnd;
	// check for *.*
	if (word.compare("*") == 0 && ext.compare("*") == 0)
		return 1;
	
	spotW = word.find("*");
	spotE = ext.find("*");

	// set character array to string for compares and trunc before * in
	// word and the extension (name and extension of file).

	name.assign(file);
	if (spotW != string::npos)
	newWordEnd = word.substr(spotW);
	else newWordEnd = word;
	if (spotW != string::npos)
	newWordFront = word.substr(0, spotW+1);
	else newWordFront = word;
	if (spotE != string::npos)
	newExtEnd = ext.substr(spotE+1);
	else newExtEnd = ext;
	if (spotE != string::npos)
	newExtFront = ext.substr(0, spotE+1);
	else newExtEnd = ext;
	inWord = name.find(newWordEnd);
	inExt = name.find(newExtEnd);
	// test for one * in either the name or extension.
	// test for front
	if (newWordFront.compare("*") == 0)
	{
		if ( inExt != string::npos)
		return 1;
		// check word before * 
		inExt = name.find(newWordFront);
		if (inExt != string::npos)
			return 1;
		//reset
		inExt = name.find(newWordEnd);
	}
	// test for end
	if (newExtEnd.compare("*") == 0)
	{
		if (inWord !=string::npos)
		return 1;
		// check word before * 
		inWord = name.find(newExtFront);
		if (inWord !=string::npos)
			return 1;
		//reset
		inWord = name.find(newExtEnd);
	}
	// Both name and extension found in the file
	if ( inWord != string::npos && inExt !=string::npos)
		return 1;
	// trunc after * for final check.
	inWord = name.find(newWordFront);
	inExt = name.find(newExtFront);
	if ( inWord != string::npos && inExt !=string::npos)
		return 1;
	return 0;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function starts the initial writting into the xml 
 * file for version used as well as current file path.  The path will be listed
 * as "." should it be run from within the parent directory, and will have the
 * full path should the program be executed from outside of the directory.
 *
 * @param[in] fout: the xml file to be written to.
 * @param[out] fout: the xml file to be written to.
 * @param[in] pattern: extension section of the pattern to be compared.  Will
 * not be used, but rather passed through to the calls of files.
 * @param[in] pattern2: name section of the pattern to be compared.  Will
 * not be used, but rather passed through to the calls of files.
 * @param[in] directory: will be used as the start point for the search, and 
 * will then be passed on.
 * @param[in] option: character for which options will be used for dates.  This
 * option will be switched upon to determine which version of the recursive 
 * function will be called.
 *
 * @returns - none.
 *
 ******************************************************************************/
void write_xml(ofstream &fout, string pattern, string pattern2, string directory, 
	char option)
{
	char dir[100];
	fout << "<?xml version= \"1.0\"?>" << endl;// output first line for xml file

	strcpy_s(dir, directory.c_str());

	fout << "<folder name=\"" << dir  << "\">" << endl;
					// now switch on option for date look up.
	switch (int(option))
	{
		case (int) 'B':
			if ( _chdir(dir) == 0)
	{
		_finddata_t all_files;
		intptr_t dir_handle;
		dir_handle = _findfirst("*.*", &all_files);

		bdate_rec_write(fout, dir, pattern, pattern2, all_files, dir_handle);
		_findclose(dir_handle);
		fout << "<\/folder>" << endl;
		return;
	}
	else exit (3);
			break;
		case (int) 'C':
			if ( _chdir(dir) == 0)
	{
		_finddata_t all_files;
		intptr_t dir_handle;
		dir_handle = _findfirst("*.*", &all_files);

		cdate_rec_write(fout, dir, pattern, pattern2, all_files, dir_handle);
		_findclose(dir_handle);
		fout << "<\/folder>" << endl;
		return;
	}
	else exit (3);
			break;
		case (int) 'M':
			if ( _chdir(dir) == 0)
	{
		_finddata_t all_files;
		intptr_t dir_handle;
		dir_handle = _findfirst("*.*", &all_files);

		mdate_rec_write(fout, dir, pattern, pattern2, all_files, dir_handle);
		_findclose(dir_handle);
		fout << "<\/folder>" << endl;
		return;
	}
	else exit (3);
			break;
		default:
	if ( _chdir(dir) == 0)
	{
		_finddata_t all_files;
		intptr_t dir_handle;
		dir_handle = _findfirst("*.*", &all_files);

		rec_write(fout, dir, pattern, pattern2, all_files, dir_handle);
		_findclose(dir_handle);
		fout << "<\/folder>" << endl;
		return;
	}
	else exit (3);
	}
	return;
}
