/*************************************************************************//**
 * @file 
 *
 * @brief This file contains the main function which will take comands from
 * the comand line and interpret them as arguments for spell checking of a
 * text file.
 *
 * @mainpage Program 2 - Text-file Spell Checker
 * 
 * @section course_section CSC 250
 *
 * @authors Alex Wulff (part I) & Andrew Rossow (part II)
 * 
 * @date February 29th, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program will use a specified text dictionary to test for
 * correct spelling against a file to be corrected.  The file cross checked
 * with the spelling from the dictionary will be re-written to a file with the
 * same base name, but with a '.new' extension.  Should any word found in the
 * file to be checked be not in the dictionary, the word will be presented to
 * the user for confirmation of the spelling.  The word can here be corrected
 * manually.  Should the spelling be correct or not, the word will hence forth
 * be added to the dictionary, to reduce further prompting for the user.
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog2.exe dictionary.txt file_to_be_spell_checked.txt 
   d:\> c:\bin\Prog2.exe dictionary.txt file_to_be_spell_checked.txt
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug spell_check function has difficulty with numbers being in the list to
 * spell check.
 * 
 * @todo Fix spell_check function to handle integer interpretation
 * 
 * @par Modifications and Development Timeline: 
 * @verbatim 
 * Date          Modification 
 * ------------  -------------------------------------------------------------- 
 *
 *	2/27/12		- Worked on class calls and initiation.
 *
 *	3/1/12		- Checked and validated command line arguments.
 *
 *	3/5/12		- Created class insert function.
 *
 *	3/5/12		- Worked on additional class funcitons.
 *
 *	3/7/12		- Finalized class functions and supplimental functions.
 *
 *  3/8/12		- Created documentation for this program.
 *
 *	3/12/12		- Fixed error with reading integers from file and repeated
 *				  output of last word on dictionary file.
 *
 * @endverbatim
 *
 *****************************************************************************/


#include <list>
#include "mylist.h"


template < class T, class Allocator = allocator<T> > class list
{




}

/**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This is the starting point to the program.  It will take commands from the 
 * command promt or an IDE's debug functions.  The program will take these for
 * specification of what dictionary file to use and which file to spell check.
 * The program will go on to read in the dictionary into a series of nodes in
 * alphabetical order.  Once complete, the program will check these words 
 * against the words found in the file being spell checked, and if needed will
 * prompt the user to confirm spelling and add any new words to the dictionary
 * upon completion, while removing nodes from memory.
 *
 * @param[in] argc - the number of arguments from the command prompt.
 * @param[in] argv - a 2d array of characters containing the arguments.
 *
 * @returns 0 - upon completion of spell check as successful.
 * @returns 1 - if the arguments are invalid.
 * @returns 2 - dictionary file can not be opened.
 * @returns 3 - file to spell check and the new file fail to be opened.
 *
 *****************************************************************************/

int main(int argc, char **argv)
{
	//mylist wordlist[26];
	list. wordlist[26];

	string dic = "";
	string read = "";
	fstream dict;			// dictionary file
	ifstream readme;
	ofstream readme_new;
	if (!argcheck(argc, argv[1], argv[2]))	// verify arguments
		exit (1);
	dic = argv[1];
	read = argv[2];
	if (!open_dict(dic, dict))	//verify opening of dictionary
		exit (2);
	if (!open_readme(read, readme, readme_new)) // verify readme opens
		exit (3);
	read_dictionary(dict, wordlist);	// read in dictionary
	
	count( wordlist);		// count number of entries in dictionary

	spell_check (readme, readme_new, dict, wordlist);	//do spellchecking

	close(readme, readme_new, dict, wordlist, dic);

	return 0;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function uses a count provided by main to determine
 * if usage has been set correctly.  Should the count of arguments passed be 
 * any number other than three (program name, dictionary, target to spellcheck)
 * it can be assumed that the user has provided incorrect arguments, so a usage
 * menu will be presented.
 *
 * @param[in] count: a simple integer representing the number of
 * arguments from command prompt.
 * @param[in] dic: dynamically sized array of characters, containing the user's
 * choice of dicitionary file.
 * @param[in] read: dynamically sized array of characters, containing the 
 * user's requested file to perform the spell checking on.
 *
 * @returns - True: if the number of arguments is 3, and both files are in a
 * ".txt" extension.
 * @returns - False: if the number of arguments is not 3, or the files are
 * in a non-compatable format.
 *
 ******************************************************************************/


bool argcheck(int count,char *dic,char *read)
{
	if (count != 3)	// usage check
	{
		usage_menu();
		return false;
	}
	string dictionary = dic;
	string read_file = read;

	string_grab( dictionary, 5);
	string_grab( read_file, 5);
	dictionary.resize(dictionary.size());
	read_file.resize(dictionary.size());
	if (dictionary == ".txt" && read_file == ".txt")
	{
		return true;
	}
	usage_menu();
	return false;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function will close the files readme, readme_new, 
 * and dictionary.  While freeing the nodes, the dictionary file will be 
 * written out with the current words in every node, so the new dictionary will
 * be in alphabetical order.
 *
 * @param[in]  readme: Original input that was spell 
 * checked.  This will be closed out of use here.
 * @param[in]  readme_new: Modified file after spell
 * checking.  This too will be closed here.
 * @param[in]  dictionary: will be closed and re-opened
 * for writing out the new words as well as originals read thru from 
 * initiation.
 * @param[in]  wordlist: used to retreive words from the arrays
 * of nodes and pull words directly for write out and deletion.
 * @param[in]  dic: Used so the output dictionary after being
 * closed can be re-opened and then written out to.
 *
 *
 * @returns - none.
 *
 ******************************************************************************/
void close(ifstream &readme, ofstream &readme_new, fstream &dictionary, 
	mylist wordlist[], string dic)
{
	int i;
	string word;
	readme.close();
	readme_new.close();
	dictionary.close();
	dictionary.open(dic, ios::out | ios::trunc);
		for (i = 0; i < 26; i ++)
		{
			while ( wordlist[i].retrieve(word, 0))
			{
				dictionary << word << endl;
				wordlist[i].remove(word);
			}
		}
	dictionary.close();
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function walks through each index in the array of
 * the wordlist and has the class provide a number for how many individual 
 * nodes exist within each section.
 *
 * @param[in]  wordlist: used to check count of nodes per index
 * (first letter of the word in the alphabet)
 *
 * @returns - none.
 *
 ******************************************************************************/
void count( mylist wordlist[])
{
	int count = 0;
	int i;
	for (i = 0; i< 26; i++)
	{
		count += wordlist[i].get_count();
	}

	cout << count << " Records read-in" << endl;
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Validates the argument passed through main and uses it to
 * open a file for dictionary.
 *
 * @param[in]  arg: used for the argument containing the name
 * of the dictionary file.
 * @param[in]  dictionary: used to open the dictionary into the name dictionary
 *  and return it back to main for further manipulation.
 * @param[out]  dictionary: used to open the dictionary into the name dictionary
 *  and return it back to main for further manipulation.
 *
 * @returns - True if dictionary opens correctly.
 * @returns - False should dictionary not open. And prompts a message.
 *
 ******************************************************************************/
bool open_dict(string &arg, fstream &dictionary)
{
	char open[20];
	strcpy(open, arg.c_str());
	dictionary.open( open, ios::in | ios:: out | ios::app );
	if (!dictionary)
	{
		cout << "Dictionary not found or not openable.  Please validate and" 
			<< "try again." << endl;
		return false;
	}	
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Using a string designated as the file name to be spell 
 * checked, this function will open the file and check for varification of 
 * a successful open.  Should it fail a binary response will be sent back to
 * the function that called it.
 *
 * @param[in] read: String containing the name of the file to be read in and
 * spell checked.
 * @param[in] readme: ifstream to be the object the program will reference for
 * the spell checking process.
 * @param[in] readme_new: Ofstream to hold the spellchecked file and to write
 * out the new data upon completion
 *
 * @returns - True: if the files open correctly
 * @returns - False: should the file to be opened not be found or could not
 * be opened.
 *
 ******************************************************************************/
bool open_readme(string &read, ifstream &readme, ofstream &readme_new)
{
	char open[20];
	int size = read.size();
	strcpy(open, read.c_str());
	readme.open(open, ios::in);
	if (!readme)
	{
		cout << "Error opening" << read << ". Please check disk space and access." 
			<< endl;
		return false;
	}
	read.erase (size - 4);
	read.append (".new");
	strcpy(open, read.c_str());
	readme_new.open( open, ios::out | ios::trunc);
	if (!readme_new)
	{
		cout << "Error opening new readme. Please check disk space and access."
			<< endl;
		return false;
	}
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function reads the words from the specified 
 * dictionary into an array of nodes aranges by the first letter of the word.
 * before reading the word in, the words will be converted to lowercase.
 *
 * @param[in]  dict: the file used to hold all of the words
 * to be used as a dictionary for spell checking.
 * @param[in]  wordlist: used for cataloging and inserting words
 * into nodes for later use of the dictionary.
 *
 * @returns - none.
 *
 ******************************************************************************/
void read_dictionary(fstream &dict, mylist wordlist[])
{
	int index;
	string word;
	char first_letter[1];
	char punct;
	while (dict >> word)
	{
		word.copy(first_letter, 1);
		first_letter[0] = tolower(first_letter[0]);
		word.replace (0, 1, first_letter, 1);
		index = int (first_letter[0]) - int ('a');
		
		wordlist[index].insert(word);
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function will read in every word from the original
 * readme file, and will save punctuation for later use should it exist.  The
 * words read in will be compaired to the words in the dictionary file, and
 * should the word not be in the dictionary, this function will prompt the user
 * to see if the spelling is correct.  If not the function will receive new 
 * spelling.  In either case the new word will be added into the dictionary, 
 * and the new readme will be written out.
 *
 * @param[in]  readme: used for initial file to be spell
 * checked.
 * @param[in]  readme_new: used for the write out after
 * spell checking has been completed on each word.
 * @param[in]  dict: dictionary file used for spell checking
 * of old file.  Will be added to with any new words found.
 * @param[in]  wordlist: used as indexed search for inserting
 * and finding specific words.
 *
 *
 * @returns - none.
 *
 ******************************************************************************/
void spell_check (ifstream &readme, ofstream &readme_new, fstream &dict, mylist wordlist[])
{
	string word, word_copy, word_new;
	int index;
	int new_words=0;
	char punct, choice, first_letter[1], cstr_word[20], upper;
	readme >> word;
	do
	{

	if (isdigit(word[0]))
	{
		readme_new << word << " ";
		if (readme.peek() == '\n')
			readme_new << '\n';
	}
	else 
	{
		word_copy = word;
		word_new = word_copy;
		word.copy(first_letter, 1);
		first_letter[0] = tolower(first_letter[0]);
		word.replace (0, 1, first_letter, 1);
		index = int (first_letter[0]) - int ('a');
		if (ispunct(word.c_str()[word.size() -1]))
		{
			punct = word_copy.c_str()[word_copy.size() -1];
			word.resize (word.size() - 1);
			word_copy = word;
			word_new = word_copy;
		}
		else punct = 'a';
			if (!wordlist[index].find(word))
		{
			cout << "Word not found in Dictionary.  Is " << word <<
				" spelled correctly? (y/n)" << endl;
			cin >> choice;
			new_words++;
			choice = tolower(choice);
			if (choice == 'y')
				wordlist[index].insert(word);
			else 
			{
				cout << "What is the correct spelling of " << word << "?"<< endl;
				cin >> word_new;
				wordlist[index].insert(word_new);
			}
		}
		word_new.resize(word_new.size());
		if (ispunct((int) punct))
		{
			readme_new << word_new << punct << " ";
			if (readme.peek() == '\n')
				readme_new << '\n';
		}
		else
		{
			readme_new << word_new << " ";	
			if (readme.peek() == '\n')
				readme_new << '\n';
		}
	}
	}while (readme >> word);
		cout << new_words << " New words written." << endl;
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Uses a specified number to extract the last characters of
 * a string.
 *
 * @param[in]  word : used for initial string and return to
 * user after processing.
 * @param[out]  word : returned modified value after processing.
 * @param[in]  number: the specified number of characters to take off
 * the end of the string.
 *
 * @returns - none.
 *
 ******************************************************************************/
void string_grab(string &word, int number)
{
	int i;
	char *sub_word = new (nothrow) char [25];
	for (i = 1; i < number; i++)
	{
		sub_word[i] = word.c_str()[word.size() -i];
	}
	while (i < number)
	{
		i++;
	}
	sub_word[i] = '\0';
	sub_word = strrev(sub_word);
	word = sub_word;
	word.resize(number -1);
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Outputs correct usage information to the user.  
 *
 * @param[in] : none.
 *
 * @returns - none.
 *
 ******************************************************************************/
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\... prog2.exe [dictionary file] [readme file]" << endl << endl;
	cout << "Both files must be in txt extensions" << endl;
	return;
}