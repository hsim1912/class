#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <iomanip>

using namespace std;

void display_menu();
char choice();
int build_custom_array();
bool file_check();
int file_read_in();
int file_read_out();
void input_array1d();
void input_array2d();
void input_array3d();
void output_array1d();
void output_array2d();
void output_array3d();
int main()
{
	int x = 0;
	int row;
	int column;
	int depth;
	int size;
	int answer;
	int *ptr = NULL;

	cout << "Building arrays is fun.  How many dimensions?\n\n";

	cin >> answer;

	if (answer == 1)
	{
		cout << "Can do.  How many indexes?\n\n";
		cin >> row;
		if (answer == 1)
		{ 
			cout << "How about a number instead?";
			row = 0;
		}
		if (answer < 1)
		{
				cout << "Not gonna happen.\n\n";
				exit (2);
		}
	}
	if (answer == 2)
	{
		cout << "Can do.  How many rows?\n\n";
		cin >> row;
		if (row <= 0)
		{
				cout << "Sorry not gonna happen.\n\n";
				exit (2);
		}
		if (row == 1)
		{
			cout << "Will make one number instead of an array.\n\n";
			cout << "Enter number now: ";
			cin >> answer;
		}
		cout << "How many columns?\n\n";
		cin >> column;
		if (column <= 0)
		{
				cout << "Sorry not gonna happen.\n\n";
				exit (2);
		}
		if (column == 1)
		{
			cout << "Will make a one dimensional array.\n\n";
		}
	}
	if (answer == 3)
	{
		cout << "Can do.  How many rows?\n\n";
		cin >> row;
		if (row <= 0)
		{
			cout << "Sorry not gonna happen.\n\n";
			exit (2);
		}
		if (row == 1)
		{
			cout << "Will make one number instead of an array.\n\n";
			cout << "Enter number now: ";
			cin >> answer;
		}
		cout << "How many columns?\n\n";
		cin >> column;
		if (column <= 0)
		{
				cout << "Sorry not gonna happen.\n\n";
				exit (2);
		}
		if (column == 1)
		{
			cout << "Will make a one dimensional array.\n\n";
			column = 0;
		}
		cout << "How many depth indexes?\n\n";
		cin >> depth;
		if (depth <= 0)
		{
				cout << "Sorry not gonna happen.\n\n";
				exit (2);
		}
		if (depth == 1)
		{
			cout << "Will make a two dimensional array.\n\n";
			depth = 0;
		}
	}
		if (answer > 3)
		{
			cout << "I'm too stupid to do more than three... Check back later.\n\n";
			exit (1);
		}
	
	if (answer == 1)
	{
		column = 0;
		depth = 0;
	}

	if (answer == 2)
	{
		depth = 0;
	}
	*ptr = build_custom_array( row, column, depth,);


	return 0;
}
void display_menu()
{
	char choice = 0;

	while ( tolower (choice) != 'q')
	{
	cout << "select from the following: " << endl;
	// insert choices here from
	cin >> choice;
	}

	return;
}
char choice()
{
	char selection;

	cout << "Run once more?" << endl << endl << "Choose Y for yes or N for no." << endl;

	cin >> selection;

	selection = tolower(selection);

	while (selection != 'y' && selection != 'n')
	{
		cout << "Invalid choice.  Try again." << endl << "\n Y for yes or N for no." << endl;

		cin >> selection;

		selection = tolower(selection);
	}
	return selection;
}
int build_custom_array( int r, int c, int d)
{
	int ***ptr = NULL;
	int i;
	int j;
	int k;
	*ptr = new(nothrow) int * [r];
	if ( ptr == NULL)
	return NULL;
	for ( i = 0 ; i < r ; i++)
{
	*ptr[i] = new(nothrow) int  [c];

	for (j = 0 ; j < c ; j++)
	{
		ptr[i][j] = new(nothrow) int [c];
	}
	for (k = 0 ; k < d ; k++)
	{
		ptr[i][j][k] = new(nothrow) int [d];
	}
	
}
bool file_check();
int file_read_in();
int file_read_out();
void input_array1d();
void input_array2d();
void input_array3d();
void output_array1d();
void output_array2d();
void output_array3d();