#include <iostream>

using namespace std;

int main()
{

    // declare var
    double radius;
    double area;
    const double pi = 3.14159;

    // receive imput for radius

    cout << "Enter radius of circle: " << endl;

    cin >> radius;

    //calc area

    area = pi * radius * radius;

    //display calculated area

    cout << "Your area is: " << area;
}