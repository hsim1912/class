#include <iostream>
#include <iomanip>
#include <cctype>

using namespace std;

int main()
{
	// Purpose is to order items from a snack bar.  
	// Choices include a roast beef sandwitch and a pizza.
	// The roast beef sandwitch can bee customized with Lettuce, Cheese or Onion (one only)
	// The pizza can be customized with the following (all applicable):
	// Pepperoni, Mushrooms, Olives, Sausage, Ham or Pineapple
	// Add cost(s) with customized items and add tax to result in a total.
	
	// Declare variables and assign base costs

	double roastBeef, pizza, Cheese, Lettuce, Onion, None, Pepperoni, Mushrooms, Olives, Sausage, Ham, Pineapple;
	char selection;
	double sum, tax, total;

	roastBeef = 4.5;
	pizza = 3.5;
	Cheese = 1.00;
	Lettuce = 0.5;
	Onion = 0.5;
	Pepperoni = 1.00;
	Mushrooms = 0.75;
	Olives = 0.75;
	Sausage = 1.25;
	Ham = 1.00;
	Pineapple = 0.75;

	tax = .06;

	//Prompt menu and display options for submission

	cout << "Snack Bar Menu :" << endl;
	cout << "A. Roast Beef......$4.50" << endl;
	cout << "B. Pizza...........$3.50" << endl;
	cout << "Q. Quit without ordering." << endl;
	cout << "Your choice: ";

	// Receive input from user for choice

	cin >> selection;

	// Use logic arguements to determine path.

	selection = toupper (selection);

	if (selection == 'A')
	{
		cout << endl << "Add-ons:" << endl;
		cout << "L. Lettuce....... 0.50" << endl;
		cout << "C. Cheese........ 1.00" << endl;
		cout << "O. Onion......... 0.50" << endl;
		cout << "N. None" << endl;
	
		cin >> selection;

		selection = toupper (selection);

		if (selection == 'L')
	
		{
			cout << "Your Choice: L" << endl;
			sum = roastBeef + Lettuce;
			
			cout << setprecision(2);
			cout << fixed;
			cout << showpoint;

			cout << "Your food subtotal: " << sum << endl;
			tax = sum * tax;
			cout << "Tax                 " << tax << endl;
			total = tax + sum;
			cout << "Total               " << total << endl;

			exit (0);
		}
		else if (selection == 'C')
		{
			cout << "Your Choice: C" << endl;
			sum = roastBeef + Cheese;

			cout << setprecision(2);
			cout << fixed;
			cout << showpoint;

			cout << "Your food subtotal: " << sum << endl;
			tax = sum * tax;
			cout << "Tax                 " << tax << endl;
			total = tax + sum;
			cout << "Total               " << total << endl;

			exit (0);
		}
		else if (selection == 'O')
		{
			cout << "Your Choice: O" << endl;
			sum = roastBeef + Onion;

			cout << setprecision(2);
			cout << fixed;
			cout << showpoint;

			cout << "Your food subtotal: " << sum << endl;
			tax = sum * tax;
			cout << "Tax                 " << tax << endl;
			total = tax + sum;
			cout << "Total               " << total << endl;

			exit (0);
		}
		else if (selection == 'N')
		{
			cout << "Your Choice: N" << endl;
			sum = roastBeef;

			cout << setprecision(2);
			cout << fixed;
			cout << showpoint;

			cout << "Your food subtotal: " << sum << endl;
			tax = sum * tax;
			cout << "Tax                 " << tax << endl;
			total = tax + sum;
			cout << "Total               " << total << endl;

			exit (0);
		}
		
	



	}


	if (selection == 'B')
	{
		sum = pizza;

		cout << endl << "Add Pepperoni ($1.00)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Pepperoni;


		cout << endl << "Add Mushrooms ($0.75)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Mushrooms;

		cout << endl << "Add Olives ($0.75)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Olives;

		cout << endl << "Add Sausages ($1.25)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Sausage;

		cout << endl << "Add Ham ($1.00)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Ham;
		
		cout << endl << "Add Pineapple ($0.75)? Y/N: ";
		cin >> selection;
		selection = toupper (selection);
		if (selection == 'Y')
			sum = sum + Pineapple;
		
			cout << setprecision(2);
			cout << fixed;
			cout << showpoint;

			cout << "Your food subtotal: " << sum << endl;
			tax = sum * tax;
			cout << "Tax                 " << tax << endl;
			total = tax + sum;
			cout << "Total               " << total << endl;

			exit (0);
	}

	if (selection == 'Q')
	{
		cout << "Quitting program without choosing food." << endl;
		exit (0);
	}

	else cout << "You did not make a correct choice. Program ending.." << endl;


	return 0;
}