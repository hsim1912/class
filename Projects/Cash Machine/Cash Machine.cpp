#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	//declare variables

	double cost, tendered, dif; 
	int diff, p, n, d, q, s, f, t, twen, fif, h;

		
	//request cost

	cout << "This program is built to calculate and display the most effective change from a cost and amount tendered." << endl;

	cout << "Input cost: ";

	cin >> cost;

	
	//request amount tendered

	cout << endl << "input amount tendered: ";
	
	cin >> tendered;

	//take difference of them

	dif = (tendered - cost);

	cout << setprecision(2);
	cout << fixed;
	cout << showpoint;

	cout << endl << "Your difference is " << dif << endl;

	//determine number of pennies needed to be given back

	diff = dif * 100;

	p = (diff % 10);

	p = (p % 5);
	cout << "number of pennies: " << p << endl;

	//determine number of nickles needed to be given back

	n = (diff % 100);

	n = (n % 25);

	n = (n % 10);

	cout << "number of nickles: " << n << endl;


	//determine number of dimes  needed to be given back

	//determine number of quarters  needed to be given back
	
	//determine number of singles needed to be given back

	//determine number of fives needed to be given back

	//determine number of tens needed to be given back

	//determine number of twenties needed to be given back

	//determine number of fifties needed to be given back

	//determine number of hundreds needed to be given back









	return 0;
}