#include <iostream>
#include <cmath>
#include <iomanip>

double factoral(double radian);
void get_choice();
void find_height();
void find_distance();
void find_slant_range();
double my_pow(double radian, double power);
double my_sin(double radian);
double my_cos(double radian);
double my_tan(double radian);
double my_cosec(double radian);
double my_sec(double radian);
double my_cot(double radian);
int my_factorial( int &pow);
int get_choice();

using namespace std;

int main()
{
// devlare variables for usage
double angle = 11;
double rad;

// set precision for following outputs
	cout << setprecision(15);
	cout << fixed;
	cout << showpoint;
	
// start with 11 degrees and convert to radians... then display
while (angle <= 360)
{
	rad = (angle * (3.1415926535897932384626433832795 / 180));
	cout << "angle: " << angle << "radians: " << rad << endl;

	cout << "sin" << "     " << my_sin (rad) << "     " << sin(rad) << "     " << my_sin(rad) - sin(rad) << endl;
	cout << "cos" << "     " << my_cos (rad) << "     " << cos(rad) << "     " << my_cos(rad) - cos(rad) << endl;
	cout << "tan" << "     " << my_tan (rad) << "     " << tan(rad) << "     " << my_tan(rad) - tan(rad) << endl;
	cout << "csc" << "     " << my_cosec (rad) << "     " << csc(rad) << "     " << my_cosec(rad) - csc(rad) << endl;
	cout << "sec" << "     " << my_sec (rad) << "     " << sec(rad) << "     " << my_sec(rad) - sec(rad) << endl;
	cout << "cot" << "     " << my_cot (rad) << "     " << cot(rad) << "     " << my_cot(rad) - cot(rad) << endl;
	angle += 25;
}
// display menu and change precision back to two decimal spaces

cout << setprecision(2);
 if (get_choice() == 1)
	 return 0;
}
void factoral(&pow)
{
	int pow;
	int minusPow = pow;
	
	while (pow > 0)
	{
		minusPow = minusPow * (pow - 1);
		pow -= 1;
	}

	pow = minusPow;
	return;
}
double my_pow( double number, double exponant)
{
	// declare variables
	double num;
	double exp;
	int i;

	cin >> num >> exp;

	if (exp == 0)
	{
	num = 1;
	return 0;
	}

	else for (i=1 ; i < exp ; i++)
	{
		num *= num;
	}

	return 0;

}

double my_sin( double radian )
{
	double num, num2;
	double numer;
	double denom;
	int i = 0;
	double power = 0;
	double modpower = power;
	int counter = 0;


	cin >> num;

	num2 = num;
	
	numer = num;

	for ( i ; i<1000 ; i ++)
	{
		
		numer = num;
		

		if (counter % 2 == 0)
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 += numer / power;
		}
		else
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 -= numer / power;
		}
		power += 2;
		counter++;
	}

	num = num2 / denom;
	return num;
}
double my_cos( double radian )
{
	double num, num2;
	double numer;
	double denom;
	int i = 0;
	double power = 1;
	double modpower = power;
	int counter = 0;


	cin >> num;

	num2 = num;
	
	numer = num;

	for ( i ; i<1000 ; i ++)
	{
		
		numer = num;
		

		if (counter % 2 == 0)
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 += numer / power;
		}
		else
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 -= numer / power;
		}
		power += 2;
		counter++;
	}

	num = num2 / denom;
	return num;
}
double my_tan( double radian )
{
	return (my_sin (radian) / my_cos(radian);
}
double my_cosec( double radian)
{
	return (1.0 / my_sin( radian ));
}

double my_sec( double radian )
{
	return (1.0 / my_cos(radian));
}
double my_cot( double radian )
{
	return (1/ my_tan( radian ));
}
int get_choice()
{
	char selection;

	cout << "***Hardrocker Triangulator***" << endl
		<< "H - Find Height" << endl << "D - Find Distance" << endl
		<< "S - Find Slant Range" << endl << "Q - Quit Program" << endl
		<< "Your Choice: ";

	cin >> selection;
	
	tolower (selection);

	do
	{
		if (selection == 'h')
		{
		find_height();
		}
		if (selection == 'd')
		{
			find_distance();
		}
		if (selection == 's')
		{
			find_slant_range();
		}
		if (selection == 'q')
		{
			cout << "Goodbye." << endl;
			return 1;
		}

		cout << "***Hardrocker Triangulator***" << endl
		<< "H - Find Height" << endl << "D - Find Distance" << endl
		<< "S - Find Slant Range" << endl << "Q - Quit Program" << endl
		<< "Your Choice: ";

		cin >> selection;
		tolower (selection);

	}while (selection != 'q' || selection != 'h' || selection != 'd' || selection != 's')

	return 1;
}

void find_height()
{
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle < 0 || angle > 90)
	{
		cout << "Angle must bee between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);

	cout << endl << "Enter distance to object: ";
	cin >> distance;

	while (distance <= 00
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance to object: ";
		cin >> distance;
	}
	
	height = distance * my_tan (radian);

	cout << endl << "The object is " << height << " units high." << endl;
}
void find_distance(double radian)
{
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle < 0 || angle > 90)
	{
		cout << "Angle must bee between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);

	cout << endl << "Enter height of the object: ";
	cin >> height;

	while (height <= 00
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance to object: ";
		cin >> distance;
	}
	
	distance = height * my_cot (radian);

	cout << endl << "The object is " << distance << " units away." << endl;
}

void find_slant_range(double radian)
{
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle < 0 || angle > 90)
	{
		cout << "Angle must bee between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);

	cout << endl << "Enter distance to object: ";
	cin >> distance;

	while (distance <= 00
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance to object: ";
		cin >> distance;
	}
	
	height = distance * my_cosec (radian);

	cout << endl << "The top of the object is " << height << " units away." << endl;

}