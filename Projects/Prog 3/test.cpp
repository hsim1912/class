#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

void my_factorial ( &pow );
double my_pow( double &number, double exponant);
double my_sin( double radian );
double my_cos( double radian );
double my_tan( double radian );
double my_cosec( double radian);
double my_sec( double radian );
double my_cot( double radian );

int main()
{
// devlare variables for usage
double angle = 11;
double rad;

// set precision for following outputs
	cout << setprecision(15);
	cout << fixed;
	cout << showpoint;
	
// start with 11 degrees and convert to radians... then display
while (angle <= 360)
{
	rad = (angle * (3.1415926535897932384626433832795 / 180));
	cout << "angle: " << angle << "radians: " << rad << endl;

	cout << "sin" << "     " << my_sin (rad) << "     " << sin(rad) << "     " << my_sin(rad) - sin(rad) << endl;
	cout << "cos" << "     " << my_cos (rad) << "     " << cos(rad) << "     " << my_cos(rad) - cos(rad) << endl;
	cout << "tan" << "     " << my_tan (rad) << "     " << tan(rad) << "     " << my_tan(rad) - tan(rad) << endl;
	cout << "csc" << "     " << my_cosec(rad) << "     " << 1.0 / sin (rad) << "     " << my_cosec(rad) - (1.0 / sin (rad)) << endl;
	cout << "sec" << "     " << my_sec (rad) << "     " << 1.0 / cos (rad) << "     " << my_sec(rad) - (1.0 / cos (rad))<< endl;
	cout << "cot" << "     " << my_cot (rad) << "     " << 1.0 / tan (rad) << "     " << my_cot(rad) - (1.0 / tan (rad))<< endl;
	angle += 25;
}

return 0;
}
void my_factorial(&pow)
{
	int minusPow = pow;
	
	while (pow > 0)
	{
		minusPow = minusPow * (pow - 1);
		pow -= 1;
	}

	pow = minusPow;
}
double my_pow( double &number, double exponant)
{
	// declare variables
	double num;
	double exp;
	int i;

	cin >> num >> exp;

	if (exp == 0)
	{
	num = 1;
	return 0;
	}

	else for (i=1 ; i < exp ; i++)
	{
		num *= num;
	}

	return 0;

}

double my_sin( double radian )
{
	double num, num2;
	double numer;
	double denom;
	int i = 0;
	double power = 0;
	double modpower = power;
	int counter = 0;


	cin >> num;

	num2 = num;
	
	numer = num;

	for ( i ; i<1000 ; i ++)
	{
		
		numer = num;
		

		if (counter % 2 == 0)
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 += numer / power;
		}
		else
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 -= numer / power;
		}
		power += 2;
		counter++;
	}

	num = num2 / denom;
	return num;
}
double my_cos( double radian )
{
	double num, num2;
	double numer;
	double denom;
	int i = 0;
	double power = 1;
	double modpower = power;
	int counter = 0;


	cin >> num;

	num2 = num;
	
	numer = num;

	for ( i ; i<1000 ; i ++)
	{
		
		numer = num;
		

		if (counter % 2 == 0)
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 += numer / power;
		}
		else
		{
		my_pow(&numer, power);
		my_factorial(&power);
		num2 -= numer / power;
		}
		power += 2;
		counter++;
	}

	num = num2 / denom;
	return num;
}
double my_tan( double radian )
{
	return (my_sin (radian) / my_cos(radian));
}
double my_cosec( double radian)
{
	return (1.0 / my_sin( radian ));
}

double my_sec( double radian )
{
	return (1.0 / my_cos(radian));
}
double my_cot( double radian )
{
	return (1/ my_tan( radian ));
