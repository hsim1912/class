/*-------------------------------------------------------------------   
   Program: lab6_2.cpp
   Author: <Fill in your name>
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description
     This program will calculate approximations of PI using Leibniz's 
     formula: pi = 4 * ( 1 - 1/3 + 1/5 - 1/7 + 1/9 - ....) 
     Multiple approximations will be calculated using increasing numbers
     of terms ( 10, 100, 1000, etc) until the result is within 0.00001%
     of the value in M_PI.

     Program displays the calculated value, the difference between this 
     result and M_PI and the number of loop iterations (terms) used.
     It stops when the error value is less than 0.0000001.

---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         abs( )
*********************************************************************/
int main( )
{
    int i = 0;
    int iter = 10;
    double pi_value = 0.0;
    double error = 0.0;

    cout << setprecision( 20 );
    cout << fixed << showpoint;



    //you write the code here to caclulate and display PI 
	
	do
	{
	
		error = 0.0;

		while (i < iter)
		{
			if ( i % 2 == 0)
			pi_value += 1 / ((i * 2.0) + 1);

			else if (i % 2 == 1)
			pi_value -= 1/((i * 2.0) + 1);
			
			i++;
		}

		pi_value = 4 * pi_value;

		error = abs (pi_value - M_PI);

		setw(100);

		cout << pi_value << "          " << error << "          " << iter << endl;

		// reset variables
	
		i = 0;
		pi_value = 0.0;
		

		iter *= 10;

	} while (error >= .000001);

    return 0;
}