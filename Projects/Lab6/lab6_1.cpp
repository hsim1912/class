/*-------------------------------------------------------------------
   Program: lab6_1.cpp
   Author: <Fill in your name >
   Course: CSC 150 - <Fill in lab section number>
   Instructor: <Fill in lab TA's name>
   Date: < Fill in today's date>
   Description:  
     This program computes and displays the first n elements of 
	 the Fibonacci sequence.  The value n must be between 2 and 
	 45. The first seven Fibonacci elements are 1, 1, 2, 3, 5, 
	 8, 13.  The user inputs the number n from the keyboard. 


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
using namespace std;


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         none
*********************************************************************/
int main()
{
	
	int number;				// input number
	int counter = 0;		// count number of elements displayed
	long fib1 = 1, fib2 = 1;	// first and second element
	long next;				// store the next element

	do
	{
		
	// loop for continued runs

	cout << "Enter the number of elements of the Fibonacci sequence: ";
	cin >> number;

	if (number < 1)
		cout << "Please enter a value between 1 and 45.\n" << endl;
	else if (number > 45)
		cout << "This program can only find up to the 45th element.\n" << endl;

	else if (number < 0)
	{
		cout << "Program exiting!" << endl;
		exit (0);
	}

	else if (number == 1)
	{
		cout << "This number is: " << fib1 << endl;
	}

	else if (number == 2)
	{
		cout << "This number is: " << fib1 << endl;
	}
	else
	{
		// Display the first two elements
	
		//Determine the other elements of the sequence
		while (number > counter)
		{
			counter++;
			next = fib1 + fib2;
			fib1 = fib2;
			fib2 = next;   
		}

		cout << "This number is: " << next << endl << endl;
	}

	fib1 = 1;
	fib2 = 1;
	counter = 2;

	}while (number >= 0);

	cout << "Program exiting!" << endl;

	return 0;

}
