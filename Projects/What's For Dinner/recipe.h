#include <iostream>
#include <fstream>
#include <string>

// pull from file list of Allergens

using namespace std;


// spices, meat, veggies, herbs, dairy, starch, fruit, sauces

class recipe
{
struct ingredient	
{
	string name;
	float weight;
	string amount;
	string type;
	string expirationDate;
};

	ingredient stuff;
	float servings;
	string *instructions;

protected:

	ostream &operator << (recipe &rec);
	ostream &operator >> (recipe &rec);

public:

	bool allergens( string *allergens );
	double multiply();
	void display();
	void digiPantryMod( char option );
};