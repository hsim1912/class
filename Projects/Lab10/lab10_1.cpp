/*--------------------------------------------------------------------
  Program: lab10_1.cpp
  Author: <Fill in your name >
  Course: CSC 150 - <Fill in lab section number>
  Instructor: <Fill in lab TA's name>
  Date: < Fill in today's date>
  Description: 
    This program initializes a 2-D array named cadmium with daily 
    readings of airborne cadmium in a room of a house every six 
    hours (6am, noon, 6pm, and midnight) for seven consecutive 
    days.  Then, it computes the highest cadmium reading,
    displaying the day it occurred. In case of two readings with 
    the same highest reading, it chooses the day of the first 
    reading. 


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <iomanip>
using namespace std;


//-------------------------------------------------------------------
// Global constants
//------------------------------------------------------------------
const int NUM_DAYS       = 7;
const int NUM_READINGS   = 4;
const char days[7][10] = {"Sunday", "Monday", "Tuesday", "Wednesday", 
	   "Thursday", "Friday", "Saturday"};
const char dayTime[4][5] = {"6am", "12pm", "6pm", "12am"};

//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
int calculateHighestReading( double cadmium[], double &cadmiumValue );


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         calculateHighestReading()
*********************************************************************/
int main( )
{
   // 2-D array of airborne cadmium readings 
   // taken in a room every six hours for seven days
   double cadmium[NUM_DAYS][NUM_READINGS] =
   { 
	   {0.044, 0.030, 0.052, 0.044}, {0.046, 0.043, 0.020, 0.065},
	   {0.053, 0.049, 0.030, 0.035}, {0.057, 0.050, 0.056, 0.061},
	   {0.042, 0.051, 0.034, 0.030}, {0.052, 0.044, 0.045, 0.048},
       {0.043, 0.040, 0.048, 0.033 }
   };

   double highCadmium = 0.0;
   double cadmiumValue = 0.0;
   int highTime;
   int highDay = 0;

   cout << fixed << showpoint << setprecision(4);

   //find highest reading for each day, store overall highest value and its day
   for (int day = 0; day < NUM_DAYS; day++)
   {
		highTime = /*call the function, passing a single row of array */
			calculateHighestReading( cadmium[day], cadmiumValue );

   
		cout << "Highest cadmium reading"<< " on " << days[highDay] << " was " << cadmiumValue << " at " << dayTime[highTime] << endl;
   }

   
   
   return 0;
}


/********************************************************************
   Function: calculateHighestReading
   Description:
      This function computes the highest cadmium reading for
	  a given day.  
   Parameters:
			      cadmium : 1-D array of daily cadmium readings
   return value:  highTime: int
   Called by:     main()
   Calls:         none
*********************************************************************/
int calculateHighestReading( double cadmium[], double &cadmiumValue )
{
   double highReading = 0.0;
   int highTime = 0;

   for (int j = 0; j < NUM_READINGS; j++)
   {
         if ( cadmium[j] > highReading )
         {
            highReading = cadmium[j];
			cadmiumValue = highReading;
            highTime = j;
         }
   }

   return highTime;
}

