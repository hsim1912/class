/*--------------------------------------------------------------------
  Program: lab10_2.cpp
  Author: <Fill in your name >
  Course: CSC 150 - <Fill in lab section number>
  Instructor: <Fill in lab TA's name>
  Date: < Fill in today's date>
  Description: This program reads in the standard size and weight data
    for reinforcing steel (rebar) used in construction and the requirements
    for a particular project. The rebar specifications and the project
    data are stored to 2D arrays, then the program displays the required
    rebar sizes, diameters, lengths, and weights, with a summary line that
    displays total length and total weight.
    
---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <fstream>
#include <iomanip>
using namespace std;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
double calc_weight( int sizes[], double rebar[][2], double reqs[][2] );
/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         calc_weight()  
*********************************************************************/
int main( )
{
    // Variable declaration
    int sizes[12] = { 0 };
    double rebar[12][2] = { 0 };
    double reqs[10][2] = { 0 };
    int num_reqs = 0;
    double weight = 0;
    double total_weight = 0;
    double total_length = 0;
    int needed_size = 0;
    int rebar_index = 0;

    int i, j;

    ifstream rebarin;
	ifstream req;
   /* open and test file rebar.txt  exit the program on error */
	rebarin.open("rebar.txt");
	if (!rebarin)
	{
		cout << "Unable to open.  Exiting program.";
		exit (1);
	}

    //read in exactly 12 records, store size number to 1D array, 
    //weight and diameter to 2D array
    for( i = 0; i < 12; i++ )
    {
        rebarin >> sizes[i];
        for( j = 0; j < 2; j++ )
		{
			rebarin.ignore();
			rebarin >> rebar[i][j];
   		}
	}

 /* close the input stream, make it ready to resuse
    open the requirements.txt file, test it.
    exit the program on error*/
	rebarin.close();

	req.open("requirements.txt");
	if (!req)
	{
		cout << "Unable to open.  Exiting program.";
		exit (0);
	}

    //read requirements, up to 10 possible pairs
    i = 0;
    while( i < 10 && req >> reqs[i][0] )
    {
        req >> reqs[i][1]; //read second item from a record
        i++;
    }

    req.close();

    num_reqs = i;   //store number of requirements read in

    cout << fixed << showpoint << setprecision(1);

    //output headings
    cout << left << setw(12) << "SIZE" << setw(12) << "DIAMETER" << setw(10)
         << "LENGTH" << setw(10) << "WEIGHT" << endl;
	j = 0;
 for( i = 0; i < num_reqs ; i++ )
    { 
     /* find the index into the rebar array so that you display correct diamter */
	//get weight of one requirement, add weight and length to accumulators
	    weight = calc_weight( sizes, rebar, reqs );
        total_weight += weight;
        total_length += reqs[i][1];

		for ( j ; j < 12 ; j++ )
		{
			if (sizes[j] == reqs[i][0])
			{
				rebar_index = j;
			}
		}
        cout << setw(10) << left <<  setprecision(0) << noshowpoint << reqs[i][0] 
             << right << setw(10) << showpoint << setprecision(3) 
             << rebar[rebar_index][1] << setprecision(1)
             << setw(10) << reqs[i][1] << setw(10) << weight << endl;
			j=0;
	}

    cout << left << setw(20) << "Total" << right << setw(10) 
         << total_length << setw(10) << total_weight << endl;

   return 0;
}


/********************************************************************
   Function: calc_weight
   Description:
     This function returns the weight of a given number of feet of a
     specified size of rebar
   Parameters:    STUDENT TO FILL IN PARAMETER DESCRIPTIONS
   Return value:  weight of rebar
   Called by:     main()
   Calls:         none
*********************************************************************/
double calc_weight( int sizes[], double rebar[][2], double reqs[][2] )
{
    double weight = 0;
	int rebar_index = 0;
	static int i;
	int needed_size = reqs[i][0];
	int feet = reqs[i][1];

	while( needed_size != sizes[rebar_index] )
        rebar_index++;

    weight = feet * rebar[rebar_index][0];
    i++;
    return weight;
}
