#include <iostream>
#include <cmath>
#include <cctype>
#include <iomanip>
using namespace std;
void find_distance();
void find_height();
void find_slant_range(double radian);
bool get_choice();
double my_cos( double num );
double my_cosec(double radian);
double my_cot(double radian);
void my_factorial(double &pow);
double my_pow(double num, double exp);
double my_sec(double radian);
double my_sin( double num );
double my_tan( double radian );

/***************************************************************************//**
 * @file prog3.cpp
 *
 * @mainpage Program 3 - Creation of functions, comparison to Cmath libraries, and use of said libraries to determine lenghts of a triangle
 *
 * @authors Alex Wulff
 *
 * @section course_section CSC-150
 *
 * @date November, 5, 2011
 *
 * @par Instructor:
 *         Dr. Schrader
 *
 * @par Course:
 *         CSC 150 � Section 5 � 3:00 pm
 *
 * @par Location:
 *         McLaury - Room 313
 *
 * @section program_section Using Taylor's Series of Approximations, I have created math functions that approach the exact value of trigonometric functions.  
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      None
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * Reduce difference between my functions and cmath's by increasing itterations of summations.
 *
 ******************************************************************************/
/*!****************************************************
 * @mainpage Main()
 *
 * @description 
 * My functions are at-first tested, then compaired against standard library deffinitions to find differences to the fifteenth decimal-place. Out put is then reset to two decimal-places 
 * My functioins are then used for the rest of the program to calculate triangle lengths for height, distance, and slant (hypotenuse) by means of calling a menu
 * function called get_choice.
 * @param[ none ]  - none
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values - none the function displays the distance then falls back to get_choice()
 ******************************************************/
 
int main()
{
	bool m;
	do
	{
/*double angle = 11;
double rad;
// set precision for following outputs
	cout << setprecision(15);
	cout << fixed;
	cout << showpoint;
	
// start with 11 degrees and convert to radians... then display
	do
	{
	while (angle <= 360)
{   // convert to radians and proceed with calculated differences between 'my_' functions and cmath
	rad = (angle * (3.1415926535897932384626433832795 / 180));
	cout << "\n  angle: " << angle << "      radians: " << rad << endl;
		
	cout << "sin" << setw(20) << my_sin (rad) << setw(20) << sin(rad) << setw(20) << my_sin(rad) - sin(rad) << endl;
	cout << "cos" << setw(20) << my_cos (rad) << setw(20) << cos(rad) << setw(20) << my_cos(rad) - cos(rad) << endl;
	cout << "tan" << setw(20) << my_tan (rad) << setw(20) << tan(rad) << setw(20) << my_tan(rad) - tan(rad) << endl;
	cout << "csc" << setw(20) << my_cosec (rad) << setw(20) << 1/sin(rad) << setw(20) << my_cosec(rad) - 1/sin(rad) << endl;
	cout << "sec" << setw(20) << my_sec (rad) << setw(20) << 1/cos(rad) << setw(20) << my_sec(rad) - 1/cos(rad) << endl;
	cout << "cot" << setw(20) << my_cot (rad) << setw(20) << 1/tan(rad) << setw(20) << my_cot(rad) - 1/tan(rad) << endl;
	angle += 25;
}
*/	// revert precision to 2 decimal places
	cout << setprecision(2) << fixed << showpoint;
m = get_choice(); //Menu time
}while (m);
return 0;
}
/*!****************************************************
 * @mainpage void Find_Distance()
 *
 * @description 
 * @par This function requests an observed angle between person and an object's top.  Then error checks the value.
 * @par The function then requests the distance and checks to make sure it is greater than 0.  After all values are
 * @par checked, the function converts the angle to radians and runs the values avgainst my cotangent function to 
 * @par find distance.
 * @param[ none ]  - none
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values - none the function displays the distance then falls back to get_choice()
 ******************************************************/

void find_distance()
{
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle <= 0 || angle >= 90)	//angle test
	{
		cout << "Angle must be between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);	//convert to radians

	cout << endl << "Enter height traveled: ";
	cin >> height;

	while (height <= 00)	//height test
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance traveled: ";
		cin >> distance;
	}
	
	distance = height * my_cot (radian);

	cout << endl << "The distance is " << distance << " units." << endl;
}
/*!****************************************************
 * @mainpage void Find_Height()
 *
 * @description 
 * @par This function requests an observed angle between person and an object's top.  Then error checks the value.
 * @par The function then requests the height and checks to make sure it is greater than 0.  After all values are
 * @par checked, the function converts the angle to radians and runs the values avgainst my cotangent function to 
 * @par find height.
 * @param[ none ]  - none
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values - none the function displays the height goes back to get_choice()
 ******************************************************/
void find_height()
{	//variables
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle <= 0 || angle >= 90)	//angle test
	{
		cout << "Angle must be between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);	//angle convert

	cout << endl << "Enter distance traveled: ";
	cin >> distance;

	while (distance <= 0.0)	//distance test
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance traveled: ";
		cin >> distance;
	}
	
	height = distance * my_tan (radian);

	cout << endl << "The total elevation change is " << height << " units high." << endl;
}
/*!****************************************************
 * @mainpage void Find_Slant_Range()
 *
 * @description 
 * @par This function requests an observed angle between person and an object's top.  Then error checks the value.
 * @par The function then requests the distance and checks to make sure it is greater than 0.  After all values are
 * @par checked, the function converts the angle to radians and runs the values avgainst my cotangent function to 
 * @par find distance.
 * @param[ none ]  - none
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values - none the function displays the hypotenuse then falls back to get_choice()
 ******************************************************/
void find_slant_range()
{
	double angle, radian, height, distance;
	cout << "Enter observed angle: ";
	cin >> angle;
	while (angle <= 0 || angle >= 90)	//angle test
	{
		cout << "Angle must be between 0 and 90 degrees" << endl;
		cout << "Enter observed angle: ";
		cin >> angle;
	}

	radian = (angle * 3.1415926535897932384626433832795 / 180);	//convert to radian

	cout << endl << "Enter distance traveled: ";
	cin >> distance;

	while (distance <= 00)	//distance test
	{
		cout << "Distance must be greater than 0."  << endl;
		cout << "Enter distance traveled: ";
		cin >> distance;
	}
	
	height = distance * my_cosec (radian);

	cout << endl << "The total distance is " << height << " units." << endl;

}
/*!****************************************************
 * @mainpage bool get_choice()
 *
 * @description 
 * @par This function displays a menu, to find: height, slant, distance or to quit.  For all choices other than quit,
 * @par the function calls the coorisponding trig functions to determine respective choices (height, hypotenuse or distance).
 * @par The function continues to loop for further calculations, evaluating both lower and uppercase choices, until quit is
 * @par sellected.  When this occurs, the funciton returns a false value to main, exiting it's own loop and main's.
 * @param[ none ] - none
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par True - Any value other than to quit.
 * @par False - Quit.  Will exit all loops
 ******************************************************/
bool get_choice()
{
char selection;	//output choices

	cout << "***Mike Rotchtikle's Triangulator***" << endl
		<< "H - Find Height" << endl << "D - Find Distance" << endl
		<< "S - Find Slant Range" << endl << "Q - Quit Program" << endl
		<< "Your Choice: ";

	cin >> selection;
	
	tolower (selection);	//verify for lower case and upper case selections

	do
	{
		if (selection == 'h')
		{
		find_height();
		}
		if (selection == 'd')
		{
			find_distance();
		}
		if (selection == 's')
		{
			find_slant_range();
		}
		if (selection == 'q')
		{
			cout << "Goodbye." << endl;	//be polite
			return false;
		}
			//loop if needed
		cout << "***Hardrocker Triangulator***" << endl
		<< "H - Find Height" << endl << "D - Find Distance" << endl
		<< "S - Find Slant Range" << endl << "Q - Quit Program" << endl
		<< "Your Choice: ";

		cin >> selection;
		tolower (selection);

	}while (selection != 'q' || selection != 'h' || selection != 'd' || selection != 's');
}

/*!****************************************************
 * @mainpage double my_cos()
 *
 * @description 
 * @par This function receives a double for radian value, and then sets up for taylor's approximations as: 1 - x^2/2! +x^4/4!... etc.
 * @par The function acomplishes this through a counter system that determines whether addition or subtraction is nessicary. and runs for 100 itterations
 * @par to help build a solid estimate of cosine's value.  After every run, the power must be reset and is done at the end of every loop.
 * @parameters - receives a double number (num) 
 *
 * @param[ in ]  num � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated cosine of radian measure
 ******************************************************/

double my_cos( double num )
{	//declare variables
	double num2 = 0;
	double numer = num;
	int i = 0;
	double power = 0;
	double power0 = power;
	int counter = 0;

	for ( i ; i<200 ; i ++)
	{
		//reset numer
		numer = num;
		
		if (counter % 2 == 0)
		{
		numer = my_pow(numer, power);
		//cout << "current power: " << numer;
		my_factorial(power);
		num2 += numer / power;
		}
		else
		{
		numer = my_pow(numer, power);
		//cout << "current power: " << numer;
		my_factorial(power);
		num2 -= numer / power;
		}
		power0 +=2;
		power = power0;
		counter++;
	}
	//finished value
	return num2;
}

/*!****************************************************
 * @mainpage double my_cosec()
 *
 * @description 
 * @par This function receives a double for radian value, and then calls for 1 / sine's measure of radian.
 *
 * @param[ in ]  radian � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated cosecant of radian measure
 ******************************************************/
double my_cosec( double radian)
{
	return (1.0 / my_sin( radian ));
}
/*!****************************************************
 * @mainpage double my_cot()
 *
 * @description 
 * @par This function receives a double for radian value, and then calls for 1 / tangent's measure of radian.
 *
 * @param[ in ]  radian � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated cotangent of radian measure
 ******************************************************/
double my_cot( double radian )
{
	return (1/ my_tan( radian ));
}

/*!****************************************************
 * @mainpage void my_factorial()
 *
 * @description 
 * @par This function receives a double for a number (power as the same poser is the factorial in the denominator,
 * @par then assigns minusPow as our factorial receiver.  Using loops for a number of power, the function then captures
 * @par a number for minusPow and sets back equal to pow as we are passing by reference.
 *
 * @param[ in, out ]  pow � number and it's factorial (after calculations)
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns nothing, because the number is passed by reference and is modified within the function.
 ******************************************************/
void my_factorial(double &pow)
{
	// declare variables
	double minusPow = pow;
	double powInitial = pow;
	int i;
	
	if (pow == 1)
	{
	return;
	}
	if (pow == 0)
	{
		minusPow = 1;
	}
	
	for (i =1 ; i<powInitial ; i++)
	{
		pow -=1;
		minusPow *= pow;
	}
	pow = minusPow;
}
/*!****************************************************
 * @mainpage double my_pow()
 *
 * @description 
 * @par This function receives a double for a number (num) and a double exponant (exp).  The function then uses if statements
 * @par to determine if 1 or num should be returned.  If not, the system then assignes a for loop where multiplication is repeated
 * @par so as to get an exponential value.
 *
 * @param[ in ]  num � radian measure for angle
 * @param[ in ]  exp � current exponent value
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double value for the power of num.
 ******************************************************/

double my_pow(double num, double exp)
	{
		// declare variables

	int i;
	double num2 = num;
	if (exp == 0)
	{
	num = 1;
	return num;
	}
	//check for crazyness and root exponents
	else if (exp < 1)
	{
		cout << "Invalid.  Exiting program." << endl;
		exit (0);
	}
	// test for exponent of 1
	else if (exp == 1)
	{
		return num;
	}
		for (i=1 ; i < exp ; i++)
	{
		num2 *= num;
	}

	return num2;
}
/*!****************************************************
 * @mainpage double my_sec()
 *
 * @description 
 * @par This function receives a double for radian value, and then calls for 1 / cosine's measure of radian.
 *
 * @param[ in ]  radian � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated cosine of radian measure
 ******************************************************/
double my_sec( double radian )
{
	return (1.0 / my_cos(radian));
}

/*!****************************************************
 * @mainpage double my_sin()
 *
 * @description 
 * @par This function receives a double for radian value, and then sets up for taylor's approximations as: x - x^3/3! +x^5/5!... etc.
 * @par The function acomplishes this through a counter system that determines whether addition or subtraction is nessicary. and runs for 100 itterations
 * @par to help build a solid estimate of sine's value. After every run, the power must be reset and is done at the end of every loop.
 * @param[ in ]  num � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated sine of radian measure
 ******************************************************/
double my_sin( double num )
{	//declare variables
	double num2 = 0;
	double numer = num;
	int i = 0;
	double power = 1;
	double power0 = power;
	int counter = 0;

	for ( i ; i<200 ; i ++)
	{	//reset numerator
		numer = num;
		
		if (counter % 2 == 0)
		{
		numer = my_pow(numer, power);
		//cout << "current power: " << numer;
		my_factorial(power);
		num2 += numer / power;
		}
		else
		{
		numer = my_pow(numer, power);
		//cout << "current power: " << numer;
		my_factorial(power);
		num2 -= numer / power;
		}
		power0 +=2;
		power = power0;
		counter++;
	}
	
	return num2;
}

/*!****************************************************
 * @mainpage double my_tan()
 * @description 
 * @par This function receives a double for radian value, and then calls for sine / cosine's measure of radian.
 *
 * @param[ in ]  radian � radian measure for angle
 *
 * @authors Alex Wulff
 *
 * @date November, 5, 2011
 *
 * @return values
 * @par This function returns a double for the evaluated tangent of radian measure
 ******************************************************/
double my_tan( double radian )
{
	return (my_sin (radian) / my_cos(radian));
}