/****************************************************************************//**
 * @file
 *
 * @author Andrew Koc
 *
 * @brief This file contains the queue template class and its functions
 *
 ******************************************************************************/
#ifndef _QUEUE_H_
#define _QUEUE_H_
#include <iostream>

using namespace std;

/****************************************************************************//**
 * @brief The template class queue holds multiple items in a FIFO order
 *
 * @details This structure can hold any data type in which the = operator has
 * been overloaded.  It has a Constructor, Copy Constructor, and Deconstructor
 * as well as three other functions, each with two different names for easier
 * use.  Somepeople might be used to using push rather than enque
 *
 ******************************************************************************/
template <typename _TY>
class queue
{
public:
	queue();
	queue( queue<_TY> &q );
	~queue();
	bool push( _TY item );
	bool enque( _TY item );
	bool pop( _TY &retval );
	bool deque( _TY &retval );
	bool top( _TY &retval )const;
	bool front( _TY &retval )const;
	int get_length()const;
	bool is_empty()const;
private:

	/*!
	 * @brief holds a value and a pointer to the next value
	 */
	struct node
	{
		_TY item;/*!<the value of the node*/
		node *next;/*!<the address of the next node*/
	};

	node *headptr;/*!<the pointer to the first value*/
	node *endptr;/*!<the pointer to the last value*/
	int len;/*!<the number of nodes in the queue*/
};

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Constructor for queue template.  Sets both pointers to
 * NULL and the length to 0
 *
 ******************************************************************************/
template <typename _TY>
queue<_TY>::queue()
{
	headptr = NULL;
	endptr = NULL;
	len = 0;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Copy constructor for queue class.  Requires that whatever
 * data type the queue holds( int, char, float, or a struct ) has an overloaded
 * = operator
 *
 * @param[in] q - the queue to be copied
 *
 ******************************************************************************/
template <typename _TY>
queue<_TY>::queue( queue<_TY> &q )
{
	node *get = NULL;
	node *place = NULL;
	headptr = NULL;
	endptr = NULL;
	len = 0;
	//make sure the queue being passed in is not empty
	if ( q.headptr == NULL )
		return;
	
	//Make new node for first value
	headptr = new (nothrow) node;
	if ( headptr == NULL )
		return;

	//fill new node with the value it should have
	headptr->item = q.headptr->item;
	headptr->next = NULL;
	place = headptr;
	get = q.headptr;

	//get the rest of the nodes, if any
	while ( get->next != NULL )
	{
		//Make new node for next value
		place->next = new(nothrow) node;

		//Move place onto new node
		place = place->next;

		//check to make sure memory was allocated
		if ( place == NULL )
			return;

		//move get to the value to be copied in
		get = get->next;

		//fill new node with the value it should have
		place->item = get->item;
		place->next = NULL;

		//increment len
		len++;
	}

	//set endptr to the last node
	endptr = place;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Deconstructor for the queue class.  Walks through the queue
 * while it is not empty and deletes each node.
 *
 ******************************************************************************/
template <typename _TY>
queue<_TY>::~queue()
{
	node *temp = NULL;
	while ( headptr != NULL )
	{
		temp = headptr;
		headptr = headptr->next;
		delete temp;
	}
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Adds an item to the end of the queue.  If the queue is
 * empty then it makes headptr and endptr point to the item, else endptr->next
 * points to the new node
 *
 * @param[in] item - the data value to be used for the new node.
 *
 * @returns true - the node was inserted into the queue
 * @returns false - the node was not inserted into the queue
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::push( _TY item )
{
	node *val = NULL;
	val = new(nothrow) node;
	if ( val == NULL )
		return false;
	val->item = item;
	val->next = NULL;
	if ( headptr == NULL )
	{
		headptr = val;
		endptr = val;
		len = 1;
		return true;
	}
	endptr->next = val;
	endptr = val;
	len = len + 1;
	return true;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Checks if there is an item in the queue to remove.  If
 * there is then it removes this item and decrements len. Requires that whatever
 * the data type of _TY has an overloaded = operator.
 *
 * @param[out] retval - the value of the item removed
 *
 * @returns true - the item was removed and stored in retval
 * @returns false - there was no item to remove
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::pop( _TY &retval )
{
	node *temp;
	if ( headptr == NULL )
		return false;
	temp = headptr;
	headptr = headptr->next;
	retval = temp->item;
	len = len - 1;
	delete temp;
	return true;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Retrives the value of the item at the front of the queue.
 *  Requires that the data type _TY has an overloaded = operator
 *
 * @param[out] retval - the value of the front item
 *
 * @returns true - the value was found and stored
 * @returns false - there was no item at the front of the queue
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::top( _TY &retval )const
{
	if ( headptr == NULL )
		return false;
	retval = headptr->item;
	return true;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Gives the number of items in the queue.
 *
 * @returns len - the number of items in the queue
 *
 ******************************************************************************/
template <typename _TY>
int queue<_TY>::get_length()const
{
	return len;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Determines if the queue is empty.
 *
 * @returns true - the queue is empty
 * @returns false - the queue contains at least one item
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::is_empty()const
{
	return len == 0;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Adds an item to the end of the queue.  If the queue is
 * empty then it makes headptr and endptr point to the item, else endptr->next
 * points to the new node
 *
 * @param[in] item - the data value to be used for the new node.
 *
 * @returns true - the node was inserted into the queue
 * @returns false - the node was not inserted into the queue
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::enque( _TY item )
{
	return push(item);
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Checks if there is an item in the queue to remove.  If
 * there is then it removes this item and decrements len. Requires that whatever
 * the data type of _TY has an overloaded = operator.
 *
 * @param[out] retval - the value of the item removed
 *
 * @returns true - the item was removed and stored in retval
 * @returns false - there was no item to remove
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::deque( _TY &retval )
{
	return pop(retval);
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Retrives the value of the item at the front of the queue.
 *  Requires that the data type _TY has an overloaded = operator
 *
 * @param[out] retval - the value of the front item
 *
 * @returns true - the value was found and stored
 * @returns false - there was no item at the front of the queue
 *
 ******************************************************************************/
template <typename _TY>
bool queue<_TY>::front( _TY &retval )const
{
	return top(retval);
}

#endif