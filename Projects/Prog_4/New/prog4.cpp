/*************************************************************************//**
 * @file 
 *
 * @brief This file contains all functions used to simulate a printer being
 * used through out the day.  The simulation can be ran with random numbers
 * being generated or read from a file.
 *
 * @mainpage Program 4 - Printer Simulation
 * 
 * @section course_section CSC 250
 *
 * @authors Andrew Koc, Andrew Rossow, Alex Wulff
 * 
 * @date April, 27, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program takes parameters from the user, average time between
 * prints, the time to print each page, and whether recorded values will be
 * used or random numbers for a simulation of printing in a local legal office.
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog4.exe [average time between prints] [time to print a page] <option>
   d:\> c:\bin\c:\> Prog4.exe [average time between prints] 
   [time to print a page] <option>
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug - none.
 * 
 * @todo - debug and try to crash.
 * 
 * @par Modifications and Development Timeline: 
 * @verbatim 
 * Date          Modification 
 * ------------  -------------------------------------------------------------- 
 *
 *      4/13/12         - Set up team and started basic argument checking and made
 *                              main's structure.
 *      4/14/12         - Implimented a template class for queue's and added structure
 *                              for Documents.
 *      4/15/12         - Fixed glitch with printing past close of business (28800 
 *                              seconds).  Set additional functions for reading from a file
 *                              for statistics on prints and pages using a generated sample.
 *                      - Altered random functions for more even spread of random
 *                              numbers.
 *                      - Simulation using random numbers and files written
 * @endverbatim
 *
 *****************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include "queue.h"

using namespace std;

/********************************************************************************
 *                         Function Prototypes
 *******************************************************************************/
bool arg_check(int argc, char **argv, int &time, int &timePrint, char &option);
void usage_menu();
void mem_err();
int page_rand();
int Fpage_rand( ifstream &in );
int send_rand( int curr_time, int avg );
int Fsend_rand ( ifstream &in, int curr_time, int avg );
void R_sim( int time_interval, int print_time );
void F_sim( int time_interval, int print_time );
bool open_input( ifstream &arrival, ifstream &pages );
void close_input( ifstream &in1, ifstream &in2 );
void stats( int time_interval, int print_time, int jobs, int idle, int docs );

/*!
 * @brief Structure contains relvent information about a document to this
 * simulation.
 *
 * @details It holds the number of pages in the document, the time it was sent
 * to the printer, the time when it would exit the printer, and the ammount
 * of time it would take to print the document.
 *
 * The only time the time_dequeued is actually filled is when it is being
 * checked when the item would finish printing, everything else is set
 * before it goes into the queue.
 */
struct document
{
	int pages;/*!<The number of pages in the document*/
	int time_arrived;/*!<The time the document arrives at the printer*/
	int time_dequeued;/*!<The time when the printer finishes printing*/
	int print_time;/*!<The ammount of time it takes to print the document*/

	/*!
	 * @brief The = operator assigns all of the data from one document
	 * into another.  It was overloaded like this so that the document
	 * structure can be used in the queue.
	 */
	bool operator = (document d)
	{
		pages = d.pages;
		time_arrived = d.time_arrived;
		time_dequeued = d.time_dequeued;
		print_time = d.print_time;
		return true;
	}
};

/********************************************************************************
 *             Constant Variables, defines and Enums
 *******************************************************************************/
/*!
 * @brief The total number of seconds in a work day
 */
const int dayTime = 28800;

/****************************************************************************//**
 * @authors Andrew Koc and Alex Wulff
 *
 * @par Description: Checks the command line arguments then, assuming they're
 * all valid, runs the simulation of the printer.  If -r was specified the 
 * simulation runs using random numbers seeded by the time that the program was
 * executed.  If -f was specified then it reads random numbers from two seperate
 * files.
 *
 * @param[in] argc - then number of arguments being passed to the program
 * @param[in] argv - An array of strings holding the arguments passed to the
 * program
 *
 * @returns 0 - the program ran correctly.
 * @returns -1 - An error occured.
 *
 ******************************************************************************/
int main( int argc, char **argv )
{
	srand( (int) time(NULL) );
	int time_interval;
	int print_time;
	char option;

	//check proper number of arguments
	if ( !arg_check( argc, argv, time_interval, print_time, option ) )
	{
		return -1;
	}

	if ( option == 'R' )
	{
		//execute with random numbers
		R_sim( time_interval, print_time );
	}
	else if ( option == 'F' )
	{
		F_sim( time_interval, print_time );
	}

	return 0;
}

/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This function uses a count provided by main to determine
 * if usage has been set correctly.  Should the count of arguments passed be
 * any number other than fout (program name, average time between prints,
 * time to print a page, simmulation type) it can be assumed that the user
 * has provided incorrect arguments, so a usage menu will be presented.
 *
 * @param[in] argc: a simple integer representing the number of
 * arguments from command prompt.
 * @param[in] **argv: dynamically sized array of characters, containing the
 * user's arguements as entered at command prompt.
 * @param[out] time: the average time between each document being sent to
 * the printer.
 * @param[out] timePrint: the ammount of time it takes to print a page
 * @param[out] option: a character to be assigned a value (R or F) to
 * coorespond with the type of simmulation to run.
 *
 * @returns - True: if the number of arguments is 4, and the option, time
 * and timePrint are entered correctly
 * @returns - False: if the number of arguments is not 4, or the option,
 * average time or print time is invalid.
 *
 ******************************************************************************/
bool arg_check(int argc, char **argv, int &time, int &timePrint, char &option)
{
        char  *temp;
        string compare;
        if (argc != 4)  // usage check
        {
                usage_menu();
                return false;
        }
        // check option request
		temp = argv[argc - 3];
		time = atoi(temp);
		compare.assign(argv[argc-1]);
		if ( compare != "-f" && compare != "-r" )
		{
			usage_menu();
			return 0;
		}
		
		option = compare[1];
		option = toupper(option);
		timePrint = atoi(argv[argc-2]);
		if ( timePrint < 1 )
		{
			usage_menu();
			return false;
		}
		
		time = atoi(argv[argc-3]);
		if ( time < 31 )
		{
			usage_menu();
			return false;
		}
                
        return 1;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Outputs correct usage information to the user.  
 *
 ******************************************************************************/
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\\... prog4.exe [average time between prints]" <<
		"[time to print a page] <option>" << endl << endl;
	cout << "Time should be given in seconds." << endl << endl;
	cout << "<option>: \n\"-r\": used for internally generated random times."
		<< endl << "\"-f\": used for external times to be provided." << endl;

	cout << "\n***Note***\n\n";
	cout << "External times should be named \"arivals.rand\" and"
		<< "\n\"pages.rand\".  These files should exist within the same\n" 
		<< "directory as prog4.exe at run-time." << endl;
	return;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Generates a random number between 15 and 25
 *
 * @returns pages - The number between 15 and 25
 *
 ******************************************************************************/
int page_rand()
{
	int pages;
	
	//gives us a number between 0 - 10 adds 15 to it
	//giving us a number between 15 - 25
	pages = rand() % 11 + 15;
	
	return pages;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Generates a random number between 15 and 25 based off of a
 * file
 *
 * @param[in] in - the file stream to read from
 *
 * @returns pages - The number between 15 and 25
 *
 ******************************************************************************/
int Fpage_rand( ifstream &in )
{
	int pages, random;
	//gets random number from file
	in >> random;
	//gives us a number between 0 -10 adds 15 to it
	//giving us a number between 15 - 25
	pages = random % 11 + 15;

	return pages;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Generates a random number between -30 and 30, modifies the
 * average time by that much then adds that to the current time to give the new
 * insert time.
 *
 * @param[in] curr_time - the time the last document was sent to the printer
 * @param[in] avg - the average time between documents being sent to the printer
 *
 * @returns time - The new insert time
 *
 ******************************************************************************/
int send_rand( int curr_time, int avg )
{
	int time;
	
	//gives us a number between 0 and 60
	time = rand() % 61;
	//makes the number between -30 and 30
	time = time - 30;
	//modifies the average time by +-30
	time = time + avg;
	//adds this to the current time to give the arrival time
	time = time + curr_time;

	return time;
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Generates a random number between -30 and 30, modifies the
 * average time by that much then adds that to the current time to give the new
 * insert time.  The random number is read from a file.
 *
 * @param[in] in - the file stream to read the random number from
 * @param[in] curr_time - the time the last document was sent to the printer
 * @param[in] avg - the average time between documents being sent to the printer
 *
 * @returns time - The new insert time
 *
 ******************************************************************************/
int Fsend_rand( ifstream &in, int curr_time, int avg )
{
	int time, random;
	//read in random number from file
	in >> random;

	//gives us a number between 0 and 60
	time = random % 61;
	//makes the number between -30 and 30
	time = time - 30;
	//modifies the average time by +-30
	time = time + avg;
	//adds this to the current time to give the arrival time
	time = time + curr_time;

	return time;
}

/****************************************************************************//**
 * @author Andrew Koc and Alex Wullf
 *
 * @par Description: Simulates documents being printed on a printer throughout
 * the day.  It keeps track of the number of documents printed and the ammount
 * of time that the printer is idle.
 *
 * @param[in] time_interval - the average ammount of time between prints
 * @param[in] print_time - the ammount of time it takes to print one page
 *
 ******************************************************************************/
void R_sim( int time_interval, int print_time )
{
	queue<document> Que;
	document Doc;
	document temp;
	int idle = 0;
	int curr_time = 0;
	int printer_time = 0;
	int jobs = 0;
	int docs_left;

	//Generate Document
	Doc.pages = page_rand();
	Doc.print_time = print_time * Doc.pages;
	Doc.time_arrived = send_rand( curr_time, time_interval );
	curr_time = Doc.time_arrived;
	do
	{
		//if the queue is empty increase idle time
		if ( Que.is_empty() )
		{
			idle += Doc.time_arrived - printer_time;
			//move the printers time to when it starts working again
			printer_time = Doc.time_arrived;
			//Put Doc in queue, if it cannot be, exit program
			if ( !Que.enque(Doc) )
				mem_err();
		}
		//check if first print job would be done
		else
		{
			//get first print job
			Que.front( temp );
			//find when the document would finish printing and be removed
			temp.time_dequeued = printer_time + temp.print_time;
			//If it would finish printing before the new one was sent
			if ( Doc.time_arrived > temp.time_dequeued )
			{
				do
				{
					//Remove the item
					Que.deque(temp);
					//Increse the printers time by the ammount of time it took
					//to print the document
					printer_time += temp.print_time;
					//Increase number of jobs done
					jobs++;
					//Move to the next document if applicable
					if ( !Que.is_empty() )
					{
						Que.front( temp );
						temp.time_dequeued = printer_time + temp.print_time;
					}
				} while ( !Que.is_empty() && 
					Doc.time_arrived > temp.time_dequeued );

			}
			//If the queue is now empty
			//Increase the idle time
			if ( Que.is_empty() )
			{
				idle += Doc.time_arrived - printer_time;
				//make the printer time when it starts working again
				printer_time = Doc.time_arrived;
			}

			//Put Doc in queue, if it cannot be, exit program
			if ( !Que.enque(Doc) )
				mem_err();

		}

		//Generate new document
		Doc.pages = page_rand();
		Doc.print_time = print_time * Doc.pages;
		Doc.time_arrived = send_rand( curr_time, time_interval );
		curr_time = Doc.time_arrived;
	} while( curr_time < dayTime );

	while ( !Que.is_empty() && printer_time < dayTime )
	{
		Que.deque( temp );
		printer_time += temp.print_time;
		jobs++;
	}

	//Display stats
	docs_left = Que.get_length();
	stats( time_interval, print_time, jobs, idle, docs_left );
}

/****************************************************************************//**
 * @author Andrew Koc and Alex Wullf
 *
 * @par Description: Simulates documents being printed on a printer throughout
 * the day.  It keeps track of the number of documents printed and the ammount
 * of time that the printer is idle. The random numbers are pulled from two
 * file streams filled with random numbers.
 *
 * @param[in] time_interval - the average ammount of time between prints
 * @param[in] print_time - the ammount of time it takes to print one page
 *
 ******************************************************************************/
void F_sim( int time_interval, int print_time )
{
	queue<document> Que;
	document Doc;
	document temp;
	ifstream arrival, pages;
	int idle = 0;
	int curr_time = 0;
	int printer_time = 0;
	int jobs = 0;
	int docs_left;

	//open input files
	if ( !open_input( arrival, pages ) )
	{
		cout << "Input files could not be opened. Ending program\n";
		exit(1);
	}
	//Generate Document
	Doc.pages = Fpage_rand( pages );
	Doc.print_time = print_time * Doc.pages;
	Doc.time_arrived = Fsend_rand( arrival, curr_time, time_interval );
	curr_time = Doc.time_arrived;
	do
	{
		//if the queue is empty increase idle time
		if ( Que.is_empty() )
		{
			idle += Doc.time_arrived - printer_time;
			//move the printers time to when it starts working again
			printer_time = Doc.time_arrived;
			//Put Doc in queue, if it cannot be, exit program
			if ( !Que.enque(Doc) )
				mem_err();
		}
		//check if first print job would be done
		else
		{
			//get first print job
			Que.front( temp );
			//find when the document would finish printing and be removed
			temp.time_dequeued = printer_time + temp.print_time;
			//If it would finish printing before the new one was sent
			if ( Doc.time_arrived > temp.time_dequeued )
			{
				do
				{
					//Remove the item
					Que.deque(temp);
					//Increse the printers time by the ammount of time it took
					//to print the document
					printer_time += temp.print_time;
					//Increase number of jobs done
					jobs++;
					//Move to the next document if applicable
					if ( !Que.is_empty() )
					{
						Que.front( temp );
						temp.time_dequeued = printer_time + temp.print_time;
					}
				} while ( !Que.is_empty() && 
					Doc.time_arrived > temp.time_dequeued );

			}
			//If the queue is now empty
			//Increase the idle time
			if ( Que.is_empty() )
			{
				idle += Doc.time_arrived - printer_time;
				//make the printer time when it starts working again
				printer_time = Doc.time_arrived;
			}

			//Put Doc in queue, if it cannot be, exit program
			if ( !Que.enque(Doc) )
				mem_err();

		}

		//Generate new document
		Doc.pages = Fpage_rand( pages );
		Doc.print_time = print_time * Doc.pages;
		Doc.time_arrived = Fsend_rand( arrival, curr_time, time_interval );
		curr_time = Doc.time_arrived;
	} while( curr_time < dayTime );

	while ( !Que.is_empty() && printer_time < dayTime )
	{
		Que.deque( temp );
		printer_time += temp.print_time;
		jobs++;
	}

	//Display stats
	docs_left = Que.get_length();
	stats( time_interval, print_time, jobs, idle, docs_left );
	close_input( arrival, pages );
}

/*!**************************************************************************//**
 * @author Alex Wullf
 *
 * @par Description: Opens arrival and pages file streams to read random numbers
 * from.
 *
 * @param[out] arrival - the arrival file stream
 * @param[out] pages - the pages file stream
 *
 * @returns true - both file streams were opened
 * @returns false - One or both file streams could not be opened
 *
 ******************************************************************************/
bool open_input ( ifstream &arrival, ifstream &pages )
{
	//open input files
	arrival.open("arrival.rand");
	pages.open("pages.rand");
	//check that both opened
	if ( !arrival || !pages )
		return false;
	return true;
}

/*!**************************************************************************//**
 * @author Alex Wullf
 *
 * @par Description: Closes two input file streams.
 *
 * @param[out] in1 - the first input file stream
 * @param[out] in2 - the second input file stream
 *
 ******************************************************************************/
void close_input ( ifstream &in1, ifstream &in2 )
{
	in1.close();
	in2.close();
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Displays an error about not having enough memeory then exits
 * the program.
 *
 ******************************************************************************/
void mem_err()
{
	cout << "Simulation requires more memory than available" << endl;
	exit(0);
}

/****************************************************************************//**
 * @author Andrew Koc
 *
 * @par Description: Displays the stats of the simmulation.
 *
 * @param[in] time_interval - the average time between prints
 * @param[in] print_time - the ammount of time to print a page
 * @param[in] jobs - the number of documents printed
 * @param[in] idle - the ammount of time the printer was idle
 * @param[in] docs - the number of documents still in the printers queue
 *
 ******************************************************************************/
void stats( int time_interval, int print_time, int jobs, int idle, int docs )
{
	cout << "Number of seconds between printing documents (arrival) : "
		<< time_interval << '\n'
		<< "Number of seconds to print a page: "
		<< print_time << '\n'
		<< "Documents printed: "
		<< jobs << '\n'
		<< "Number of seconds printer idle: "
		<< idle << '\n'
		<< "Number of documents still in queue: "
		<< docs << '\n';
}