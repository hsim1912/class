#include "queue.h"

	queue::queue()
	{
		headptr = NULL;
		queue::count = 0;
	}
	queue::queue (queue &alpha)
	{
		// make temp nodes and set headptr = Null for new
		node *alphaTemp = new (nothrow) node;
		node *newTemp;

		headptr = NULL;
		
		count = 0;

		if (alpha.empty())
			return;

		alphaTemp = alpha.headptr;
		newTemp = headptr;

		do
		{
			newTemp = new (nothrow) node;
			newTemp->pages = alphaTemp->pages;
			newTemp->time = alphaTemp->time;
			newTemp->next = NULL;
			count ++;
		}while (alphaTemp != NULL);
		newTemp->next = NULL;

	}
	queue::~queue()
	{
		if (empty())
		{
			return;
		}
			
		node *temp = new (nothrow) queue::node;
		
		while (headptr->next != NULL)
		{
			temp = headptr;
			headptr = headptr->next;
			delete temp;
		}
		headptr =NULL;
	}


	// need only insert from back of the list
	bool queue::push (int doc, int time)
	{
	node *temp;
	temp = new (nothrow) queue::node;
	node *current;
	current = new (nothrow) queue::node;
	node *previous;
	previous = new (nothrow) queue::node;
	current = headptr;
	previous = headptr;
	temp->pages = doc;
	if ( queue::empty() )
	{
		temp->next = NULL;
		headptr = temp;
		queue::count ++;
		return true;
	}
	while( current->next != NULL)
	{
		previous = current;
		current = current->next;
	}
	current -> next = temp;
	temp -> next = NULL;
	queue::count ++;
	return true;
}
	bool queue::pop (int &doc, int &time)
{
	node *current = new (nothrow) queue::node;
	node *previous = new (nothrow) queue::node;
	current = headptr;
	previous = headptr;
	
	if (current == NULL)
	return false;
	current = current->next;
	current  = headptr;
	doc = previous->pages;
	time = previous->time;
	
	delete previous;
	queue::count--;
	
	return 1;
}

	bool queue::top (int &doc, int &time)
	{
		node *temp;
		temp = headptr;
		if (temp == NULL)
			return 0;
		doc = temp->pages;
		time = temp->time;
		return 1;
	}
	bool queue::empty()
	{
		node *temp = headptr;
		if (temp == NULL)
			return 1;
		else return 0;
	}