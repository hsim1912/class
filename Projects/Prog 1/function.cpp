/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the functions needed for prog1.cpp.  They are
 * defined in function.h.
 ****************************************************************************/

#include "function.h"

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will test the arguments sent into main from command prompt. 
 * Using argc, the function will use strings to compare the values passed in 
 * for correct usage.
 *
 * @param[in] (value) arguments:  an integer base for the count of arguments, 
 * passed this way to reduce input being modified more than needed.
 * @param[in] (reference) *argv[]: the array of arguments at command level 
 * passed in as an array to be further modified to individual strings.
 * @param[in/out] (reference) s1, s2, s3: These strings are empty at compile time,
 * but depending upon the argc, might need multiple to further handle options 
 * of usage.
 *
 * @calls[out] usage_menu: if validation of arguments should fail, a usage menu 
 * will be presented to the user, then return false to main.
 * @returns - True: should the conditions be met of correct argument count with
 * correct arguments.
 * @returns - False: should the conditions of argument count or the arguments
 * themselves be invalid.
 *
 ******************************************************************************/
bool arg_check( int arguments, char *argv[], string &s1, string &s2, string &s3)		
	// check number of arguments in argc and test for correct
	// usage.
{
	int b;
	if (arguments < 4 || arguments > 6)
		{
		usage_menu();
		return false;
		}
	if (arguments == 4)
	{
		s1 = argv[1];
		if (s1.compare ("-oa") !=0 && s1.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
	if (arguments == 5)
	{
		s1 = argv[1];
		if ( s1.compare ("-n") !=0 && s1.compare ("-p") !=0 
			&& s1.compare ("-s") !=0 && s1.compare ("-g") !=0 
			&& s1.compare ("-c") !=0)
		{
		usage_menu();
		return false;
		}
		s2 = argv[2];
		if (s2.compare ("-oa") !=0 && s2.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
		if (arguments == 6)
	{
		s1 = argv[1];
		if ( s1.compare ("-n") !=0 && s1.compare ("-p") !=0 &&
			s1.compare ("-s") !=0 && s1.compare ("-g") !=0 && 
			s1.compare ("-c") !=0 && s1.compare ("-b") !=0)
		{
		usage_menu();
		return false;
		}
		s2 = argv[2];
		b = atoi (argv[2]);
		if ( b < -255 || b > 255)
		{
			usage_menu();
			return false;
		}
		s3 = argv[3];
		if (s3.compare ("-oa") !=0 && s3.compare ("-ob") !=0)
		{
		usage_menu();
		return false;
		}
	}
	return true;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to brighten the
 * image by an intiger value
 *
 * @param[in/out] (reference) pic:  Structure image will be passed by reference
 * so the modifications made will be saved when returning to main for writing.
 * @param[in] (value) brighten: an integer that will be used for the 
 * brightening of the image given that every value is increased by this int.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void bright(image &pic, int brighten)
{
	int i = 0;
	int j = 0;
	for (i; i<pic.rows; i++)	// set for loop walk thru to take absolute value
	{							// of 255 - current color value and check to keep between
		for (j; j<pic.cols; j++)// 0 and 255.
		{
			if (brighten + (int) pic.redgray[i][j] > 255)
				pic.redgray[i][j] = (char) 255;
			else if (brighten + (int) pic.redgray[i][j] < 0)
				pic.redgray[i][j] = 0;
			else
			pic.redgray[i][j] = (char) (brighten + (int) pic.redgray[i][j]);
			if (brighten + (int) pic.green[i][j] > 255)
				pic.green[i][j] = (char) 255;
			else if (brighten + (int) pic.green[i][j] < 0)
				pic.green[i][j] = 0;
			else
			pic.green[i][j] = (char) (brighten + (int) pic.green[i][j]);
			if (brighten + (int) pic.blue[i][j] > 255)
				pic.blue[i][j] = (char) 255;
			else if (brighten + (int) pic.blue[i][j] < 0)
				pic.blue[i][j] = 0;
			else
			pic.blue[i][j] = (char) (brighten + (int) pic.blue[i][j]);
		}
		j = 0;
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function take the dynamicaly created arrays within pic and will free
 * the memory back to the operating system.
 *
 * @param[in/out] (reference) pic:  Structure of image will be 
 * passed by reference as the removal of pointers to dynamic arrays needs 
 * reference
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void clear_array (image &pic)
{
	int i;
	for (i = 0; i < pic.rows; i++)
	{
		delete []pic.redgray[i];
		delete []pic.green[i];
		delete []pic.blue[i];
	}
	delete []pic.redgray;
	delete []pic.green;
	delete []pic.blue;

	return;
}

/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to adjust the
 * contrast of the image by averaging adjacent pixel values.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
  * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void contrast(image &pic)
{
	int i= 0;
	int j= 0;
	int contrast_scale;
	int min = 255;
	int max = 0;
	for (i; i<pic.rows; i++)//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<pic.cols; j++)
		{
			pic.redgray[i][j] = .3*(int)pic.redgray[i][j] + 
			.6*(int)pic.green[i][j] + .1*(int)pic.blue[i][j];
			if ((int) pic.redgray[i][j] > max)
			{
				max = (int) pic.redgray[i][j];
			}
			if ((int) pic.redgray[i][j] < min)
			{
				min = (int) pic.redgray[i][j];
			}
		}
		j = 0;
	}
	contrast_scale = 255.0 / (max - min);

	for (i = 0; i<pic.rows; i++)//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j = 0; j<pic.cols; j++)
		{
			pic.redgray[i][j] = contrast_scale * (int) pic.redgray[i][j];
		}
	}		
		
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to balance the 
 * current aspects of the image (red, green, blue) and construct a gray scale
 * version.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
  * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void gray(image &pic)
{
	int i= 0;
	int j= 0;
	for (i; i<pic.rows; i++)//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<pic.cols; j++)
		{
			pic.redgray[i][j] = .3*(int)pic.redgray[i][j] + 
			.6*(int)pic.green[i][j] + .1*(int)pic.blue[i][j];
		}
		j = 0;
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to determine how
 * to proceed with the desired modifications and calls there from.
 *
 * @param[in/out] (reference) pic and pic2:  Structures of image will be 
 * passed by reference as the original picture has to be, and the 
 * modifications made will be saved when returning to main for writing of
 * pic2 or the new image.
 * @param[in] (value) option: this character will be used to set a switch
 * for which modification needs to be made.
 * @param[in] (value) brighten: this integer is used only in an instance of
 * brightening the photo.  Should it need to be passed, it will be ready
 * to do so.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] will call the following functions based on switch:
 * negate(), bright(), sharpen(), smooth(), grey(), contrast(),
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void manip(image &pic, image &pic2, char option, int brighten)
{    
	int choice = (int) option;

	switch (choice)
	{
		case (int) 'n':
			negate(pic);
			break;
		case (int) 'b':
			bright (pic, brighten);
			break;
		case (int) 'p':
			sharpen(pic, pic2);
			break;
		case (int) 's':
			smooth(pic, pic2);
			break;
		case (int) 'g':
			gray(pic);
			break;
		case (int) 'c':
			contrast(pic);
			break;
		default:
			return;
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will take a string for the potential values of manipulation 
 * to be used on the image, and will cataloge them for further use.
 *
 * @param[in] (reference) option:  this string will contain the options 
 * given at run-time, and having been validated, will run the comparison
 * and assign a value for return
 *
 * @calls[out] none.
 *
 * @returns - a single character that will be used for later evaluation on which
 * modification is desired.
 * @returns - will exit if an error should occur and was not caught previously
 * yeilding an exit code of 3.
 *
 ******************************************************************************/
char manip_options(string option)
{
	if ( option.compare("-n") == 0)
		return 'n';
	if ( option.compare("-b") == 0)
		return 'b';
	if ( option.compare("-p") == 0)
		return 'p';
	if ( option.compare("-s") == 0)
		return 's';
	if ( option.compare("-g") == 0)
		return 'g';
	if ( option.compare("-c") == 0)
		return 'c';
	usage_menu();
	exit (3);
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will invert each value in the image's structure to be the
 * opposite value (255 - current value) to produce an inverted color spectrum.
 *
 * @param[in/out] (reference) pic: the structure which will be modified to
 * produce the inverted colorscheme.
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void negate( image &pic)
{
	int i = 0;
	int j = 0;
	for (i; i<pic.rows; i++)//set for loop walk thru to take absolute value
	{								// of 255 - current color value 
		for (j; j<pic.cols; j++)
		{
			pic.redgray[i][j] = (char) (255 - (int) pic.redgray[i][j]);
			pic.green[i][j] = (char) (255 - (int) pic.green[i][j]);
			pic.blue[i][j] = (char) (255 - (int) pic.blue[i][j]);
		}
		j = 0;
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to open an input
 * file for reading.
 *
 * @param[in/out] (reference) fin: file for input of ifstream, that will be
 * opened and tested with the name given from comandline.
 * @param[in/out] (reference) string1: used to carry the name that will be 
 * opened by fin.open.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 * @returns - exits with code of 1 should fin not open and prompts error.
 *
 ******************************************************************************/
void open_input(ifstream &fin, string &string1 )
{
	char open[20];
	strcpy(open, string1.c_str());
	fin.open( open );
	
	if (!fin)
	{
		cout << "Failed to open " << open << ". Please validate and try ";
		cout << "again.";
		exit (1);
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the arguments assigned by the user to open an output
 * file for writing.
 *
 * @param[in/out] (reference) fout: file for output of ofstream, that will be
 * opened and tested with the name given from comandline.
 * @param[in/out] (reference) s1: string used to carry the name that will be 
 * opened by fout.open.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 * @returns - exits with code of 1 should fin not open and prompts error.
 *
 ******************************************************************************/
void open_output(ofstream &fout, string &s1)
{
	fout.open ( s1, ios::out, ios::trunc );
	if (!fout)
	{
		cout << "Failed to open " << s1 << ". Please validate and try again.";
		exit (1);
	}

	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use part of the string of commands given at runtime to
 * determine which file format the pictures should be in.
 *
 * @param[in/out] (reference) string option - this will be the section from 
 * comand prompt witht he options of -o[ab] for ascii or binary.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
char output_options(string option)
{
	if ( option.compare("-oa") == 0)
		return 'a';
	if ( option.compare("-ob") == 0)
		return 'b';
	return 'n';
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use type to determine if the file needs to be read as
 * ascii or binary, and will pass to that function at determination time.
 *
 * @param[in] (reference) fin: Input file of ifstream, that will be providing 
 * data for reading.
 * @param[in] (value) type: Integer as determined by function type_check.  Used
 * to help determine read settings.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as the original picture has to be for further modification
 * @param[in] (value) position: Integer with current position needed for an
 * accurate read-in.
 *
 * @calls[out] read_ascii: Should the type be of an ascii file, fin, position
 * and the image will be passed to this function.
 * @calls[out] read_binary: Should the type be of an binary file, fin, position
 * and the image will be passed to this function.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void read_pic(ifstream &fin, image &pic, int type, int position)
{
	if (type == 2 || type == 3)
		read_ascii(fin, position, pic);
	if (type == 5 || type == 6)
		read_binary(fin, position, pic);
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will read integers from the file into the colourbands of the
 * structure image.  The function will begin at the designated position and
 * proceed until all values are filled within pic.rows and pic.cols
 *
 * @param[in] (reference) fin: Input file of ifstream, that will be providing 
 * data for reading.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as the original picture has to be for further modification
 * @param[in] (value) position: Integer with current position needed for an
 * accurate read-in.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void read_ascii(ifstream &fin, int position, image &pic)
{
	int r,g,b;
	int i= 0;
	int j = 0;
	fin.seekg(position, ios::beg);

	for (i; i<pic.cols; i++)
	{
		for(j=0; j<pic.rows; j++)
		{
			fin >> r;
			fin >> g;
			fin >> b;
			pic.redgray[j][i] = (unsigned char) r;
			pic.green[j][i] = (unsigned char) g;
			pic.blue[j][i] = (unsigned char) b;
		}
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will read thru the characters of a binary file and load them
 * into the structure image in the correct colour bands.
 *
 * @param[in] (reference) fin: Input file of ifstream, that will be providing 
 * data for reading.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as the original picture has to be for further modification
 * @param[in] (value) position: Integer with current position needed for an
 * accurate read-in.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void read_binary(ifstream &fin, int position, image &pic)
{
	unsigned char r, g, b;			// temporary variables
	int i= 0;							// counters i and j
	int j = 0;
	fin.seekg(position, ios::beg);
    for (i; i<pic.cols; i++)
    {
        for(j=0; j<pic.rows; j++)
        {

            fin.read((char *) &r, sizeof(unsigned char));
            fin.read((char *) &g, sizeof(unsigned char));
            fin.read((char *) &b, sizeof(unsigned char));
            pic.redgray[j][i] = r;
            pic.green[j][i] = g;
            pic.blue[j][i] = b;
        }

    } 

		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use the values earlier extracted to set array size.
 * Set_pic_size does this by walking thru the first instance of pic's pointers
 * in each colour band.  After setting the "top-level" of the array, the 
 * funciton walks through each row setting an array of size column.
 *
 * @param[in/out] (reference) pic:  Structure of image will be 
 * passed by reference as the original picture has to be given structure's 
 * arrays
 * @param[in] (value) row: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 * @param[in] (value) column: an integer used to help keep boundaries of the 
 * arrays that make up red/gray and/or green and blue in check so no 
 * overstepping occurs.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 * @exits - (-1) should a pointer fail to be allocated for size row or column
 *
 ******************************************************************************/
void set_pic_size(image &pic, int row, int column)
{
	int i = 0;
	int j = 0;
	pic.redgray = new (nothrow) unsigned char * [row];
	pic.green = new (nothrow) unsigned char * [row];
	pic.blue = new (nothrow) unsigned char * [row];

if (pic.redgray == NULL || pic.green == NULL || pic.blue == NULL)
{
	cout << "ERROR WITH ALLOCATING IMAGE SPACE." << endl;
	exit (-1);
}

for (i = 0; i < row ; i ++)
{
	pic.redgray[i] = new (nothrow) unsigned char [column];
	pic.green[i] = new (nothrow) unsigned char [column];
	pic.blue[i] = new (nothrow) unsigned char [column];
}
pic.rows = row;
pic.cols = column;
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use pic and pic2 to calculate adjacent pixel difference
 * in pic and then store the difference multiplied by 5 into pic2, which will
 * be used hence forth.
 *
 * @param[in/out] (reference) pic:  Structure of image will be 
 * passed by reference as the original picture has to be given structure's 
 * arrays.
 * @param[in/out] (reference) pic2: Structure of image to receive the changes
 * made within pic as the value modifications will destroy the orriginal data.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void sharpen(image &pic, image &pic2)
{
	int i = 1;
	int j = 1;
	for (i; i<pic.rows - 1; i++)//set for loop walk thru to take absolute adjacent
	{		// pixels, subtract their value and multiply by 5 to sharpen 
		for (j = 1; j<pic.cols - 1; j++)
		{
			pic2.redgray[i][j] = ( 5 * (int) pic.redgray[i][j]) - 
			(int)pic.redgray[i-1][j] - (int)pic.redgray[i][j-1] - 
			(int)pic.redgray[i][j+1] - (int)pic.redgray[i+1][j];
			pic2.green[i][j] = ( 5 * (int) pic.green[i][j]) - 
			(int)pic.green[i-1][j] - (int)pic.green[i][j-1] - 
			(int)pic.green[i][j+1] - (int)pic.green[i+1][j];
			pic2.blue[i][j] = ( 5 * (int) pic.blue[i][j]) - 
			(int)pic.blue[i-1][j] - (int)pic.blue[i][j-1] - 
			(int)pic.blue[i][j+1] - (int)pic.blue[i+1][j];
		}
	}
	for (i = 0; i < pic.rows; i++)			// do boundary copy from old to new.
	{
			pic2.redgray[i][0] = pic.redgray[i][0];
			pic2.redgray[i][pic.cols - 1] = pic.redgray[i][pic.cols - 1];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[i][pic.cols - 1] = pic.green[i][pic.cols - 1];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[i][pic.cols - 1] = pic.blue[i][pic.cols - 1];
	}
	for (i = 0; i< pic.cols; i++)			// remaining boundaries
	{
			pic2.redgray[0][i] = pic.redgray[0][i];
			pic2.redgray[pic.rows - 1][i] = pic.redgray[pic.rows - 1][i];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[pic.rows - 1][i] = pic.green[pic.rows - 1][i];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[pic.rows - 1][i] = pic.blue[pic.rows - 1][i];
	}
		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use pic and pic2 to calculate the sum of all pixels 
 * within a 3x3 grid using current coordinates as the center and manipulated
 * location.  The sum found from pic will be saved into pic2 as the data will
 * be destroyed after manipulations.
 *
 * @param[in/out] (reference) pic:  Structure of image will be 
 * passed by reference as the original picture has to be given structure's 
 * arrays.
 * @param[in/out] (reference) pic2: Structure of image to receive the changes
 * made within pic as the value modifications will destroy the orriginal data.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void smooth(image &pic, image &pic2)
{
	int newred, newgreen, newblue;
	int i = 1;
	int j = 1;
	for (i; i<(pic.rows - 1); i++)
	{		//set for loop walk thru to take absolute adjacent	
		for (j; j<(pic.cols - 1); j++)
		{	// pixels, subtract their value and multiply by 5 to sharpen 
		
			newred = (unsigned char) (((long)pic.redgray[i][j] + (int) 
			pic.redgray[i-1][j-1] + (int)pic.redgray[i-1][j] + 
			(int)pic.redgray[i-1][j+1] + (int)pic.redgray[i][j-1] + 
			(int)pic.redgray[i][j+1] + (int)pic.redgray[i+1][j-1] + 
			(int)pic.redgray[i+1][j] + (int)pic.redgray[i+1][j+1])/9.0);

			pic2.redgray[i][j] = char(newred);
			
			newgreen = (unsigned char) (((long)pic.green[i][j] + (int) 
			pic.green[i-1][j-1] + (int)pic.green[i-1][j] + 
			(int)pic.green[i-1][j+1] + (int)pic.green[i][j-1] + 
			(int)pic.green[i][j+1] + (int)pic.green[i+1][j-1] + 
			(int)pic.green[i+1][j] + (int)pic.green[i+1][j+1])/9.0);
			
			pic2.green[i][j] = char(newgreen);
		
			newblue = (unsigned char) (((long)pic.blue[i][j] + (int) 
			pic.blue[i-1][j-1] + (int)pic.blue[i-1][j] + (int)pic.blue[i-1][j+1]
			+ (int)pic.blue[i][j-1] + (int)pic.blue[i][j+1] +
			(int)pic.blue[i+1][j-1] + (int)pic.blue[i+1][j] + 
			(int)pic.blue[i+1][j+1])/9.0);

			pic2.blue[i][j] = char(newblue);

		}
		j = 0;
	}
	for (i = 0; i < pic.rows - 1; i++)		// do boundary copy from old to new.
	{
			pic2.redgray[i][0] = pic.redgray[i][0];
			pic2.redgray[i][pic.cols - 1] = pic.redgray[i][pic.cols - 1];
			pic2.green[i][0] = pic.green[i][0];
			pic2.green[i][pic.cols - 1] = pic.green[i][pic.cols - 1];
			pic2.blue[i][0] = pic.blue[i][0];
			pic2.blue[i][pic.cols - 1] = pic.blue[i][pic.cols - 1];
	}
	for (i = 0; i< pic.cols - 1; i++)			// remaining boundaries
	{
			pic2.redgray[0][i] = pic.redgray[0][i];
			pic2.redgray[pic.rows - 1][i] = pic.redgray[pic.rows - 1][i];
			pic2.green[0][i] = pic.green[0][i];
			pic2.green[pic.rows - 1][i] = pic.green[pic.rows - 1][i];
			pic2.blue[0][i] = pic.blue[0][i];
			pic2.blue[pic.rows - 1][i] = pic.blue[pic.rows - 1][i];
	}
	
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will "sample" from the top of the file given to the program to
 * determine image type (P2/P3/P5/P6) and withdraw row and column data as well
 * as capture the comment line begining with the '#'. This function will also
 * find a suitable writing position
 *
 * @param[in] fin - Input file of ifstream, that will be providing data for 
 * reading.
 *
 * @param[in/out] (reference) current:  An integer used for tellg()'s location
 * so later functions can read from the spot after color depth (255).
 *
 * @param[in/out] (reference) r: Integer to capture the number of rows listed
 * at the top of the file.
 * @param[in/out] (reference) c: Integer to capture the number of columns for
 * the picture
 * @param[out] (reference) comment: String used to collect the comment line
 * found at top of the image file.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
int type_check(ifstream &fin, int &current, int &r, int &c, string &comment)
{
	string type;
	char test[5];
	char test2[5];
	char test3[5];
	fin >> type;
	fin.ignore();
	if(fin.peek() == '#')
	getline(fin, comment, '\n') ;
	fin >> test2;
	fin >> test3;
	
	while(atoi (test) != 255)
	{
		fin >> test;
	}
	current = fin.tellg();
	r = atoi(test2);
	c = atoi(test3);
	fin.clear();
	if (type.compare ("P2") == 0)
	{
		return 2;
	}
	if (type.compare ("P3") == 0)
	{
		return 3;
	}
	if (type.compare ("P5") == 0)
	{
		return 5;
	}
	if (type.compare ("P6") == 0)
	{
		return 6;
	}
	
	return 0;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will help with reading by re-opening the file if needed
 * and also re-calculate position past 255 for reading.
 *
 * @param[in/out] fin: Input file of ifstream, that will be providing data for 
 * reading.
 * @param[in] type: Integer as determined by function type_check.  Used to help
 * determine read settings.
 * @param[in/out] (reference) position: An integer used for tellg()'s location
 * so later functions can read from the spot after color depth (255).
 * @param[in] (reference) open: String filled with the file's name given at
 * comand line.  Will be used if it is determined necissary to re-open fin.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void typed_open(ifstream &fin, int type, int &position, string &open)
{
	if (type == 5 || type == 6)
	{
		fin.close();
		fin.open(open, ios::binary);
		fin.ignore(80, '\n');
		fin.ignore(80, '\n');
		fin.ignore(80, '\n');
		fin.ignore(80, '\n');
		position = fin.tellg();
		fin.seekg(position, ios::beg);
	}
	
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will help with reading by re-opening the file if needed
 * and also re-calculate position past 255 for reading.
 *
 * @param[in/out] fout: Output file of ofstream, that will be providing data 
 * for reading.
 * @param[in] type: Integer as determined by function type_check.  Used to help
 * determine read settings.
 * @param[in/out] (reference) position: An integer used for tellg()'s location
 * so later functions can read from the spot after color depth (255).
 * @param[in] (reference) open: String filled with the file's name given at
 * comand line.  Will be used if it is determined necissary to re-open fout.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void typed_close(ofstream &fout, int type, int position, string &open)
{
	if (type == 5 || type == 6)
	{
		fout.close();
		fout.open(open, ios::binary | ios::app);
		fout.seekp(position, ios::beg);
	}
	else
	{
		fout.close();
		fout.open(open, ios::out | ios::trunc);
	}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will simply provide a usage menu to help the users navigate
 * the options and parameter requirements.  This function will be used only
 * if another integral argument checking function should fail for arguments,
 * input, output or options.
 *
 * @param[in] (reference) fin: Input file of ifstream, that will be providing 
 * data for reading.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as the original picture has to be for further modification
 * @param[in] (value) position: Integer with current position needed for an
 * accurate read-in.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void usage_menu()
{
		cout << "\nProg1 usage:\n\nC:\... Prog1.exe [options] -o[ab]";
		cout << "New_image_name Existing_image.ppm" << endl << endl;
		cout <<"Requirements: -o[ab].  -oa for ascii output or -ob for binary."
			<< "\n\n";
		cout << "Options:\nOption Code:" << setw(20) << "Option Name:" << endl;
		cout << setw(5) << "-n\t\t\t Negate" << endl;
		cout << setw(5) << "-b #\t\t\t Brighten by # (as integer)" << endl;
		cout << setw(5) << "-p\t\t\t Sharpen" << endl;
		cout << setw(5) << "-s\t\t\t Smooth" << endl;
		cout << setw(5) << "-g\t\t\t Grayscale" << endl;
		cout << setw(5) << "-C\t\t\t Contrast to average" << endl;

		cout <<"\nExamples: \n\nExample 1: Prog1.exe -oa balls ColorBalls1.ppm"
			<<"\n";
		cout << "Example 2: Prog1.exe -g -ob hearts BleedingHearts2.ppm\n";
		cout << "Example 3: Prog1.exe -b 34 -ob balloon balloons1.ppm\n";

		return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will first output image type (P2/P3/P5/P6) then the comments,
 * number of rows, and columns, then the 255 for pixel depth.  Then the 
 * function walks through the colorbands and writes them out in the order of
 * red, green, blue or grey as in the density of each pixel.
 *
 * @param[out] (reference) fout: Output file of ofstream, that will be holding 
 * the written data.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as the structure demands it.
 * @param[in] (value) magic_num: type of image which will be used to determine
 * how to write the image.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void write_ascii(ofstream &fout, image &pic, int magic_num)
{		
	int i, j;
	fout << "P"<< magic_num << endl;
	fout << "#" << "removed orriginal comments" << endl;
	fout << pic.rows << " " << pic.cols << endl << "255" << endl;
		if (magic_num == 2 || magic_num == 5)
		{
			for (i=0; i<pic.cols; i++)
			{
				for ( j = 0; j< pic.rows; j ++)
				{	
				fout << (int) pic.redgray[j][i] << endl;			
				}
				fout << endl;
				j = 0;
			}
		}
		if (magic_num == 3 || magic_num== 6)
		{
			 for (i=0; i<pic.cols; i++)
			{
				for ( j = 0; j< pic.rows; j ++)
				{

                fout << (int) pic.redgray[j][i] << " ";
                fout << (int) pic.green[j][i] << " ";
                fout << (int) pic.blue[j][i] << " ";
                fout << endl;
				}
			} 
		}
			return;
	}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function is here to write the first bit of the file information: P#, 
 * # of rows and columns and terminate so the file can be closed and re-opened
 * in a binary format.
 *
 * @param[in] (reference) fout: Onput file of ofstream, that will be providing 
 * the location for write out.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as needed by structure type.
 * @param[in] (reference) current: Integer with current position needed for an
 * accurate read-in.
 * @param[in] (value) magic_num: Integer representing type of file being 
 * written.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void write_bin_header(ofstream &fout, image &pic, int &current, int magic_num)
{
	if (magic_num == 5)
	{
		fout << "P5" << endl;
		fout << "# removed orriginal comments" << endl;
		fout << pic.rows << " " << pic.cols << endl;
		fout << "255" << endl;
	}
	if (magic_num == 6)
	{
		fout << "P6" << endl;
		fout << "# removed orriginal comments" << endl;
		fout << pic.rows << " " << pic.cols << endl;
		fout << "255" << endl;
	}
	current = fout.tellp();
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will walk through and first determine if the image is going
 * to be in grey-scale or colour.  After making that decision, the funciton
 * walks through the band(s) and outputs to the file.
 *
 * @param[in] (reference) fout: Onput file of ofstream, that will be providing 
 * the location for write out.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as needed by structure type.
 * @param[in] (reference) current: Integer with current position needed for an
 * accurate read-in.
 * @param[in] (value) magic_num: Integer representing type of file being 
 * written.
 *
 * @calls[out] none.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void write_binary(ofstream &fout, image &pic, int current, int magic_num)
{
	int i,j;
		if (magic_num == 5)
		{
			fout.seekp(current, ios::beg);
			for (i=0; i<pic.cols; i++)
			{
				for ( j = 0; j< pic.rows; j ++)
				{	
				fout.write ((char*) &pic.redgray[j][i], sizeof(char));
				}
				j = 0;
			}
		}
			if (magic_num == 6)
		{
			fout.seekp(current, ios::beg);
			for (i=0; i<pic.cols; i++)
			{
				for ( j = 0; j< pic.rows; j ++)
				{	
				fout.write ((char*) &pic.redgray[j][i], sizeof(char));
				fout.write ((char*) &pic.green[j][i], sizeof(char));
				fout.write ((char*) &pic.blue[j][i], sizeof(char));
				}
				j = 0;
			}
		}
	return;
}
/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This function will use information gathered thus far to determine a new 
 * "magic number" (P3/P4/P5/P6) based on output choices and manipulation done.
 * From there, the function will use the new number to pass the file for 
 * writing to the correct funciton.
 *
 * @param[in] (reference) fout: Onput file of ofstream, that will be providing 
 * the location for write out.
 * @param[in/out] (reference) pic:  Structure of image will be passed by 
 * reference as needed by structure type.
 * @param[in] (reference) current: Integer with current position needed for an
 * accurate read-in.
 * @param[in] (value) magic_num: Integer representing type of file being 
 * written.
 * @param[in] (value) output_option: A charachter of either 'a' or 'b' value
 * used so the function can determine a new magic number.
 * @param[in] (value) manip: A character representing one of the options of 
 * manipulation from comand line. Will be used to determine the magic number as
 * grey and contrast require a different number.
 * @param[in] (reference) open: string used for holding the name for the output
 * file as it may need to be re-opened if binary.
 *
 * @calls[out] write_ascii() - will be called should the output be in ascii
 * format.
 *
 * @calls[out] typed_close() - will be called if output is binary and new 
 * position needs to be calculated.
 * @calls[out] bin_header() - will be called if output is binary to write the
 * top of the file.
 * @calls[out] write_binary() - will be called if output is binary for writing.
 *
 * @returns - nothing.
 *
 ******************************************************************************/
void write_pic(image &pic, char output_option, char manip, ofstream &fout, int position, string &open)
{
	int magic_num = 6;	// establish magic number.
	if (output_option == 'a'&& (manip == 'g' || manip == 'c'))
		magic_num = 2;
	else if (output_option == 'a' && manip !='g')
		magic_num = 3;
	else if (output_option == 'b' && (manip == 'g' || manip == 'c'))
		magic_num = 5;
		// switch on it for binary or ascii
	if ( magic_num == 2 || magic_num == 3)
	{
		write_ascii(fout, pic, magic_num);
	}
	if ( magic_num == 5 || magic_num == 6)
	{
		typed_close(fout, magic_num, position, open);
		write_bin_header(fout, pic, position, magic_num);
		write_binary(fout, pic, position, magic_num);
	}
	return;
}