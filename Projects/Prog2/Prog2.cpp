/***************************************************************************//**
 * @file Prog2.cpp
 *
 * @mainpage Program 2 - Progectile tragectory
 *
 * @section course_section Course Information
 *
 * @section program_section Program Information
 *
 * @details
 * This program will calculate tragectory of a particle as fired between 0 and 90 degrees an 0 to 150 
 * meters per second.  The program will then compute cordinates in an (X,Y) format while updating 
 * velocities every .01 second.  At every full second (1.00 second), the program will present current 
 * cordinates until the particle comes to a rest (sometimes leaving a crater as it hit's the ground) placed
 * in terms of last full second.  After computing the program will ask for another calculation.  If input
 * is either 'y' for yes then the program will loop into another calculation.
 *
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      None 
 *
 *  @par Usage:
 *  @verbatim 
 *  c:\> prog3.exe
 *  d:\> c:\bin\prog3.exe
 *  @endverbatim
 *
 * @author Alex Wulff
 *
 * @date 10/20/11
 * 
 * @par Instructor:
 *         Dr. Schrader
 *
 * @par Course:
 *         CSC 150 � Section 4 � 3:00 pm
 *
 * @par Location:
 *         McLaury - Room 313
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 *
 * @todo check integers to insure best calculations for Pi and distance. 
 ******************************************************************************/


 

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <cmath>
#include <iomanip>
#include <cctype>

using namespace std;

int main()
{
	// Collect initial velocity and firing angle to derive horizontal and vertical velocity.
	// Then find path (plot possition on interval .01 seconds) and display for each full second.
	

	// Variables
	/*!
	 * @brief description of the structure and use.
	*/

	struct variables;
	{
		double initialVelocity;			/*!< used as composite input for velocity */
		double initialY;				/*!< initial componet of Y velocity, will be set to dynamicY */
		double initialX;				/*!< initial component of X velocity, will be set to dynamicX */
		double dynamicY;				/*!< constant velocity in Y axis coordinate, left as variable for reset and final position of Y*/
		double dynamicX;				/*!< constant velocity in X axis coordinate, left as variable for reset */
		double angle;					/*!< used as initial angle from positive x axis */
		double timeInterval;			/*!< time interval of .01 seconds. used for calculating total flight time*/
		double velocityY;				/*!< used as changing input for velocity as gravity affects*/
		double maxX, maxY;				/*!< used to save height, distance */
		char choice;					/*!< used to calculate need for a second loop */
		int counter = 0;				/*!< used to determine full second calculations */
		double timeIntervalFinal;		/*!< used to calculate final time of flight */
	}

	
	double initialVelocity, initialY, initialX, dynamicY, dynamicX, angle, timeInterval, velocityY;
	double maxX, maxY, endY;
	char choice;
	int counter = 0;
	int multiple = 1;
	double timeIntervalFinal;

	do
	{
	

	// Request input for both angle and initial velocity.  Then store.

	cout << "Enter initial velocity for projectile (0-150): ";
	cin >> initialVelocity;
	cout << endl << "Enter angle of tragectory (0-90): ";
	cin >> angle;

	// Verify conditions are valid for limitations (0 to 150 m/s and angle between 0 and 90)

	while (angle < 0 || angle > 90)
	{
		cout << endl << "Angle given is invalid.  Please re-enter: ";
		cin >> angle;
	}


	while (initialVelocity <0 || initialVelocity > 150)
	{
		cout << endl << "Initial velocity given is invalid. Please re-enter: ";
		cin >> initialVelocity;
	}

	// calculate initial x and y velocities

	initialX = initialVelocity * (cos (angle * (M_PI / 180)));
	initialY = initialVelocity * (sin (angle * (M_PI / 180)));

	/* display x and y velocities

	cout << "X axis initial and constant: " << initialX << endl;
	cout << "Y axis initial: " << initialY << endl;
	*/

	
	
	// assign dynamic Y to initial Y and same for X

	dynamicY = 0;
	dynamicX = 0;
	velocityY = initialY; 
	maxY = 0;

	// set manipulation for print out

	cout << setprecision(2);
	cout << fixed;
	cout << showpoint;
	
	// run until y becomes 0 or less

		// reset counter and timeInterval.
		counter = 0;
		timeInterval = .01;

		cout << "Time        Possition" << endl;
	
		do
	{
		dynamicX += (initialX * (.01));
		dynamicY += (velocityY * .01);
		velocityY = velocityY - (9.81 * .01);
		counter +=1;

		// velocity check as cout << "velocity is " << velocityY << endl;

		//if ( int(timeInterval * 10) % 10 == 0) should check for full seconds.

		if ( (counter * 10) % 1000 == 0)
		{
		cout << timeInterval << "    (" << dynamicX << ", " << dynamicY << ")" << endl;
		timeIntervalFinal = timeInterval;
		}
		
		if (dynamicY > maxY)
			maxY = dynamicY;

		timeInterval += 0.01;

	} while (dynamicY > 0.0);

	// calculate last full second

	cout << timeIntervalFinal << "   (" << dynamicX << ", " << dynamicY << ")" << endl;


	cout << "Distance traveled: " << dynamicX << "metres." << endl;
	cout << "Maximum height: " << maxY << "metres." << endl;
	cout << "Time in flight: " << timeInterval << "Seconds" << endl;

	// prompt for another run
	cout << "Track another projectile? ";
	
	// take input
	cin >> choice;

	// convert for capitals
	tolower (choice);

	while (choice != 'n' && choice != 'y')
	{
	cout << "Track another projectile? ";
	cin >> choice;
	tolower (choice);
	}
	

		dynamicX += (initialX * (.1 * multiple));
		dynamicY += (velocityY * (.1 * multiple));
	


}while (choice == 'y');

cout << "End of program." << endl;

	return 0;
}