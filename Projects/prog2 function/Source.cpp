#include <iostream>
#include <cmath>
#include <iomanip>
#include <cctype>

using namespace std;

int main()
{
	// Collect initial velocity and firing angle to derive horizontal and vertical velocity.
	// Then find path (plot possition on interval .01 seconds) and display for each full second.
	

	// Variables
	double initialVelocity, initialY, initialX, dynamicY, dynamicX, angle, timeInterval, velocityY;
	double maxX, maxY, endY;
	char choice;
	int counter = 0;
	int multiple = 1;
	int timeIntervalFinal;

	const double pi = 3.1415926535897932384626433832795;
	timeInterval = .1;



	do
	{

	// Request input for both angle and initial velocity.  Then store.

	cout << "Enter initial velocity for projectile (0-150): ";
	cin >> initialVelocity;
	cout << endl << "Enter angle of tragectory (0-90): ";
	cin >> angle;

	// Verify conditions are valid for limitations (0 to 150 m/s and angle between 0 and 90)

	while (angle < 0 || angle > 90)
	{
		cout << endl << "Angle given is invalid.  Please re-enter: ";
		cin >> angle;
	}


	while (initialVelocity <0 || initialVelocity > 150)
	{
		cout << endl << "Initial velocity given is invalid. Please re-enter: ";
		cin >> initialVelocity;
	}

	// calculate initial x and y velocities

	initialX = initialVelocity * (cos (angle * (pi / 180)));
	initialY = initialVelocity * (sin (angle * (pi / 180)));

	// display x and y velocities

	cout << "X axis initial and constant: " << initialX << endl;
	cout << "Y axis initial: " << initialY << endl;


	// define time interval


	
	// assign dynamic Y to initial Y and same for X

	dynamicY = 0;
	dynamicX = 0;
	velocityY = initialY; 
	maxY = 0;

	// set manipulation for print out

	/*cout << setprecision(2);
	cout << fixed;
	cout << showpoint;
	*/
	// run until y becomes 0 or less

	do
	{
		dynamicX += (initialX * (.01));
		dynamicY += (velocityY * .01);
		velocityY = velocityY - (9.81 * .01);
		counter +=1;

		// velocity check as cout << "velocity is " << velocityY << endl;

		//if ( int(timeInterval * 10) % 10 == 0) should check for full seconds.

		if ( (counter * 100) % 1000 == 0)
		{
		cout << "Coordinates at " << timeInterval << " are (" << dynamicX << "," << dynamicY << ")" << endl;
		timeIntervalFinal = timeInterval;
		}


		if (dynamicY > maxY)
			maxY = dynamicY;

		timeInterval += 0.01;

	} while (dynamicY > 0.0);

	// calculate last full second







	cout << "Distance traveled: " << dynamicX << "metres." << endl;
	cout << "Maximum height: " << maxY << "metres." << endl;
	cout << "Time in flight: " << timeIntervalFinal << "Seconds" << endl;

	// prompt for another run
	cout << "Track another projectile? ";
	
	// take input
	cin >> choice;

	// convert for capitals
	tolower (choice);

	while (choice != 'n' && choice != 'y')
	{
	cout << "Track another projectile? ";
	cin >> choice;
	tolower (choice);
	}
	

		dynamicX += (initialX * (.1 * multiple));
		dynamicY += (velocityY * (.1 * multiple));
	


}while (choice == 'y');

	return 0;
}