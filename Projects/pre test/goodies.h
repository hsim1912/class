#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;

struct node
{
	string word;
	node * next;
};
class awesomeList
{
public:

	awesomeList();
	awesomeList( awesomeList &l);
	~awesomeList();

	bool empty();
	bool insert(string &word);
	bool remove(string &word);
	void print();
	int get_size();

private:
	node * headptr;
};
class awesomeStack
{
public:

	awesomeStack();
	awesomeStack( awesomeStack &s);
	~awesomeStack();

	bool empty();
	bool push(string &word);
	bool pop(string &word);
	bool top(string &word);
	int get_size();

private:

	node *headptr;
};

class awesomeQ
{
public:

	awesomeQ();
	awesomeQ (awesomeQ &q);
	~awesomeQ();

	bool empty();
	bool enque(string &word);
	bool deque(string &word);
	bool front(string &word);
	int get_size();

private:

	node *headptr;
};