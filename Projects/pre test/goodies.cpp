#include "goodies.h"

using namespace std;

awesomeList::awesomeList()
{
	headptr = NULL;
}
awesomeList::awesomeList(awesomeList &l)
{
	node * src = NULL;
	node * dst = NULL;

	headptr = NULL;

	if (l.empty())
		return;
	src = l.headptr;

	// create headptr
	headptr = new (nothrow) node;

	if (headptr == NULL)
	{
		cout << "out of memory";
		exit (-1);
	}
	headptr->word = src->word;
	headptr->next = NULL;
	// set source and dest
	src = src->next;
	dst = headptr;
	// set other values
	while (dst->next != NULL)
	{
		dst->next = new (nothrow) node;
		dst = dst->next;
		dst->word = src->word;
		src = src->next;
	}
	

}
awesomeList::~awesomeList()
{
	node *temp = new (nothrow) node;
	// set temp to headptr to start chopping;
	temp = headptr;
	while (headptr->next != NULL)
	{
		headptr = headptr->next;
		delete [] temp;
		temp = headptr;
	}

}
bool awesomeList::empty()
{
	return headptr == NULL;
}
int awesomeList::get_size()
{
	int count = 0;
	node *temp = new (nothrow) node;
	temp = headptr;
	while (headptr->next != NULL)
	{
		temp = temp->next;

		count ++;
	}
	return count;
}
bool awesomeList::insert( string & word)
{
	node *temp, *curr, *prev;

	if (headptr->next == NULL)
	{
		temp = new (nothrow) node;
		temp->word = word;
		temp->next = NULL;
		headptr = temp;
		return 1;
	}

	else
	{
		temp = new (nothrow) node;
		curr = new (nothrow) node;
		prev = new (nothrow) node;
		// set for traversal
		temp->word = word;
		curr = prev = headptr;
	}
	if (temp->word.compare(curr->word) <= 0 )
	{
		temp->next = headptr->next;
		headptr = temp;
	}
	while (curr->next != NULL && temp->word.compare(curr->word) >= 0 )
	{
		prev = curr;
		curr = curr->next;
	}
	if (curr->next != NULL)
	{
		prev->next = temp;
		temp->next = curr;
		return 1;
	}
	if (curr->next == NULL)
	{
		curr->next = temp;
		temp->next = NULL;
		return 1;
	}
	return 0;
}
bool awesomeList::remove(string &word)
{
	node *temp, *curr, *prev;
	// set nodes;
	temp = new(nothrow) node;
	curr = new(nothrow) node;
	prev = new(nothrow) node;

	curr = prev = headptr;
	temp->word = word;

	if (headptr->next == NULL)
	{
		headptr = NULL;
		return 0;
	}

	while (curr->next != NULL && temp->word.compare(curr->word) !=0)
	{
		prev = curr;
		curr = curr->next;
	}
	if (curr->word.compare(temp->word) == 0)
	{
		prev->next = curr->next;
		delete [] curr;
		delete [] temp;
		return 1;
	}
	return 0;
}
void awesomeList::print()
{
	node * temp = new (nothrow) node;
	temp = headptr;

	while(temp->next != NULL)
	{
		cout << temp->word << endl;
		temp = temp -> next;
	}

	if (temp->next == NULL)
		cout << "List is empty!!\n";
}
awesomeStack::awesomeStack()
{
	headptr = NULL;
}
awesomeStack::awesomeStack( awesomeStack &s)
{
	node * src = NULL;
	node * dst = NULL;

	headptr = NULL;

	if (s.empty())
		return;
	src = s.headptr;

	// create headptr
	headptr = new (nothrow) node;

	if (headptr == NULL)
	{
		cout << "out of memory";
		exit (-1);
	}
	headptr->word = src->word;
	headptr->next = NULL;
	// set source and dest
	src = src->next;
	dst = headptr;
	// set other values
	while (dst->next != NULL)
	{
		dst->next = new (nothrow) node;
		dst = dst->next;
		dst->word = src->word;
		src = src->next;
	}

}
awesomeStack::~awesomeStack()
{
	node *temp = new (nothrow) node;
	// set temp to headptr to start chopping;
	temp = headptr;
	while (headptr->next != NULL)
	{
		headptr = headptr->next;
		delete [] temp;
		temp = headptr;
	}

}
bool awesomeStack::empty()
{
	return headptr == NULL;
}
bool awesomeStack::pop(string &word)
{
	node *temp = new(nothrow) node;
	temp = headptr;

	if (temp == NULL)
		return 0;

	word = temp->word;
	if (temp->next == NULL)
	{
		headptr = NULL;
		return 1;
	}
	headptr = headptr->next;
	delete temp;
	return 1;
}
bool awesomeStack::push(string &word)
{
	node *temp;
	temp = new(nothrow) node;
	
	temp->word = word;
	if(headptr != NULL)
	{
		temp->next = headptr->next;
		headptr = temp;
		return 1;
	}
	return 0;
}
bool awesomeStack::top(string &word)
{
	word = headptr->word;
	return headptr != NULL;
}
int awesomeStack::get_size()
{
	int count = 0;

	node *temp = new(nothrow) node;
	temp = headptr;

	while (temp->next != NULL)
		count++;
	return count;
}
awesomeQ::awesomeQ()
{
	headptr = NULL;
}
awesomeQ::awesomeQ(awesomeQ &q)
{
	node * src = NULL;
	node * dst = NULL;

	headptr = NULL;

	if (q.empty())
		return;
	src = q.headptr;

	// create headptr
	headptr = new (nothrow) node;

	if (headptr == NULL)
	{
		cout << "out of memory";
		exit (-1);
	}
	headptr->word = src->word;
	headptr->next = NULL;
	// set source and dest
	src = src->next;
	dst = headptr;
	// set other values
	while (dst->next != NULL)
	{
		dst->next = new (nothrow) node;
		dst = dst->next;
		dst->word = src->word;
		src = src->next;
	}
}
awesomeQ::~awesomeQ()
{
	node * temp = new(nothrow) node;
	temp = headptr;
	while (headptr->next != NULL)
	{
		headptr= headptr->next;
		delete [] temp;
		temp = headptr;
	}

}

bool awesomeQ::empty()
{
	return (headptr== NULL);
}
bool awesomeQ::deque(string &word)
{
	node *temp = new(nothrow) node;
	temp = headptr;

	if (empty())
		return 0;
	word = temp->word;
	if (temp->next == NULL)
	{
		headptr =NULL;
		return 1;
	}
	headptr = headptr->next;
	delete [] temp;
	return 1;
}
bool awesomeQ::enque(string &word)
{
	node *temp = new (nothrow) node;
	node *cycle = new (nothrow) node;
	if (temp == NULL || cycle == NULL)
		return 0;
	cycle = headptr;
	temp->word = word;
	if (headptr == NULL)
	{
		temp->next = NULL;
		headptr = temp;
		return 1;
	}
	while (cycle->next != NULL)
	{
		cycle = cycle->next;
	}
	cycle->next = temp;
	temp->next = NULL;
	return 1;
}
bool awesomeQ::front (string &word)
{
	if (headptr ==NULL)
		return 0;
	word = headptr->word;
	return 1;
}
int awesomeQ::get_size()
{
	int count = 0;
	node *temp = new (nothrow) node;
	temp = headptr;

	while (temp !=NULL && temp->next !=NULL)
		count ++;
	
	return count;
}