#include <iomanip>
#include <iostream>
#include <fstream>

using namespace std;

void rec_permute( int *p, int *used, int num, int current, int req, int req2, char choice, ofstream &fout);
int main ()
{
	int i;
	int num;
	int req, req2;
	int current=0;   
	char choice;

	req = req2 = 0;

	cout << "How many numbers: ";
	cin >> num;

	while (num > 100 || num <= 0)
	{
		cout << "Number of set must be between 1 and 100.\n";
		cin >> num;
	}

	int *p = new(nothrow) int [num];
	int *used = new (nothrow) int [num];

	for(i=0; i<num; i++)
	{
		p[i] = 0;
		used[i] = 0;
	}

	do
	{
	cout << "any number needed first? enter F or 0 for no. \n";
	cout << "any needed adjacent? A." << endl;
	cin >> choice;
	choice = tolower(choice);
	}while (choice != 'a' && choice !='f' && choice !='0');
	
	if(choice == 'f')
	{
		cout << "Enter first number";
		cin >> req;
	}
	if (choice == 'a')
	{
		cout << "Enter first number to be adjacent.";
		cin >> req;
		cout << "\nNext number?";
		cin >> req2;
		req--;
		req2--;
	}
	
	ofstream fout;
	fout.open("results.txt");

	rec_permute( p, used, num, current, req, req2, choice, fout);

	fout.close();

	return 0;
}
void rec_permute( int *p, int *used, int num, int current, int req, int req2, char choice, ofstream &fout)
{
	int i;
	if (current == num)
	{
		if (choice == 'f')
		{
		if (p[0] == req && req >= 0)
		{
			for (i = 0 ; i < num; i++)
			{
				cout << p[i] << " ";
			}
			cout << endl;
			return;
		}
		else if (req <0)
			{
			for (i = 0 ; i < num; i++)
			{
				cout << p[i] << " ";
			}
			cout << endl;
			return;
		}
	}
	if (choice == 'a')
	{
		for (i = 1; i < num; i ++)
		{
			if ((p[i] == req || p[i] == req2) && (p[i-1] == req || p[i-1] == req2))
		{
			for (i = 0 ; i < num; i++)
			{
				cout << p[i] << " ";
			}
			cout << endl;
			return;
		}
		}
	}
		if (choice == '0')
	{
		for (i = 1; i < num; i ++)
		{
			for (i = 0 ; i < num; i++)
			{
				cout << p[i] << " ";
			}
			cout << endl;
			return;
		}
		}
	}
	for (i = 0; i < num; i++)
	{
		if (used[i] == 0)
		{
			used[i] = 1;
			p[current] = i;
			rec_permute( p, used, num, current+1, req, req2, choice, fout);
			used[i] = 0;
		}
	}
	
	return;
}