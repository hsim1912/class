#include <iostream>
#include <cctype>


using namespace std;

int main()
{
	char selection;
	do
	{
		cout << "A. View all months' Highs" << endl << "B. View all months' Lows" << endl;
		cout << "C. View single month's stats" << endl << "D. View yearly high and low" << endl;
		cout << "Q. Quit program" << endl << "Option: ";

		cin >> selection;

		tolower(selection);

		if (selection != 'a' && selection != 'b' && selection !='c' && selection !='d' && selection !='q')
		{
			cout << "Invalid choice.  Please use A-D or Q." << endl;
		}
}while (selection != 'a' && selection != 'b' && selection !='c' && selection !='d' && selection !='q');

	if (selection = 'q')
	{
		cout << "Writing sorted data, then exiting program" << endl;
	}

	if (selection = 'a')
	{
		cout << "Showing all highs." << endl;
	}
	if (selection = 'b')
	{
		cout << "Showing all lows." << endl;
	}
	if (selection = 'c')
	{
		cout << "Showing all the statistics for the following month" << endl;
	}
	if (selection = 'd')
	{
		cout << "Showing yearly highs and lows" << endl;
	}
	return 0;
}