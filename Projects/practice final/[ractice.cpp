#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_NONSTDC_NO_DEPRECATE
#include <iostream>
#include <cstring>
#include <fstream>
using namespace std;
const int STR_SIZE = 81;
void sort_strings( char s[][STR_SIZE], int n );
int count_phrases( const char s[][STR_SIZE], int n );
int main( int argc, char * argv[] )
{
char text[100][STR_SIZE] = { "" };
int count = 0;
ifstream fin;
ofstream fout;
char fname[126] = "";
int num_phrases = 0;
int i;
if( argc != 3 )
{
cout << "Incorrect program usage, please give input & output files.";
cout << "\nEnding now, try again.\n\n";
return -1;
}
fin.open( argv[1] );
if( !fin )
{
cout << "Error opening input file: " << argv[1] << endl;
cout << "Please enter another file name > " ;
fin.clear( );
cin.getline( fname, 126 );
fin.open( fname );
if( !fin )
{
cout << "Error opening input file: " << fname << endl;
cout << "Ending now, try again\n\n" ;
return -1;
}
}
while( count < 100 && fin.getline( text[count], STR_SIZE ) )
count++;
fin.close( );
sort_strings( text, count );
num_phrases = count_phrases( text, count );
fout.open( argv[2] );
if( !fout )
{
cout << "Error opening output file: " << argv[2] << endl;
cout << "Ending now, try again.\n\n";
return -1;
}
cout << "Count of phrases is: " << num_phrases << endl << endl;
for( i = 0; i < count; i++ )
fout << text[i] << endl;
fout.close( );
return 0;
}
void sort_strings( char s[][STR_SIZE], int n )
{
int i, j;
char temp[STR_SIZE] = "";
for( i = 0; i < n-1; i++ )
for( j = 0; j < n - 1 - i;j++ )
{
if( stricmp( s[j] , s[j+1] ) > 0 )
{
strcpy( temp, s[j] );
strcpy( s[j], s[j+1] );
strcpy ( s[j+1], temp );
}
}
}
int count_phrases( const char s[][STR_SIZE], int n )
{
int count = 0;
int i = 0;
if( n == 0 )
return 0;
if( n == 1 )
return 1;
for( i = 1; i < n; i++ )
if( stricmp( s[i-1], s[i]) != 0 )
count++;
//count++; //counted transitions, add one for ending string value
return count;
}