#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	// output purpose of program
	cout << "Welcome to a program for identifying roots of a quadratic formula." << endl << endl << "*Please be advised that this program only calculates real roots.*" << endl << endl;
	
	//declare variables for quadratic function
	double a, b, c;
	double numerator1, numerator2, denominator;
	double root1, root2;

	//receive and check variables for non-reals
	cout << "Enter value for variable a." << endl;
	cin >> a;
	
	while (a==0)
		{
			cout << "Non-valid value for a.  This will result in non-real roots.  Please re-enter." << endl;
			cin >> a;
		}

	cout << "Enter value for variable b." << endl;
	cin >> b;



	cout << "Enter value for variable c." << endl;
	cin >> c;


		while ( (b*b)<= 4*a*c)
		{
			cout << "Non-valid value for b.  This will result in non-real roots.  Please re-enter." << endl;
			cin >> b;
		}
	//define numerator and denominator for quadratic functions

		numerator1 = -b + sqrt ( (b*b) - (4.0 * a * c) );
		numerator2 = -b - sqrt ( (b*b) - (4.0 * a * c) );
		denominator = 2 * a;

	//calculate equations

		root1 = numerator1 / denominator;
		root2 = numerator2 / denominator;

	//output roots
		
		cout << "Your first root is: \a" << root1 << endl;
		cout << "Your second root is: \a" << root2 << endl;

	return 0;
}