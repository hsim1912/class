/*-------------------------------------------------------------------
  Program: lab12_1.cpp
  Author: <Alex Wulff>
  Course: CSC 150 - <3>
  Instructor: <Scott Logan>
  Date: < 12.8.11 >
  Description:
    This program is incomplete.  Please read the instructions for
    this lab activity.


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
using namespace std;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
int searchCode(int shipping[][2], int numRows, int shipCode);


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         searchCode()
*********************************************************************/
int main()
{
    int shipCode;   // shipping code
    int shipRate;   // shipping rate
    // shipping array
    int shipping[5][2] = { {2, 10}, {3, 13}, {5, 16}, {8, 19}, {9, 20} };
	int rows = 5;

    bool keepProcessing = true;  //flag to end main loop

    // Display prompt for a shipping code
    cout << "Enter shipping code: ";

    // Get shipping code entered by the user
    
	while ( keepProcessing && cin >> shipCode )
    {
        // < Test for end of the program and display message, set flag false >
		if (shipCode == 0)
		{
			cout << "end of program.";
			exit (0);
		}


        // < Call searchCode function to find shipping rate >
         shipRate = searchCode(shipping, rows, shipCode );   


        // < Display shipping rate if found, else display message >
		 if ( shipRate > 0 )
		 {
			 cout << "Its Shipping rate is: " << shipRate << endl;
		 }

		 else cout << "Invalid shipping code." << endl;
        // < Display a new line and a prompt for another shipping code >

		 cout << "Enter shipping code: ";
    }

    return 0;
}


/********************************************************************
  Function: searchCode
  Author: MCS/SDSM&T
  Description:
    This function searches a 2-D array for a particular code stored
    in column 1, and returns corresponding rate stored in column 2.
  Parameters:  shipping: 2-D array of int
               numRows: number of rows in the array
               shipCode: int, a code being searched
  Returns      shipping rate: int, value in a second column or -1 if
                not found
 ********************************************************************/
int searchCode (int shipping[][2], int numRows,  int shipCode)
{
	int i, index;

	index = -1;
    // Search code
	for (i = 0 ; i< numRows ; i++)
	{
		if (shipping [i][0] == shipCode)
		{
			index = i;
		}
	}
	
	/* < write the search code. Look in first column for matching 
         shipCode. Return the shipping rate if found. >
    */
	if (index >= 0)
	{
		return shipping[index][1];
	}

    // Code not found in the first column
	return -1;
}
