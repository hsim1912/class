/*-------------------------------------------------------------------
  Program: lab12_2.cpp
  Author: <Fill in your name>
  Course: CSC 150 - <Fill in lab section number>
  Instructor: <Fill in lab TA's name>
  Date: < Fill in today's date>
  Description:
    This program is incomplete. The body of the main function is 
	empty. You must follow the instruccions for this lab activity.


---------------------------------------------------------------------
  Includes
---------------------------------------------------------------------*/
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;


//-------------------------------------------------------------------
// Global constants
//-------------------------------------------------------------------
const int SIZE = 100;


//-------------------------------------------------------------------
// Function Prototypes
//-------------------------------------------------------------------
void swap(int &a, int &b);
void bubbleSort(int ar[], int size );
void display(int list[], int size );


/********************************************************************
   Function:      main
   Description:   Program entry/exit point, prompts for inputs, 
                  performs calculations, displays results
   Parameters:    none
   Returns:       0
   Called by:     none
   Calls:         bubbleSort()
                  display( )
*********************************************************************/
int main( int argc, char *argv[] )
{
    // < Write main to perform the steps given in the lab description>
    // < You will ** NOT ** have to modify the functions display() or swap()
    // < You will have to complete the bubbleSort()>
	int i=0;
	int count = 0;
	int a[SIZE];
	ifstream fin;

	
	if (argc != 2 )
	{
		cout << "Incorrect number of arguments.  Use as program, inputfile.";
		exit (1);
	}

	fin.open ( argv[1] );

	if (!fin)
	{
		cout << "Error opening input file " << argv[1] << "Please verify it exists.";
		exit (2);
	}
	while (fin >> a[i])
	{
		i++;
		count++;
	}

	const int newsize = count;
	

	if (count >=100)
	{
		cout << "Data seems to be corrupt... " << count << " number of entries read in.";
		exit (3);
	}

	cout << "Original Array: " << endl;
	display( a, newsize);
	bubbleSort(a, newsize);
	cout << "Sorted Array:" << endl;
	display( a, newsize);

	return 0;
}

/********************************************************************
  Function: display
  Author: MCS/SDSM&T
  Description:   Displays values of a 1-D array. 
  Parameters:    list: array of int
                 size: size of array
  Return value:  void
  Called by:     main()
  Calls:         none
********************************************************************/
void display(int list[], int size)
{
   int i;

   for(i = 0; i < size; i++)
      cout << setw(3) << list[i];
   cout << endl;
}


/********************************************************************
  Function: swap
  Author: MCS/SDSM&T
  Description:
    This function swaps two values.
  Parameters:    a  : int, first value to be swapped with b
                 b: int, second value to be swapped with a
  Return value:  void
  Called by:     bubbleSort()
  Calls:         none
 ********************************************************************/
void swap(int &a, int &b)
{
    int t;
    // swap a and b
    t = a;
    a = b;
    b = t;
}


/********************************************************************
  Function: bubbleSort
  Author: MCS/SDSM&T
  Description:
    This function sorts a 1-D array with the bubble sort algorithm.
  Parameters:    ar  : array of int
                 size: int, size of array
  Return value:  void
  Called by:     main()
  Calls:         swap()
 ********************************************************************/
void bubbleSort(int ar[], int size)
{
    int i, j;
    bool swapFlag = false;

    for(i = 0; i < size -1; i++)
    {
		for (j=0 ; j < size - 1 ; j++)
		{
		if (ar[j] > ar[j+1])
		{
		swap (ar[j], ar[j+1]);
		swapFlag = true;
		}
		}
		if(swapFlag == false)
		{
		return;
		}
		swapFlag = false;

	}
}    //< complete the bubblesort for integers, using the swap( )
        //  function above >