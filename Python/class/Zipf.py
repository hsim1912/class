#!/usr/bin/env/python3


#********Zipf.py**********
#insert cute documentation here!
#
#
#
#
#
#
#
import sys
import os.path
import string
import re
import json


def main( argv ):
    fname ="0"  #initialized empty string for file
    success = 0
    success, fname = arg_check(fname)
    words = []
    usedWords = { '': 0}
    outputTup = ()

    if success == 1:    #x == 1 for file available at command line
        print(fname)    #for testing purposes
   
# open files
#    fin = open(fname, 'r')
    nameTwo = fname[:-3]
    nameTwo = str(nameTwo) + "wrd"
    fout = open(nameTwo, 'w')

# read in lines and close handle
    words = read_in(fname, words)
#    fin.close()
#    words = list(words)

# hash and scrub extra spot
    usedWords = hash_words (words, usedWords)
    del usedWords['']

# sort the output
    outputTup = formating(usedWords)
# print words and amounts
    output(fout, outputTup, fname)

#   todo:
#   I'm currently parsing too much and removing possessive (')'s
#   (4 per line (setw20) left justified) after header
#   print "file".csv spaced over rank, freq, r*f
#   print timing report
#




##########Non-Main Functions##########

#       check for correct usage

def arg_check(fname):
    success = 0
    if len (sys.argv) > 1 and len (sys.argv) < 3:	#correct num of args
        fname = sys.argv[1]				#get name of file
        success = exists(fname)				#test
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)					#error out
    elif len (sys.argv) >= 3:
        usage_menu()
        sys.exit(1)					#print usage, error out
    else:
        fname = input("Please enter the file to parse:")#manual entry
        success = exists(fname)				#test
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)					#error out

#       check for file existance

def exists(fname):
    if ( os.path.isfile( "./"+fname ) ):
        return 1
    else: return 2

#   Proper usage menu

def usage_menu():
    print ( "Program usage for Zipf's Law:\n\n$python3 Zipf.py" )
    print ( "[File_to_Parse]\n\n" )
    print ( "Overview:\n\nThis program will use zipf's law to parse" )
    print ( " through lines of text and present the resulting frequency" )
    print ( " in a table formated as a \".wrd\" file." )
    print ( "There is also a Comma Spaced Value file \".csv\" presented " )
    print ( "for linear mapping per Zipf's Law." )

def read_in(fname, words):
    fin = open(fname, 'r')
    line = fin.readline()
    test = line				#first line
    line = line.lower()			#make lowercase

    if (len(test) == 0):
        print ("This file is empty.  Try again")
        sys.exit(3)

    while (len(line) > 0):		#until end of file, parse punctuation
        temp = re.sub('[%s]' % re.escape(string.punctuation), '', line)
        words += str(temp).split()	#strip white space, store into list
        line = fin.readline()		#grab next line
        line = line.lower()		#make lowercase

    for i in words:
        print (i)

    fin.close()
    # sort the finished list
    return (words)

def hash_words(words, usedWords):
    for i in words:		#cycle thru words
        if i in usedWords:	#if exists
            usedWords[i] += 1	#increment count
        else:
            usedWords[i] = 1	#else insert as once
    return usedWords

def output(fout, usedWords, fname):
    count = 0			#temporary counting variables
    distinct = len(usedWords) 	#number of entries in tuple list
    outString = ""
    nextName = fname[:-3]
    nextName = nextName+"csv"
    for i in usedWords:
        count += i[1]

    # header for .wrd
    fout.write("Zipf's Law - Word Concordance:\n")
    fout.write("------------------------------\n")
    outString = "File:\t\t" + str(fname) + "\nTotal Words:\t" + str(count)
    fout.write(outString)
    outString = "\nDistinct Words:\t" + str(distinct) + "\n"
    fout.write(outString)


    # word frequency output (.wrd file)
    for i in usedWords:
        if count != int(i[1]):
            count = int(i[1])
            if int(i[1]) > 1:
                outString = "\nWords occuring " + str(count) + " times:\n"
                fout.write(outString)
            else:
                outString = "\nWords occuring once:\n"
                fout.write(outString)
        fout.write(i[0])
        fout.write('\t')

    fout.close()
    fout = open(nextName, 'w')

    # header for csv
    fout.write("Zipf's Law: rank * frequency = Const\n")
    fout.write("------------------------------------\n")
    outString = "File:\t\t" + str(fname) + "\nTotal Words:\t" + str(count)
    fout.write(outString)
    outString = "\nDistinct Words\t\t" + str(distinct) + "\n"
    fout.write(outString)

    # word frequency output (.csv file)
    fout.write("rank,\t\tfreq,\t\tr*f\n")


    # i'm thinking of making function calls here for the values of freq
    # rank, f*r as another dictionary... 

def formating(usedWords):
    temp = usedWords.items()
    temp = sorted (temp)
    stuff = []
    stuff = sorted (temp, key = spec_sort, reverse = True)
    return stuff

def spec_sort (e):return e[1]
       


if __name__=='__main__':
    main( sys.argv )
