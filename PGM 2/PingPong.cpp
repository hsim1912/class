#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

bool generateMessage( int *message, int *orig, int size );

// 128B = 32, 256B = 64, 512B = 128, 1kB = 256, 2kB = 512, 4kB = 1024
int size = 512;

int main(int argc, char *argv[]) 
{
   //int size;
   int nprocs, rank, namelen;
   int message[size];
   int orig[size];
   bool corrupted;
   double start = 0, end = 0;
   double average = 0;
   char processor_name[MPI_MAX_PROCESSOR_NAME];
   bool dev = false;

   MPI_Init(&argc, &argv);
   MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Get_processor_name(processor_name, &namelen);

   //size = atoi( argv[1] );

   //message = new int[size];
   //orig = new int[size];

   //for( int i = 0; i < size; i++ )
	//message[i] = orig[i] = i;

   //generateMessage( message, orig, size );

   for( int i = 0; i < 1000; i++ )
   {
	if( rank == 0 )
	{
	   generateMessage( message, orig, size );
	   start = MPI_Wtime();
	   MPI_Send( message, size, MPI_INT, 1, 0, MPI_COMM_WORLD );
	   MPI_Recv( message, size, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
	   end = MPI_Wtime();
	   for( int i = 0; i < size; i++ )
	   {
		if( message[i] != 1 + orig[i] )
		   corrupted = true;
	   }
	   if( ( !corrupted ) && dev )
	   {
		std::cout << end - start << std::endl;
		std::cout << ( ( size * 4 ) / (end - start) ) / 1000000 << " mBs / second" << std::endl;
	   }
	   else if( dev )
		std::cout << "Corrupted!";
	   
	   if( !corrupted )
		average += ( ( size * 4 ) / ( end - start ) );

	   corrupted = false;
	}
	else
	{
	   MPI_Recv( message, size, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE );
	   for( int i = 0; i < size; i++ )
		message[i] += 1;
	   MPI_Send( message, size, MPI_INT, 0, 0, MPI_COMM_WORLD );
	}
   }

   if( rank == 0 )
	std::cout << "Average: " << average / ( 1000 * 1000000 ) << " mB/s" << std::endl;

   MPI_Finalize();

   return 0;
}

bool generateMessage( int *message, int *orig, int size )
{
   //message = new int[size];
   //orig = new int[size];

   if( !message || !orig )
	std::cout << "Failed";

   for( int i = 0; i < size; i++ )
   {
	message[i] = i;
	orig[i] = i;
   }

   return true;
}
