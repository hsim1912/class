#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

/* User defined Macros */
#define M 10
#define N 8
#define MIN(a,b)			((a)<(b)?(a):(b))
#define BLOCK_LOW(id,p,n)	((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n)	(BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n)	\
		(BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+1)
#define BLOCK_OWNER(j,p,n)	((p)*((j)+1)-1)/(n))

/* Global Variables */
int 	thread_count;
double	A[M][N], x[N], y[N];

/* User defined Functions */
void*	Pth_Mat_Vect(void*);
void	Display_Answer();

int main (int argc, char **argv)
{
   int 		i,j;
   long		thread;
   pthread_t* 	thread_handles;

   // Test if argc is > 1
   if(argc != 2)
   {
      printf("Run by: ./pht_MV <number of threads>\n");
      exit(0);
   }

   // Get number of threads
   thread_count = strtol(argv[1], NULL, 10);

   // Allocate memory for the thread handles and partial sums
   thread_handles = malloc (thread_count*sizeof(pthread_t));

   // Initialize A, x and y
   for(i=0; i<N; i++)
   {
      x[i]=N-i;
      // y[i]=0.0;
      for(j=0; j<M; j++)
         A[j][i]=i;
   }

   // Create threads to calculate the matrix-vector product
   for(thread = 0; thread < thread_count; thread++)
      pthread_create(&thread_handles[thread], NULL,
                     Pth_Mat_Vect, (void*) thread);
   
   // Display_Answer();
   
   // Join the threads
   for(thread = 0; thread < thread_count; thread++)
      pthread_join(thread_handles[thread], NULL);

   // Display the answer
   Display_Answer();

   free(thread_handles);

   return 0;
}

void* Pth_Mat_Vect(void* rank)
{
   long my_rank = (long) rank;
   int i, j, local_m;

   // Calculate the local number of rows
   local_m = BLOCK_SIZE(my_rank, thread_count, M);
   // Find first and last row
   int my_first_row = BLOCK_LOW(my_rank, thread_count, M);
   int my_last_row = BLOCK_HIGH(my_rank, thread_count, M);

   // srand(time(NULL));

   // Calculate the Matrix-Vector product
   for(i= my_first_row; i<= my_last_row; i++)
   {
     y[i] = 0.0;
      for(j=0; j<N; j++)
         y[i] += A[i][j]*x[j];
     // usleep(1000*(rand()%10));
   }

   return NULL;
}

void Display_Answer()
{
   int i, j;

   if(N>M)
   {
      // print top rows
      for(i=0; i<(N-M)/2; i++)
      {
         printf("    ");
         for(j=0; j<N; j++)
            printf("     ");
         printf(" |%5.1f| \n", x[i]);
      }
      // print the body
      for(i=0; i<M; i++)
      {
         printf("|");
         for(j=0; j<N; j++)
            printf("%5.1f", A[i][j]);
         if(i==M/2)
            printf("| * ");
         else
            printf("|   ");
         printf("|%5.1f|", x[i+(N-M)/2]);
         if(i==M/2)
            printf(" = ");
         else
            printf("   ");
         printf("|%6.1f|\n", y[i]);
      }
      // print the bottom rows     
      for(i=1; i<=(N-M)/2; i++)
      {
         printf("    ");
         for(j=0; j<N; j++)
            printf("     ");
         printf(" |%5.1f| \n", x[M+i]);
      }
   }
   else
   {
      // print top rows
      for(i=0; i<(M-N)/2; i++)
      {
         printf("|");
            for(j=0; j<N; j++)
               printf("%5.1f", A[i][j]);
         printf("|             ");
         printf("|%6.1f|\n", y[i]);
      }
      // print the body
      for(i=0; i<N; i++)
      {
         printf("|");
            for(j=0; j<N; j++)
               printf("%5.1f", A[i+(M-N)/2][j]);
         if(i==M/2)
            printf("| * ");
         else
            printf("|   ");
         printf("|%5.1f|", x[i]);
         if(i==M/2)
            printf(" = ");
         else
            printf("   ");
         printf("|%6.1f|\n", y[i+(M-N)/2]);
      }
      // print the bottom rows     
      for(i=1; i<=(M-N)/2; i++)
      {
         printf("|");
            for(j=0; j<N; j++)
               printf("%5.1f", A[i+N][j]);
         printf("|             ");
         printf("|%6.1f|\n", y[i+N]);
      }
   }

   printf("\n");
}

