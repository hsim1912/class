#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define MIN(a,b)		((a)<(b)?(a):(b))
#define BLOCK_LOW(id,p,n)	((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n)	(BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n)	\
		(BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+1)
#define BLOCK_OWNER(j,p,n)	((p)*((j)+1)-1)/(n))

/* Global Variables */
int 	n, thread_count;
double	a, b, *p_sum;

/* User defined functions */
void* Pth_trapeziod(void* rank);
// Function to integrate
double f(double x)
{
   double y;

   y = x*x - 3*x + 2;

   return y;
};

int main (int argc, char **argv)
{
   long		thread;
   double	total_result;
   pthread_t* 	thread_handles;

   // Test if argc is > 1
   if(argc != 2)
   {
      printf("Run by: ./pht_trapeziod <number of threads>\n");
      exit(0);
   }

   // Get number of threads
   thread_count = strtol(argv[1], NULL, 10);

   // Allocate memory for the thread handles and partial sums
   thread_handles = malloc (thread_count*sizeof(pthread_t));
   p_sum = malloc (thread_count*sizeof(double));

   // Seed the Randomizer
   srand(time(NULL));

   // Ask for bounderies
   printf("Enter a, b and n\n");
   scanf("%lf %lf %d", &a, &b, &n); 

   // Create threads to calculate the partial sum
   for(thread = 0; thread < thread_count; thread++)
      pthread_create(&thread_handles[thread], NULL,
                     Pth_trapeziod, (void*) thread);
   
   // Join the threads
   for(thread = 0; thread < thread_count; thread++)
      pthread_join(thread_handles[thread], NULL);
   
   // Calculate the total sum
   for(thread = 0; thread < thread_count; thread++)
      total_result += p_sum[thread];

   printf("With n = %d trapezoids, the estimate of the integral\n", n);
   printf("from %4.1f to %4.1f = %.8e\n", a, b, total_result); 

   free(thread_handles);
   free(p_sum);
 
   return 0;
}

void* Pth_trapeziod(void* rank)
{
   long my_rank = (long) rank;
   double h, x;
   double local_a, local_b;
   int    i, local_n;

   // Calculate the width of each trapezoid
   h = (b-a)/n;
   // Calculate the local number of trapezoids
   local_n = BLOCK_SIZE(my_rank,thread_count,n);
   // Calculate the local start and end points
   local_a = a + BLOCK_LOW(my_rank,thread_count,n)*h;
   local_b = a + (1+BLOCK_HIGH(my_rank,thread_count,n))*h;

   // Add the end points
   p_sum[my_rank] = (f(local_a) + f(local_b))/2.0;
   for(i=1; i< local_n; i++)
   {
      x = local_a + i*h;
      p_sum[my_rank] += f(x);
      // Sleep for 0-9 msec
      usleep(1000*(rand()%10));
   }
   p_sum[my_rank] *= h;

   return 0;
}

