#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define MIN(a,b)		((a)<(b)?(a):(b))
#define BLOCK_LOW(id,p,n)	((id)*(n)/(p))
#define BLOCK_HIGH(id,p,n)	(BLOCK_LOW((id)+1,p,n)-1)
#define BLOCK_SIZE(id,p,n)	\
		(BLOCK_HIGH(id,p,n)-BLOCK_LOW(id,p,n)+1)
#define BLOCK_OWNER(j,p,n)	((p)*((j)+1)-1)/(n))

/* Global Variables */
int 	flag, n, thread_count;
double	a, b, total_result;

/* User defined functions */
void* Pth_trapeziod(void* rank);
// Function to integrate
double f(double x)
{
   double y;

   y = x*x - 3*x + 2;

   return y;
};

int main (int argc, char **argv)
{
   long		thread;
   pthread_t* 	thread_handles;

   // Test if argc is > 1
   if(argc != 2)
   {
      printf("Run by: ./trapeziod_flg <number of threads>\n");
      exit(0);
   }

   // Get number of threads
   thread_count = strtol(argv[1], NULL, 10);

   // Allocate memory for the thread handles
   thread_handles = malloc (thread_count*sizeof(pthread_t));

   // Ask for bounderies
   printf("Enter a, b and n\n");
   scanf("%lf %lf %d", &a, &b, &n);

   // Initialize the variables
   total_result = 0.0;
   flag = 0;

   // Create threads to calculate the partial sum
   for(thread = 0; thread < thread_count; thread++)
      pthread_create(&thread_handles[thread], NULL,
                  Pth_trapeziod, (void*) thread);
   
   // Join the threads
   for(thread = 0; thread < thread_count; thread++)
      pthread_join(thread_handles[thread], NULL);

   // Display results
   printf("\nWith n = %d trapezoids, the estimate of the integral\n", n);
   printf("from %4.1f to %4.1f = %.8e\n", a, b, total_result);
    
   free(thread_handles);  
   return 0;
}

void* Pth_trapeziod(void* rank)
{
   long my_rank = (long) rank;
   double h, x, my_sum;
   double local_a, local_b;
   int    i, local_n;

   // Calculate the width of each trapezoid
   h = (b-a)/n;
   // Calculate the local number of trapezoids
   local_n = BLOCK_SIZE(my_rank,thread_count,n);
   // Calculate the local start and end points
   local_a = a + BLOCK_LOW(my_rank,thread_count,n)*h;
   local_b = a + (1+BLOCK_HIGH(my_rank,thread_count,n))*h;

   // Add the end points
   my_sum = (f(local_a) + f(local_b))/2.0;

   for(i=1; i < local_n; i++)
   {
      x = local_a + i*h;
      my_sum += f(x);
   }

   // Critical section
   while(flag != my_rank);	// Busy Waiting
      printf("%ld: %.4f -- %.4f: %.6f\n", my_rank, local_a, local_b, my_sum*h);
      total_result += my_sum*h; 
   flag = (flag+1) % thread_count;

   return 0;
}

