#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define TRAILS 10

/* User defined functions */
double trapeziod(double, double, int);
// Function to integrate
double f(double x)
{
   double y;

   y = x*x - 3*x + 2;

   return y;
};

int main (int argc, char **argv)
{
   int i, n;
   double a, b, F, tot_time=0.0;
   struct timeval start, end;

   // Ask for bounderies
   printf("Enter a, b and n\n");
   scanf("%lf %lf %d", &a, &b, &n);

   for(i=0; i< TRAILS; i++)
   {   
      // Start timer
      gettimeofday(&start, 0);
      F = trapeziod(a, b, n);
      // Stop timer
      gettimeofday(&end, 0);
      // Add time
      tot_time += end.tv_usec - start.tv_usec;
   }

   printf("\nWith n = %d trapezoids, the estimate of the integral\n", n);
   printf("from %4.1f to %4.1f = %.8e\n", a, b, F); 
   printf("\nTimed test Sequential\n");
   printf("Elapsed time: %8.2f ms\n", tot_time/(1000*TRAILS));
 
   return 0;
}

double trapeziod(double a, double b, int n)
{
   double h, x, p_sum;
   int    i;

   // Calculate the width of each trapezoid
   h = (b-a)/n;

   // Add the end points
   p_sum = (f(a) + f(b))/2.0;
   for(i=1; i< n; i++)
   {
      x = a + i*h;
      p_sum += f(x);
   }
   p_sum *= h;

   return p_sum;
}

