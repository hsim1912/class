#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <mpi.h>


/****************************************************************************//**
Compiling instructions!
USE:
mpicc -g -Wall -lm -o Sort sort.c, 

then mpiexec -np <thread count> ./Sort <threadcount> <sort size>
Or to step through, use
mpirun -np <thread count> xterm -e gdb ./Sort.
If stepping, dont forget to set arguments
******************************************************************************/



/****************************************************************************//**
namespacing and typedefs
 ******************************************************************************/
typedef long long ll;

#define negInf (-1 * (pow (2, 63) -1))

/****************************************************************************//**

Globals, yet localized...
though never used I had these for debugging
 ******************************************************************************/

ll arrSize;


/****************************************************************************//**

Prototypes

 ******************************************************************************/

void radix(ll *localArr, ll size);




/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This is the main function.  This will set up the initial
 * array of random values, and establish sub-arrays for each thread upon which
 * to work.  The sort method will be called seperately (for clarity of code)
 * upon each thread and then the root process devides the tasks and everyone 
 * begins the merge and sort back to root.
 *
 * @param[in] argc/argv
 *
 * @returns 0 - Program ran without errors.
 * @returns 1 - An invalid argument entry.
 *
 ******************************************************************************/

int main(int argc, char **argv)
{
    ll worldSize, i, testi;
    double start, end, test;

    int id;

    ll *arr;

    worldSize = strtol(argv[1], NULL, 10); //grab num processes
    arrSize = strtol(argv[2], NULL, 10);// make array

    test = arrSize/worldSize;

    testi = 10*test;

// watch for bad users
    if ((testi % 10) != 0 )
    {
        printf("ERROR!\nNumber of randoms should be divisible by threads!\n");
        return 1;
    }

    arr = (ll*) malloc (arrSize * sizeof (ll));

    srand( time (NULL) );


    // QQ here goes my life
    for (i = 0; i < arrSize; i ++ )
        arr[i] = rand();

//INIT MPI
    MPI_Init(&argc, &argv);
    start = MPI_Wtime();

//worldSize = MPI::COMM_WORLD.Get_size();
    MPI_Comm_rank(MPI_COMM_WORLD, &id);

    ll localSize = arrSize/worldSize;

//Make negative inf so we know when done with list

//printf ("Here\n");

//Defined here for all thread housing
    ll* localArr = (ll*) malloc (arrSize* sizeof(ll));
    ll* tempArr = (ll*) malloc (arrSize * sizeof(ll));

    if (id != 0)
    {
        for (i = 0; i < arrSize; i++ )
             localArr[i] = negInf;
    }

// wait for all threads
MPI_Barrier(MPI_COMM_WORLD);

//printf ("Here from %d\n", id);

// Toss to threads
    if (id == 0)
    {
        for (i=1; i < worldSize; i++)
        {
        // spit against amount against threads
        MPI_Send(&arr[(i*localSize)], localSize, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
        }
    }
    else 
    {
        MPI_Recv(&tempArr[0], localSize, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        memcpy(localArr, tempArr, localSize * sizeof(ll));
    }
// count tracker
    int count =2;
    MPI_Barrier(MPI_COMM_WORLD);
    //printf ("Here from %d\n", id);
    // each thread does a receive except r0; they just copy into their local
    if (id == 0 )
        memcpy( localArr, arr, (localSize * sizeof(ll)));

//printf ("Here from %d\n", id);

// do sort and sends (as while until last 2 then we'll finalize and do last sort)
    MPI_Barrier(MPI_COMM_WORLD);
    // sort
    //printf("stopping\n");
    radix(localArr, localSize);

    while(count <= worldSize)
    {// only need left to right and double the sum
//printf("BACK");
    MPI_Barrier(MPI_COMM_WORLD);

    // 1, 2, 4, 8, etc. send back
    if ( (id % count) == count/2)
    {
        MPI_Send(&localArr[0] , localSize, MPI_LONG_LONG, (id - count/2), 0, MPI_COMM_WORLD);
    }

    else if (tempArr != NULL && !(id%count)) // some mod to 0
    {
//        printf("HERE\n");
        MPI_Recv(&tempArr[0], localSize, MPI_LONG_LONG, (id + count/2), 0, 
        MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        memcpy(&localArr[localSize], &tempArr[0], localSize*sizeof(ll));
        radix(localArr, localSize * 2);
    }
    // clean what memory can be done
    // if here, this thread is irrelivant to the rest of the sort process
        else 
        {
            if (tempArr && localArr)
            {
                free (tempArr);
                free (localArr);
                tempArr = localArr = NULL;
            }
        }
        count *=2;
        localSize *= 2;
    }
//recover dynamic memory for tempARR and localArr on count/4 (last not freed thread)
    if (id == count/4)
    {
        if (tempArr && localArr)
        {
            free (tempArr);
            free (localArr);
            tempArr = localArr = NULL;
        }
    }

// reduced to 0 in ln(p) time and now for output
    end = MPI_Wtime();
    MPI_Finalize();
    if (id == 0)
    {
    // output header
    //printf("FINISHED:\n");

    // output here for values of sorted list
   // for (i=0; i < arrSize; i++)
      //  printf("%llu\n", localArr[i]);

    // output time
        printf("\n...And it only took %f!", (end - start) );

    // free local, temp and arr of 0
        if (tempArr && localArr)
        { 
            free (tempArr);
            free (localArr);
            free (arr);
        }
    }
    return 0;
}


/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This is the radix sort itself.  Taking a pointer to an
 * array and it's size, we sort with a traditional radix-bucket path
 *
 * @param[in] localArr - Pointer to a sub array./
 * @param[in] size - curent size of the array
 *
 * @returns [none] local array is modified for the return.
 *
 ******************************************************************************/

void radix (ll *localArr, ll size)
{
   // printf("YEAH!\n");
    ll i;
    ll* temp = (ll*) malloc (sizeof(ll)*size);
    ll max = localArr[0];
    // initial pass will start at identity value
    ll pass = 1;
    ll toggleNum[10] = {0};

//printf("1");


    // find local max of array
    for (i = 0; i < size; i++)
    {
        if (localArr[i] > max)
            max = localArr[i];
    }
//    printf("no");
 
    while (max / pass > 0)
    {
        // get number to toggle over
        for (i = 0; i < size; i++)
            toggleNum[localArr[i] / pass % 10]++;
        // total to this point, lol could have done global sum
        for (i = 1; i < 10; i++)
            toggleNum[i] += toggleNum[i - 1];
        // counting backwards, place into temp 
        for (i = size - 1; i >= 0; i--)
            temp[--toggleNum[localArr[i] / pass % 10]] = localArr[i];
        //write ?(partial)+sort, lol regex
        for (i = 0; i < size; i++)
            localArr[i] = temp[i];
        //go to next pass
        pass *= 10;
       // number to toggle at values 0-9
       for (i = 0; i < 10; i ++)
           toggleNum[i] = 0;
   }

   free(temp);
   temp = NULL;
//printf("done");
}
