#include <iostream>
#include <fstream>
#include <iomanip>
#include <pthread.h>
#include <time.h>
#include <ctime>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>


/****************************************************************************//**
Compiling instructions!
g++ -o Monte2 -g monte2.C -lgomp -fopenmp
 ******************************************************************************/


/****************************************************************************//**
namespacing and typedefs
 ******************************************************************************/


using namespace std;
typedef long long ll;



/****************************************************************************//**

Globals

 ******************************************************************************/

clock_t runTime = clock() + 15 * CLOCKS_PER_SEC;
ll numPhilosophers;
bool *sticks;
void pick_up_sticks(int id);
ll eats[5]= {0};


/******************************************************************************
 * @brief - Base structure to hold philosopher info and maintain required 
 * timings for eats and thinks.  Will be modified in *.C to make deadlocks and
 * equal eats.
 *****************************************************************************/

class philosopher
{
    public:
        philosopher();
        philosopher(int count);
        ~philosopher();

         int id;                // which philosopher
         bool left;             // true if has left stick
         bool right;            // true if has right stick
         bool get_left();       // returns true and modifies left
         bool get_right();      // returns true and modifies right
         bool hungry;           // true if philosopher needs to eat

         bool eat(int extra);   // true for success, returns sticks to false
         void think();          // thinking stall time

};




/*!**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: The following include the methods needed to keep 
 * philosophers eating, thinking and making decisions based on environmental 
 * factors such as stick availability.
 *
 ******************************************************************************/

philosopher::philosopher()
{
    
    id = 0;
    // All philosophers begin not hungry and without any sticks
    right = left = hungry = 0;
}


philosopher::philosopher(int count)
{
    // initialize with specified id, made human readable
    id = count + 1;
    // All philosophers begin not hungry and without any sticks
    right = left = hungry =0;
}

philosopher::~philosopher()
{
    // nothing dynamic to clean up
}

bool philosopher::get_left()
{
    // check if left is free
    // move to far end if first philosopher

    if (this->id == 0)
	// if free
        if (!sticks[numPhilosophers - 1])
        {
            this->left = 1;
            sticks[numPhilosophers - 1] = 1;
            return 1;
        }
	// if free
    if (!sticks[(this->id - 1)])
    {
        this->left = 1;
        sticks[this->id - 1] = 1;
        return 1;    
    }

    return 0;
}

bool philosopher::get_right()
{
    // check if right is free
    // no moves needed, right is same as philosopher id

	// if not used
    if (!sticks[this->id])
    {
        sticks[this->id] = 1;
        this->right = 1;
        return 1;
    }
    return 0;
}

void philosopher::think()
{
    // seed random
    srand(time(NULL));

    // time to stall between eats, double between 0 and 5
    double waitFactor = ((rand()/RAND_MAX) * (rand() % 6));

    clock_t start = clock() + waitFactor * CLOCKS_PER_SEC;
    while (!this->hungry && clock() < start)
    {}
    // burnt calories thinking so hard
    this->hungry = 1;
    usleep(4);

}


bool philosopher::eat( int extra)
{
// must be hungry to eat.
    if (this->hungry)
    {
	// need both sticks
        if (this->left && this->right)
        {
            // I presume the philosopher eats here... 
            usleep(20000 + extra);
            this->hungry = 0;
            this->left = 0;
            this->right = 0;
            pick_up_sticks(this->id);
/* Can be removed to watch philosophers become happy
            cout << "Philosopher " << this->id<< " eats now!" << endl;
*/
            eats[this->id-1] ++;
            return 1;
        }
/* Can be removed to watch philosophers complain
        else cout << "Philosopher " << this->id<< " can't eat!" << endl;
*/
    }
// else we fail
return 0;
}

/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: Though not technically a class method, it was decided that
 * keeping a class essential function for maintaining the relationship between
 * sticks and philosophers in check was important to keep in the header.
 ******************************************************************************/
void pick_up_sticks( int id )
{
    if (id == 0)	// watch for left stick
        sticks[numPhilosophers - 1] = 0; // instantly clean
    else sticks[id - 1] = 0;             // that's cool!

    sticks[id] = 0;
}
