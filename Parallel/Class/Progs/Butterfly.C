#include <iostream>
#include <ctime>
#include <mpi.h>
#include <cstdlib>
#include <unistd.h>

typedef long long ll;

using namespace std;

bool lock = 0;

int main(int argc, char **argv)
{

int worldSize, rank;

ll value, oldValue, nextValue;
ll num, sum, i, count;


MPI_Init (&argc, &argv);
MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
MPI_Comm_rank(MPI_COMM_WORLD, &rank);

srand(time(NULL));

value = (rand() - rand()) % 51;
oldValue = value;
nextValue = 0;


count = 2;

MPI_Barrier(MPI_COMM_WORLD);

// non-2^n set of processes?


while (count <= worldSize)
{// only need left to right and double the sum
MPI_Barrier(MPI_COMM_WORLD);

    if (rank % count >= count/2)
    {
        cout << rank << " " << rank - count/2 <<endl;
        MPI_Send(&value, 1, MPI_LONG_LONG, (rank - count/2), 0, MPI_COMM_WORLD);
        MPI_Recv(&nextValue, 1, MPI_LONG_LONG, (rank - count/2), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        value = oldValue = (nextValue + oldValue);
    }

    if (rank % count < count/2)
    {
        cout << rank << " " << rank + count/2 <<endl;
        MPI_Send(&value, 1, MPI_LONG_LONG, (rank + count/2), 0, MPI_COMM_WORLD);
        MPI_Recv(&nextValue, 1, MPI_LONG_LONG, (rank + count/2), 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        value = oldValue = (nextValue + oldValue);
    }

count *=2;
}
MPI_Barrier(MPI_COMM_WORLD);

if (rank == 0){
    for (i = 0; i < worldSize; i++){
        MPI_Recv(&nextValue, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	cout << i << " " << nextValue << endl;
    }
}

MPI_Finalize();

return 0;
}
