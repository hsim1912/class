#include "philosopher.h"



/****************************************************************************//**
Compiling instructions!
g++ -o Philosopher2 -g philosopher.C -lpthread
 ******************************************************************************/



/****************************************************************************//**

Globals, yet localized...

 ******************************************************************************/

// time tracker per philosopher (fair semaphore)
clock_t ptimes[5];	// no bathroom pun here
pthread_mutex_t flag;


/****************************************************************************//**

Prototypes

 ******************************************************************************/

bool gen_sticks( int num);
void* work ( void *which );



/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This is the main function.  This will set up and allocate
 * the stick orientation in relation to philosophers.  This will also spawn
 * philosophers, put them to work / lunch, and output a result for total eats
 * of each philosopher to P_deadlock.txt
 *
 * @param[in] none.
 *
 * @returns 0 - Program ran without errors
 * @returns 1 - An invalid stick allocation.  Might want to check system memory
 *
 ******************************************************************************/


int main( int argc, char **argv )
{
int i;

// seed random
srand(time(NULL));

int numSticks = 5;
ofstream fout("P_deadlock.txt");
pthread_t* pHandles;
numPhilosophers = 5;


if (!gen_sticks(numSticks))
{
    cout << "ERROR ALLOCATING NUMBER OF STICKS!" << endl;
    exit(1);
}
/*
else cout << "SUCCESS WITH STICKS!\n";
*/

// create handle array
pHandles = new (nothrow) pthread_t [int(numPhilosophers)];
// intialize flag
pthread_mutex_init(&flag, NULL);

// generate philosophers... if only it was this easy
for (i = 0; i < numPhilosophers; i++)
    pthread_create ( &pHandles[i], NULL, work, (void *) i );

// asemble the team
for (i = 0; i < numPhilosophers; i++)
    pthread_join ( pHandles[i], NULL );

// loose humanitarian-of-the-year award
free(pHandles);
pthread_mutex_destroy(&flag);

// print statistical eating times...
    for (i = 0; i < 5; i ++)
        fout << "Philosopher " << i << " ate: " << eats[i] << "times!\n";

    fout << "In " << runTime/CLOCKS_PER_SEC << " seconds.";
    fout.close();

return 0;
}


/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This is the verify correct usage function.  This only
 * exists for safety as I do not trust my users... sorry. The function will
 * verify that the arguments are of the correct type and amount, or will
 * flag main to exit.  This version, to make sure that things are fair, uses
 * a semaphore to ensure everyone gets to eat.
 *
 * @param[in] which - Void pointer to id of philosopher.  Helps with seating
 * arrangements and bill, I'm sure, with coorresponding eats.
 *
 * @returns none.
 *
 *****************************************************************************/

void* work ( void *which )
{
    int id = int(long(which));
    philosopher thinker(id);
    // put the philosopher to work, if not hungry
    do
    {
        if(!thinker.hungry)
            thinker.think();
        else
        {
            pthread_mutex_lock(&flag);
                thinker.get_left();
                thinker.get_right();
                thinker.eat(0);
            pthread_mutex_unlock(&flag);
        }


    }while ( clock() < runTime );

}



/****************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description: This is the verify correct allocation for the global
 * value sticks which is a boolean array that represents the number of sticks
 * at the table (used - 1, free - 0).
 *
 * @param[in] num - The amount of sticks to generate.
 *
 * @returns 1 - Sticks are ready and clean
 * @returns 0 - Likely someone has made a booboo.  The sticks can't be made.
 *
 ******************************************************************************/

bool gen_sticks( int num)
{
    int i = 0;
    sticks = new (nothrow) bool [num];
    for (i; i< num; i ++)
        sticks[i] = 0;	// all free to use

    // safety first
    if (!sticks)
        return 0;
    return 1;
}
