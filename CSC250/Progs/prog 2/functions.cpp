#include "mylist.h"





bool argcheck(int count)
{
	if (count != 3)	// usage check
	{
		usage_menu();
		return false;
	}
	return true;
}

bool open_dict(string &arg, fstream &dictionary)
{
	char open[20];
	strcpy(open, arg.c_str());
	dictionary.open( open, ios::in | ios:: out | ios::app );
	if (!dictionary)
	{
		cout << "Dictionary not found or not openable.  Please validate and" 
			<< "try again." << endl;
		return false;
	}	
	return true;
}

bool open_readme(string &read, ifstream &readme, ofstream &readme_new)
{
	char open[20];
	int size = read.size();
	strcpy(open, read.c_str());
	readme.open(open, ios::in);
	if (!readme)
	{
		cout << "Error opening readme. Please check disk space and access." 
			<< endl;
		return false;
	}
	read.erase (size - 4);
	read.append (".new");
	strcpy(open, read.c_str());
	readme_new.open( open, ios::out | ios::trunc);
	if (!readme_new)
	{
		cout << "Error opening new readme. Please check disk space and access."
			<< endl;
		return false;
	}
	return true;
}
void read_dict(fstream &dict, mylist wordlist[])
{
	int index;
	string word;
	char first_letter[1];
	char punct;
	while (!dict.eof())
	{
		dict >> word;
		word.copy(first_letter,1);
		first_letter[1] = tolower(first_letter[1]);
		word.replace (1,1,first_letter);
		index = int (first_letter[1]) - int ('a');
		
		wordlist[index].insert(word);
	}


	return;
}
void usage_menu()
{
	cout << "Program usage:" << endl << endl;
	cout << "C:\... prog2.exe [dictionary file] [readme file]" << endl << endl;
	cout << "Both files must be in txt extensions" << endl;
	return;
}