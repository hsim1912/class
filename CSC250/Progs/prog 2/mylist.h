#include <string>
#include <cctype>
#include <iomanip>
#include <iostream>
#include <fstream>


using namespace std;

bool argcheck(int count);
bool open_dict(string &arg, fstream &dictionary);
bool open_readme(string &read, ifstream &readme, ofstream &readme_new);
void read_dict(fstream &dict, mylist wordlist[]);
void usage_menu();




class mylist
{
	public:
		mylist();
		~mylist();

		bool insert(string word);
		bool remove(string word);
		bool find( string word );
		bool retrieve( string &word, int loc );
		bool is_empty();
		int get_count();

	private:

		struct node
		{
			string word;
			node *next;
		};
		node *headptr;
};
