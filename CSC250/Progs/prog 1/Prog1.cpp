/*************************************************************************//**
 * @file 
 *
 * @brief This file contains the main function which will take comands from
 * the comand line and interpret them as arguments for image manipulation.
 *
 * @mainpage Program 1 - Image Manipulation
 * 
 * @section course_section CSC 250
 *
 * @author Alex Wulff
 * 
 * @date February 10, 2012
 * 
 * @par Professor: 
 *         Dr. Roger Schrader
 * 
 * @par Course: 
 *         CSC 250 - M001 - 10:00am
 * 
 * @par Location: 
 *         McLaury - 213
 *
 * @section program_section Program Information 
 * 
 * @details This program uses .ppm and .pgm images to transfer between ascii
 * and binary file types, while providing some image manipulation features, 
 * such as brightening, adjusting contrast, making grey-scale, sharpening,
 * smoothing or negating an image through dynamically alocated arrays holding
 * a value between 0 and 255 for concentration.
 *
 * Brigten - increases the saturation of all pixels by a specified amount.
 *
 * Contrast - converts to grey-scale if needed, then adjusts between lowest
 * saturation level and highest to re-balance the image.
 *
 * Grey-Scale - converts the image to black and white using the formula:
 * grey = 30% red + 60% green + 10%blue
 *
 * Sharpen - use the 4 adjacent pixel saturations against the current one as:
 * new = 5* (current - adj1 - adj2 - adj3 - adj4)
 *
 * Smooth - sum all surrounding pixel saturations and devide by 9 for center's
 * new value.
 *
 * Negate - Takes a saturation level and reverses it against the maximum
 * of 255
 *
 * @section compile_section Compiling and Usage 
 *
 * @par Compiling Instructions: 
 *      None 
 * 
 * @par Usage: 
   @verbatim  
   c:\> Prog1.exe [option] -o[a/b] new_base_name image.ppm 
   d:\> c:\bin\Prog3.exe 
   @endverbatim 
 *
 * @section todo_bugs_modification_section Todo, Bugs, and Modifications
 * 
 * @bug Smooth - runs correct calculations, but is writing over itself yeilding
 * an image that has little resemblence to the original with seemingly random
 * colour patterns throughout.
 * @bug Sharpen - runs calculations correctly, but forces the output file to
 * write over itself, creating three seperate partial images within the one.
 * 
 * @todo Use full to keep track of what is left to be done.
 * @todo You should list items that you are currently working on or have
 * to be worked on.
 * 
 * @par Modifications and Development Timeline: 
   @verbatim 
   Date          Modification 
   ------------  -------------------------------------------------------------- 
   Feb 6, 2012	   Started frame work (created the header, function and source)
				   Then created includes and started open and arg check functions
   Feb 9, 2012	   Began work on read and write functions, created mathematical
				   expressions for brighten, negate, sharpen, smooth, and grey.
   Feb 10, 2012	   Started testing on read and write.  Errors with both ascii and
				   Binary file types.
   Feb 13-16, 2012 Isolated issues with ascii readings.  Binary is still quite 
				   off.  Worked on modulation and design from main and within 
				   function.cpp
   Feb 19, 2012    Found error and resolved binary problem.  Created simple
				   read/ write solution to test functions.  Found output was
				   not resetting position. Created typed_close/open to resolve.
   Feb 20-22, 2012 Sharpen, smoothing still not working propperly.
   Feb 24, 2012	   Finished Documentation. Submitted as complete.
   @endverbatim
 *
 *****************************************************************************/
#include "function.h"

/**************************************************************************//**
 * @author Alex Wulff
 *
 * @par Description:
 * This is the starting point to the program.  It will take commands from the 
 * command promt or an IDE's debug functions.  The program will use these to 
 * determine how to create an image using one specified by the user at run
 * time.  The arguments are verified and the images are read in to a custom
 * structure for manipulation (should any be specified). If manipulation does 
 * take place, the reformed image will be output in the manner specified at 
 * runtime  ( either binary or ascii ) and the correct extention will be put
 * to the output file name.
 *
 * @param[in] argc - the number of arguments from the command prompt.
 * @param[in] argv - a 2d array of characters containing the arguments.
 *
 * @returns 0 - upon completion or incorrect use of arguments.
 * @returns 1 - if input/ output file can not be opened or found.
 * @returns 2 - file declared for reading of invalid type.
 * @returns 3 - should a manipulation choice not be correct and not caught
 *              durring arg_check.
 * @returns -1 - if the arrays dynamically allocated do not create successfully
 *
 *****************************************************************************/
int main( int argc, char **argv )// set up args for receiving command line args
{					//Declare Variables
int args = argc;
string s1, s2, s3;
string comment = "#EMPTY STRING FOR COMMENT CAPTURE";
string open = argv[args - 1];
image pic, pic2;
char manip_option = 'a';
char output_option;
int row, column, brighten, type, position;
int colordepth = 255;
ifstream fin; ofstream fout;
open_input(fin, open);
			// test number of args. If insubstantial report and exit.
			// Then interpret args, classify and send back for output.
if (!arg_check(args, argv, s1, s2, s3))
{
	exit (0);
}			// call function for determining if type
type = type_check(fin, position, row, column, comment);
					// validate type and re-open if nessiscary
typed_open(fin, type, position, open);
if (type == 0)
{
	cout << "File type not supported, or mis-read.  Validate and try again.";
	exit (2);
}			// Allocate space as dimensions are known and test.
set_pic_size(pic, row, column);
set_pic_size(pic2, row, column);
			//read in pic
read_pic(fin, pic, type, position);
if (argc == 5)
	manip_option = manip_options(argv[argc - 4 ]);
if (argc == 6)
{			// Assign code for output
	manip_option = manip_options(argv[argc - 5]);			
	brighten = atoi(argv[argc - 4]);
}
output_option = output_options(argv[argc - 3]);
			// call function for manipulation and assign 0 for brighten if dne
if (argc != 6)
	brighten = 0;
//pic2 = pic;			// duplicate for output
manip(pic, pic2, manip_option, brighten);
			// modify extension and open output file
open = argv[argc - 2];
if ( manip_option == 'g' || manip_option == 'c')
	open = open + ".pgm";
else open = open + ".ppm";
open_output(fout, open);
typed_close(fout, type, position, open);
			// call function for write out of processed image
if (manip_option == 'p' || manip_option == 's')
	write_pic(pic2, output_option, manip_option, fout, position, open);
else write_pic(pic, output_option, manip_option, fout, position, open);
cout << "COMPLETE!" << endl;
	clear_array(pic);				// free array space
	clear_array(pic2);
	return 0;		//end
}