/*************************************************************************//**
 * @file 
 *
 * @brief this file contains the basic information for function.cpp and 
 * prog1.cpp.  It should be included with function.cpp and prog1.cpp.
 ****************************************************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include "string.h"

/*******************************************************************************
 *             Constant Variables, defines, structures and Enums
 ******************************************************************************/
#ifndef  __FUNCTION__H__
#define __FUNCTION__H__
typedef unsigned char pixel;

struct image
{
    int rows;
    int cols;
	pixel **redgray;	// handles red channel or grayscale
    pixel **green;
    pixel **blue;
};


#endif


using namespace std;

/*******************************************************************************
 *                         Function Prototypes
 ******************************************************************************/

bool arg_check( int arguments, char *argv[], string &s1, string &s2, string &s3);
void bin_header(ofstream &fout, image &pic, int magic_num, int &current);
void bright(image &pic, int brighten);
void clear_array (image & pic);
void contrast(image &pic);
void manip(image &pic, image &pic2, char option, int brighten);
char manip_options(string option);
void negate( image &pic );
void open_input(ifstream &fin, string &string1 );
void open_output(ofstream &fout, string &s1);
char output_options(string option);
void read_ascii(ifstream &fin, int position, image &pic);
void read_binary(ifstream &fin, int position, image &pic);
void read_pic(ifstream &fin, image &pic, int type, int position);
void read_type(ifstream file, int type, int row, int column,int position,image pic);
void set_pic_size(image &pic, int row, int column);
void sharpen(image &pic, image &pic2);
void smooth(image &pic, image &pic2);
int type_check(ifstream &fin, int &current, int &r, int &c, string &comment);
void typed_open(ifstream &fin, int type, int &position, string &open);
void typed_close(ofstream &foutin, int type, int position, string &open);
void usage_menu();
void write_pic(image &pic, char output_option, char manip, ofstream &fout, int position, string &open);
void write_ascii(ofstream &fout, image &pic, int magic_num);
void write_binary(ofstream &fout, image &pic, int current, int magic_num);
void write_bin_header(ofstream &fout, image &pic, int &current, int magic_num);