/*--------------------------------------------------------------------
  Program:	     lab1.cpp 
  Author:        Prof. Val Manes
  Course:        CSC 150 - Spring 2010
  Instructor:    MCS Instructor
  Date Written:  9/7/08
  Description:  
     This program asks some personal questions of the user and
     displays a summary of the information.  Provides a bit of 
     error checking of age and gpa data.
     This program demonstrates good formatting.
  Modification:
    Date                   Comment
  -------- -----------------------------------------------------------
  08/11/09 Last question asked to user was replaced.  Code modified by
           Dr. Manuel Penaloza   


----------------------------------------------------------------------
  Includes
----------------------------------------------------------------------*/
#include <iostream>
#include <cstring>
using namespace std;


//--------------------------------------------------------------------
//  Global constants
//--------------------------------------------------------------------
#define MAX 80


/*********************************************************************
  Function:      main
  Description:   Asks user for name, age, hobbie; displays summary
  Parameters:    none
  Returns:       0
  Called by:     none
  Calls:         none
**********************************************************************/
int main ( )
{
   char fname[MAX] = "";   //store first and last name separately
   char lname[MAX] = "";
   char hobby[MAX] = "";  // store main hobbie
   int age = 0;

   //get inputs from user
   cout << "What is your first name?  > ";
   cin >> fname;

   cout << "What is your last name?   > ";
   cin >> lname;

   cout << "What is your present age? > ";
   cin >> age;
   if ( age < 16 )
      cout << "Aren't you a bit young to be in college?" << endl << endl;

   cout << "What is your main hobby?  > ";
   cin >> hobby;

   cout << endl << endl;

   //display summary of information
   cout << "Hello, " << fname << " " << lname << "!" << endl;
   cout << "You told me you are " << age << " years old, and "
        << "your main hobby is " << hobby << "." << endl;

   cout << endl;
   cout << "Hope you have fun in CSC 150!" << endl << endl;

   return 0;
}