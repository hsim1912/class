#!/usr/bin/env/python3
#ALEX READ THIS, shows you how to format. Took me a while to find it.
#>>> '{:<30}'.format('left aligned')
#'left aligned                  '
#>>> '{:>30}'.format('right aligned')
#'                 right aligned'
#>>> '{:^30}'.format('centered')
#'           centered           '
#>>> '{:*^30}'.format('centered')  # use '*' as a fill char
#'***********centered***********'

#********Zipf.py**********
#insert cute documentation here!

'''
File - Zipf.py
Brief - Python source to generate a sorted frequency analysis of words from a given text file, and using Zipf's law, build a CSV (comma-seperated-valued) file that contains the calculations needed to generate a linear function of 



'''

import sys
import os.path
import string
import re
import time

def main( argv ):
    fname ="0"  #initialized empty string for file
    success = 0 #boolean flag for file success
    total = 0	#number of words read in 

    success, fname = arg_check(fname)
    words = []
    usedWords = { '': 0}
    outputTup = ()

   
# open files
#    fin = open(fname, 'r')
    nameTwo = fname[:-3]
    nameTwo = str(nameTwo) + "wrd"
    fout = open(nameTwo, 'w')

# read in lines and close handle
    words = read_in(fname, words)
    total = len(words)

# hash and scrub extra spot
    usedWords = hash_words (words, usedWords)
    del usedWords['']

# sort the output
    outputTup = formating(usedWords)
# print words and amounts
    output(fout, outputTup, fname, total)

#   todo:
#   I'm currently parsing too much and removing possessive (')'s
#   (4 per line (setw20) left justified) after header
#   print "file".csv spaced over rank, freq, r*f
#   print timing report
#




##########Non-Main Functions##########

#       check for correct usage

def arg_check(fname):
    success = 0
    if len (sys.argv) > 1 and len (sys.argv) < 3:	#correct num of args
        fname = sys.argv[1]				#get name of file
        success = exists(fname)				#test
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)					#error out
    elif len (sys.argv) >= 3:
        usage_menu()
        sys.exit(1)					#print usage, error out
    else:
        fname = input("Please enter the file to parse:")#manual entry
        success = exists(fname)				#test
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)					#error out

#       check for file existance

def exists(fname):
    if ( os.path.isfile( "./"+fname ) ):
        return 1
    else: return 2

#   Proper usage menu

def usage_menu():
    print ( "Program usage for Zipf's Law:\n\n$python3 Zipf.py" )
    print ( "[File_to_Parse]\n\n" )
    print ( "Overview:\n\nThis program will use zipf's law to parse" )
    print ( " through lines of text and present the resulting frequency" )
    print ( " in a table formated as a \".wrd\" file." )
    print ( "There is also a Comma Spaced Value file \".csv\" presented " )
    print ( "for linear mapping per Zipf's Law." )

def read_in(fname, words):
    fin = open(fname, 'r')
    punct = string.punctuation
    punct = punct.replace("'", "")
    hyphen = '-' 			#hyphens sometimes don't get spaced
    line = fin.readline()
    test = line				#first line
    line = line.lower()			#make lowercase

    if (len(test) == 0):
        print ("This file is empty.  Try again")
        sys.exit(3)

    while (len(line) > 0):		#until end of file, parse punctuation
        temp = re.sub('[%s]' % re.escape(punct), '', line)
        temp = re.sub(r'\'(?!t\')(?!s\')', '', temp)
        words += str(temp).split()	#strip white space, store into list
        line = fin.readline()		#grab next line
        line = line.lower()		#make lowercase

#    for i in words:
#        print (i)

    fin.close()
    # sort the finished list
    return (words)

def hash_words(words, usedWords):
    for i in words:		#cycle thru words
        if i in usedWords:	#if exists
            usedWords[i] += 1	#increment count
        else:
            usedWords[i] = 1	#else insert as once
    return usedWords

def output(fout, usedWords, fname, total):
    count = 0			#temporary counting variables
    freq = 0			#used for partial values in csv
    distinct = len(usedWords) 	#number of entries in tuple list
    ranked = 0			#will hold tupples for ranked printing
    times = 1			#counter for rank, start at 1 (first word)
    previous = 0
    temp = ()


    nextName = fname[:-3]
    nextName = nextName+"csv"
    for i in usedWords:
        count += i[1]

    # header for .wrd
    fout.write("Zipf's Law - Word Concordance:\n")
    fout.write("------------------------------\n")
    print ("File:\t\t" + str(fname) + "\nTotal Words:\t" + str(count), \
    file = fout)
    print ("\nDistinct Words:\t" + str(distinct) + "\n", file = fout)

 
    # word frequency output (.wrd file)
    for i in usedWords:
        if count != int(i[1]):
            count = int(i[1])
#            temp = times, count	#counts previous entries (after count changes)
#            ranked.append(temp)

            times = 1		#equally ranked strings (counts times of 
				# ranked occurance) reset here

            if count > 1:	#singular output vs plural headding
                print ("\n\nWords occuring " + str(count) + " times:\n",\
                file = fout)
            else:
                print ("\n\nWords occuring once:\n", file = fout)

        fout.write('{:<20}'.format(i[0]))	#formatted output
        if times % 5 == 0 and times != 0: fout.write('\n') 
        times += 1		#increment counter for newline/ occurence of rank
	#IT IS HERE^ FIX THE COUNTING FOR TIMES AND MAKE SURE IT IS NOT TOO MUCH!!

	#need last ranked value pair 
#    temp = times, int(usedWords[-1][1])
#    ranked.append(temp)



    fout.close()
    fout = open(nextName, 'w')




    # header for csv
    fout.write("Zipf's Law: rank * frequency = Const\n")
    fout.write("------------------------------------\n")
    print ("File:\t\t" + str(fname) + "\nTotal Words:\t" + str(total),\
    file = fout)
    print ("Distinct Words\t\t" + str(distinct) + "\n", file = fout)

    # word frequency output (.csv file)
    fout.write("rank, freq, r*f\n")
    count = 0				# reset count  
    rank = 0				# rank counter
    freq = usedWords[0][1]		# first case setup for highest frequency
    for r in usedWords:
        if freq != r[1]:		# new word for table
            if prev == freq:		# no duplicates from before
                prev = r[1]		# manual update
            freq = r[1]                 # update the new frequency
            ranked = float(ranked/rank) # update printing rank

            print (str(ranked) + "," + str(prev) + "," + str(ranked * prev),\
            file = fout)

            ranked = 0
            rank = 1

        else:
            count += 1			# track which word
            ranked += count		# update with sum of each word at this rank
            rank += 1                   # track number at this rank
            prev = r[1]                 # accepted frequency for these

    fout.close()		#end file write


def formating(usedWords):
    temp = usedWords.items()
    temp = sorted (temp)
    stuff = []
    stuff = sorted (temp, key = spec_sort, reverse = True)
    return stuff

def spec_sort (e):return e[1]
       


if __name__=='__main__':
    main( sys.argv )
