#!/usr/bin/env/python3


#********Zipf.py**********
#
#
#
#
#
#
#
#
import sys
import os.path
import string
import re
import json


def main( argv ):
    fname ="0"  #initialized empty string for file
    success = 0
    success, fname = arg_check(fname)
    words = []
    usedWords = { '': 0}


    if success == 1:    #x == 1 for file available at command line
        print(fname)    #for testing purposes
   
# open files
#    fin = open(fname, 'r')
    nameTwo =  len(fname) - 3
    nameTwo = str(nameTwo) + "wrd"
    fout = open(nameTwo, 'w')

# read in lines and close handle
    words = read_in(fname, words)
#    fin.close()
    words = words.sort()
#    words = list(words)

# hash and scrub extra spot
#    usedWords = hash_words (words, usedWords)
    del usedWords['']
# print words and amounts
    output(fout, usedWords)

#   check for correct usage
#   if not runtime command for file to parse, prompt and initialize
#   open file
#   extract words && parse words into dictionary
#   Write to "file".wrd with count of occurance in alphabetical order\
#   (4 per line (setw20) left justified) with header
#   print "file".csv spaced over rank, freq, r*f
#   print timing report
#




#       check for correct usage

def arg_check(fname):
    success = 0
    if len (sys.argv) > 1 and len (sys.argv) < 3:
        fname = sys.argv[1]
        success = exists(fname)
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)
    elif len (sys.argv) >= 3:
        usage_menu()
        sys.exit(1)
    else:
        fname = input("Please enter the file to parse:")
        success = exists(fname)
        if success == 1:
            return 1,fname
        else:
            print ( fname, " File not found or corrupt\n\nExiting")
            sys.exit(2)

#       check for file existance

def exists(fname):
    if ( os.path.isfile( "./"+fname ) ):
        return 1
    else: return 2

#   Proper usage menu

def usage_menu():
    print ( "Program usage for Zipf's Law:\n\n$python3 Zipf.py" )
    print ( "[File_to_Parse]\n\n" )
    print ( "Overview:\n\nThis program will use zipf's law to parse" )
    print ( " through lines of text and present the resulting frequency" )
    print ( " in a table formated as a \".wrd\" file." )
    print ( "There is also a Comma Spaced Value file \".csv\" presented " )
    print ( "for linear mapping per Zipf's Law." )

def read_in(fname, words):
    fin = open(fname, 'r')
    table = string.punctuation
    line = fin.readline()
    test = line

    if (len(test) == 0):
        print ("This file is empty.  Try again")
        sys.exit(3)

    while (len(line) > 0):
#        temp = re.sub( '[%s]', re.escape(table), ' ', line )
        temp = re.sub('[%s]' % re.escape(string.punctuation), '', line)
        words += str(temp).split()
        line = fin.readline()

    temp = re.sub('[%s]' % re.escape(string.punctuation), '', line)
    words += str(temp).split()

    for i in words:
        print (i)

    fin.close()
    # sort the finished list
    return (words)

def hash_words(words, usedWords):
    for i in words:
        if i in words:
            usedWords[i] += 1
        else:
            usedWords[i] = 1
    return usedWords

def output(fout, usedWords):
    json.dump(usedWords,fout)    


if __name__=='__main__':
    main( sys.argv )
