#|
                    ***** WATERJUG.LSP *****

Water Jug Problem: Given M gallon and N gallon jugs, and a faucet with
running water, try to get W gallons in one of the jugs.

Calls BFS/DFS search routines in SEARCH.LSP.

Author: John M. Weiss, Ph.
Written 3/20/13 for CSC461 PL class.

Modifications:

|#

;--------------------------------------------------------------------------

; global variables
(defvar *M*)                ; size of jug 1
(defvar *N*)                ; size of jug 2
(defvar *W*)                ; desired gallons

; Water Jug Problem - driver routine for general M x N problem
(defun WaterJug (m n w)
    ; check for unsolvable problem instance
    (when (> w (max m n)) (return-from WaterJug "No solution!"))

    ; initialize global vars
    (setq *M* m)
    (setq *N* n)
    (setq *W* w)

    ; Solve water jug problem using BFS
    (format t "BFS graph search: ")
    (format t "~A~%" (bfs (start-state)))

    ; Solve water jug problem using DFS
    (format t "DFS graph search: ")
    (format t "~A~%"  (dfs (start-state)))

    ; suppress printing NIL upon return to interpreter
    (values)
)

; Define the start state (both buckets empty).
(defun start-state () '(0 0))

; Have we reached the goal? (W gallons in the N gallon jug)
(defun goal-state? (state)
     (or (= (car state) *W*) (= (cadr state) *W*))
)

; Generate-successors returns a list of successors to the current state.
(defun generate-successors (state)
    ; define local variables
    (let ( j1 j2 (jug1 (car state)) (jug2 (cadr state)) (succs nil) )

        ; empty jug 1
        (when (> jug1 0) (setq succs (cons (list 0 jug2) succs)))

        ; empty jug 2
        (when (> jug2 0) (setq succs (cons (list jug1 0) succs)))

        ; fill jug 1
        (when (< jug1 *M*) (setq succs (cons (list *M* jug2) succs)))

        ; fill jug 2
        (when (< jug2 *N*) (setq succs (cons (list jug1 *N*) succs)))

        ; pour 1 into 2
        (cond
            ((and (> jug1 0) (< jug2 *N*))
                (setq j2 (+ jug1 jug2))
                (when (> j2 *N*) (setq j2 *N*))
                (setq j1 (- jug1 (- j2 jug2)))
                (setq succs (cons (list j1 j2) succs))
            )
        )

        ; pour 2 into 1
        (cond
            ((and (> jug2 0) (< jug1 *M*))
                (setq j1 (+ jug1 jug2))
                (when (> j1 *M*) (setq j1 *M*))
                (setq j2 (- jug2 (- j1 jug1)))
                (setq succs (cons (list j1 j2) succs))
            )
        )

        ; return list of successors, without duplicates
        (remove-duplicates succs :test #'equal)
    )
)

;--------------------------------------------------------------------------

; load BFS/DFS routines automatically:
(load 'search)

; WaterJug problem
(cond
    ; get command-line arguments for M,N,W, and solve WaterJug problem using DFS and BFS
    ((= (length *args*) 3)
        (setf *M* (parse-integer (car *args*)) *N* (parse-integer (cadr *args*)) *W* (parse-integer (caddr *args*)))
        (WaterJug *M* *N* *W*)
    )
    (t
        ; print out a usage message:
        (format t "~%Generalized WaterJug Problem~%")
        (format t   "----------- -------- -------~%")
        (format t "Usage: (WaterJug M N W)~%")
        (format t "       M - size of jug 1~%")
        (format t "       N - size of jug 2~%")
        (format t "       W - desired amount (in either jug)~%")
    )
)


;--------------------------------------------------------------------------

#|
                    ***** SAMPLE RUN *****

> (load 'waterjug)

Generalized WaterJug Problem
----------- -------- -------
Usage: (WaterJug M N W)
       M - size of jug 1
       N - size of jug 2
       W - desired amount in jug 2

> (WaterJug 3 5 4)
BFS graph search: ((0 0) (0 5) (3 2) (0 2) (2 0) (2 5) (3 4))
DFS graph search: ((0 0) (3 0) (0 3) (3 3) (1 5) (1 0) (0 1) (3 1) (0 4))

|#
