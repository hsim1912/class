#|Joe Manke
CSC461 Program 2
Missionaries and Cannibals Problem
Due 4/11/13

This program solves the missionaries and cannibals problem, for variable
numbers of missionaries and cannibals, and a canoe that can hold 2 people. 
The user enters a number of missionaries and a number of cannibals when
calling the function m-c, and the program performs a depth-first search to
find a solution, then prints that solution.
|#

#|global constant declarations|#
(defvar *m*) ;number of missionaries
(defvar *c*) ;number of cannibals

#|all possible moves:
    move 2 missionaries
    move 1 missionary
    move 1 missionary & 1 cannibal
    move 1 cannibal
    move 2 cannibals    
    third element always 1 because canoe always switches banks|#
;moves from left to right in preferred order
(defvar *l_action* '((2 0 1) (1 1 1) (0 2 1) (1 0 1) (0 1 1) ))
;moves from right to left in preferred order
(defvar *r_action* '((0 1 1) (1 0 1) (0 2 1) (1 1 1) (2 0 1)))

(defun m-c (m c)
    "Main function. Takes user input values for m and c, checks if solvable,
    calls dfs if it is solvable."
    
    (format t "~d Missionaries and ~d Cannibals ~%~%" m c)
    ;make sure problem is solvable
    (when (> c m) (return-from m-c "No solution."))
    
    ;initialize variables
    (setf *m* m *c* c)
    (let ((left '(0 0 1));people on left bank, 1 = boat on left
          (right '(0 0 0));people on right bank
          (move '(0 0 0)));initial prev_move for dfs
        (setf (car left) m (cadr left) c)
        ;do depth-first search to find a solution
        (dfs left right move))
)

(defun dfs (left right prev_move)
    "Performs a depth-first search to find the shortest path to a solution"   
    
    ;check for base cases
    ;;all missionaries and cannibals on right bank
    (when (equal 'left '(0 0 0)) 
        (print_move left right prev_move)
        (return-from dfs t))
    ;;more cannibals than missionaries on either bank
    (when (< (car left) (cadr left)) (return-from dfs nil)) 
    (when (< (car right) (cadr right)) (return-from dfs nil))
    
    ;catch other errors
    ;;negative numbers
    (when (minusp (car left)) (return-from dfs nil))
    (when (minusp (cadr left)) (return-from dfs nil))
    (when (minusp (car right)) (return-from dfs nil))
    (when (minusp (cadr right)) (return-from dfs nil))
    ;;too many cannibals
    (when (> (+ (cadr left) (cadr right)) *c*) (return-from dfs nil))
    ;;too many missionaries
    (when (> (+ (car left) (car right)) *m*) (return-from dfs nil))
    
    (print_move left right prev_move) 
    
    ;make recursive call
    (setf i -1)
    (loop (incf i)
        (when (> i 4) (return-from dfs nil)); if no move worked, return nil
        ;if boat is on left side
        (if (= (third left) 1)
            (let ((move (nth i *l_action*)))
                (when (not (equal 'move 'prev_move));prevents flip-flopping
                    
                    (format t "preset: left ~d ~s ~s ~s~%" i left right move)
(if (eq left right) (print "fucknuggets 1"))          
                    (setf left (lst- left move))
;apparently left and right point to the same object after setting left
(if (eq left right) (print "fucknuggets 2"))
                    (setf right (lst+ right move))
;but don't anymore after setting right
(if (eq left right) (print "fucknuggets 3"))                    
                    (format t "set:    left ~d ~s ~s ~s~%" i left right move)
                    
                    (when (dfs left right move)
                        (print_move left right move)
                        (return-from dfs t))
(if (eq left right) (print "fucknuggets 4"))
                    ;undo move if it doesn't work
                    (setf left (lst+ left move) right (lst- left right))
(if (eq left right) (print "fucknuggets 5"))                    
                    (format t "reset:  left ~d ~s ~s ~s~%~%" i left right move)
                )
            )
            ;else boat is on right side
            (let ((move (nth i *r_action*)))
                (when (not (equal 'move 'prev_move));prevents flip-flopping
                    
                    (format t "preset: right ~d ~s ~s ~s~%" i left right move)
                    (setf left (lst+ left move))
                    (setf right (lst- right move))
                    
                    (format t "set:    right ~d ~s ~s ~s~%" i left right move)
                    
                    (when (dfs left right move)
                        (print_move left right move)
                        (return-from dfs t))
                    ;undo move if it doesn't work
                    (setf left (lst- left move) right (lst+ left right))
                    
                    (format t "reset:  right ~d ~s ~s ~s~%~%"
                        i left right move)
                )
		    )   
        )
    )
)

(defun lst+ (lst1 lst2)
    "Adds corresponding elements of two lists of length 3"
    (let ((sum_lst '(1 1 1))) 
        (setf (first sum_lst) (+ (first lst1) (first lst2))
            (second sum_lst) (+ (second lst1) (second lst2)) 
            (third sum_lst) (+ (third lst1) (third lst2)))
        (format t "~s + ~s = ~s~%" lst1 lst2 sum_lst)
        sum_lst)
)

(defun lst- (lst1 lst2)
    "Subtracts corresponding elements of two lists of length 3"
    (let ((dif_lst '(2 2 2)))
        (setf (first dif_lst) (- (first lst1) (first lst2))
            (second dif_lst) (- (second lst1) (second lst2)) 
            (third dif_lst) (- (third lst1) (third lst2)))
        (format t "~s - ~s = ~s~%" lst1 lst2 dif_lst)
        dif_lst)
)

(defun print_move (left right move)
    "Prints the last move made"
    (let* ((side (if (equal (third left) 1) (string "left") (string "right")))
           (prev_side (if (equal side "left") (string "right")
            (string "left"))))
        (format t "Move: ") 
        (if (equal right '(0 0 0))
            (format t "      starting position~%")
            (format t "      move ~d M, ~d C from ~a to ~a~%"
                (car move) (cadr move) prev_side side))
        (format t "Left side:  ~d M, ~d C~%" (car left) (cadr left))
        (format t "Right side: ~d M, ~d C~%" (car right) (cadr right))
        (format t "Canoe is on ~a bank ~%~%" side))
)
