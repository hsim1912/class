#|
Title - Missionaries and Cannabals on a river bank.
Authors - Alex Wulff & William Armstrong.
Date - 4.3.2013
Description - Suppose you have m Missionaries and c Cannabals on a
river bank.  Suppose as well that you have a canoe to transport them safely
across the river infested with mating hippoepotamus (yes, that's right
it isn't multiple rivers after all) but only at a maximum capacity of two
occupants per trip.  To further complicate the seemingly innocuous task of
travel, Cannabals can't be trusted.  Indeed when more of these swarmy Cannabals
group together on a bank than there are Missionaries to preach them into
submission, lunch is bound to occur, as, afterall, Cannabals are vendictive
and non-forgiving of preaching speech...

So how then do we move Cannabals and Missionaries in extremis to the other
bank?
|#

;needed here for initialize the 
(defstruct shore side cannibals missionaries)
(make-shore :side "left" :cannibals 3 :missionaries 3)
(make-shore :side "right" :cannibals 0 :missionaries 0)
(make-shore :side "doneleft" :cannibals 0 :missionaries 0)
(make-shore :side "doneright" :cannibals 0 :missionaries 0)


; initial
(setf turns 'left)

;define other side
(defun other (turns)
    (cond 
        ((eq turns 'left) (return-from other 'right))   ;NEED TO HUSH THESE
        ((eq turns 'right) (return-from other 'left))   ;
    )
)

(defvar *cycles* -1)
(defvar *check* -1)

;init *beenthere*
(defvar *beenthere* ())

; left moves need to illustrate progress to goal
; ideal is missionary moves before cannibals
(defvar *leftmoves* '((2 0) (1 0) (1 1) (0 2) (0 1)))
; right moves should contain only singular travels as more will result
; in repeats.  Should watch for cannabal moves as priority
(defvar *rightmoves* '((0 1) (1 1) (1 0) (0 2) (2 0)))



; this either needs to be recursive or an outter function needs to be implimented
; for recursive eval |
;AWOLNATION          V

(defun sail!? ()
  (display)
  (cond
    ;complete?
    ((and (equalp 'doneleft 'left) 
      (equalp 'doneright 'right)) 
    (complete)); end and (complete)
    ;successors?
    ((and (eq nil (successors)) (eq 1 (length *beenthere*))) (invalid 2)) 
  )
  (return-from sail!? nil)
)

;WHAZUP!?!?
(defun display ()
    (format t "Left Missionaries: ~S~%" (get 'left 'missionaries) )
    (format t "Left cannibals: ~S~%"(get 'left 'cannibals) )
    (format t "Right Missionaries: ~S~%"(get 'right 'missionaries) )
    (format t "Right Cannabals: ~S~%"(get 'right 'cannibals) )
    (format t "Current Side ~S~%" turns)
)


; will param for input for specific
; will also track states with refference to left
(defun init_specific_case (m c)

    (setf turns 'left)

    (setf (get turns 'cannibals) c)
    (setf (get turns 'missionaries) m)

    (setf (get (other turns) 'cannibals) 0)
    (setf (get (other turns) 'missionaries) 0)

;set initial as been there
    (setf *beenthere* (list (get 'left 'missionaries) (get 'left 'cannibals)
    (get 'right 'missionaries) (get 'right 'cannibals)))
)

;tracks states
(defun track ()
        (setf *beenthere*
            (append *beenthere* (list (get 'left 'missionaries) 
            (get 'left 'cannibals) (get 'right 'missionaries)
            (get 'right 'cannibals))))
)
;removes states in recursive
(defun backtrack ()
  (cond 
    ((eq () *beenthere*) (setf *beenthere* nil)) ; nothing if list is empty
        ((eq (length *beenthere*) 1 ) (invalid 2)) 
        ; if last to remove would leave an empty list, call the test false
        (t (cons (car l) (remove-last (cdr l)))))
; needs to assign from this!!!
)


;entry of program
(defun menu ()
;reset beenthere
;(break)
    (setf *beenthere* ())

    (print "Welcome to the Cannabals and Missionaries Problem!")
    (print "Please enter 1 for a default case of 3 Cannabals and 3 Missionaries")
    (print "Or enter 2 for a specific case specified by you!")
    (print "If you're lame and would like to leave, 9 is an option...")

    (setf choice (read))
    (cond 

        ((eq choice 1) (init_specific_case '3 '3) (end_state '3 '3) 
        (print "SET 3 and 3") (sail!?))

        ((eq choice 2) 
            (print "How many Cannabals?")
            (setf c (read))
                (if (not (numberp  c))  (let (print "bad choice, restarting") (menu)))
            
            (print "How many Missionaries?")
            (setf m (read))
                (if ( not (numberp m)) (let (print "bad choice, restarting") (menu)))
            
            (init_specific_case m c) (end_state m c)
        )

        ((eq choice 9) (print "You're missing out!") (exit))
        (t (menu))

    )
)

;initialization for first pass
(defun m-c (m c)
	;initialize the case
    (init_specific_case m c) 
	;set end state
    (end_state m c)
	;begin migration if m >=c
    (cond 
        (( < m c) (invalid 1 m c))
    
    ((eq turns 'left) (sail!?))

    )
	; when complete restart with menu
    (menu)
)

(defun invalid (option m c)
    ; if option is 1, then this is at initial test
    (cond
        ((eq option 1)
        (format t "~d Missionaries and ~d Cannabals is an invalid solution as " m c)
        (format t "atleast one person has been eaten.")); end 1
        ; no solution
        ((t)  
        (format t "~d Missionaries and ~d Cannabals is an invalid solution as " m c)
        (format t "has no solution!"))
    )
    (menu)
)

(defun end_state (m c)

    (setf (get 'doneright 'cannibals) c)
    (setf (get 'doneright 'missionaries) m)

    (setf (get 'doneleft 'cannibals) 0)
    (setf (get 'doneleft 'missionaries) 0)
)

(defun complete ()
    (format t "COMPLETED~%")
    (output)
)

(defun output ()

	;header / start
	;NOTE Place right in end of each list such that it is lM lC rM rC can do compare
	; off of front 2 sill, but for prints this could be useful

    (format t "Left Side    Right Side    Boat Landed on    last move~%")
    (format t "---------    ----------    --------------    ---------~%")
    (format t "~d M, ~d C     0 M, 0 C         Left       starting position~%" 
    (first (first *beenthere*)) (second (first *beenthere*) ))

	;cycle thru places initialized count at 2 as first has been done
    (setf cycle 2)
	;break at (), as is atom and else is list, might have to log right side too
    (loop while (not ( atom (nth cycle *beenthere*))) do
    (print "\n")
    (format t "~d M, ~d C     0 M, 0 C         Left       starting position~%"  
    (first (nth cycle *beenthere*)) (second (nth cycle *beenthere*) )
    (fourth (nth cycle *beenthere*)) (fifth (nth cycle *beenthere*)))
    )
)
;subtracts the cooresponding fields from each list
(defun minuslist (move which)
    (setf (get which 'missionaries) (- (get which 'missionaries) (first move))) 
    (setf (get which 'cannibals) (- (get which 'cannibals) (second move)))
)

;adds the cooresponding fields to the list
(defun plusList (move which)
    (setf (get which 'missionaries) (+ (get which 'missionaries) (first move)))
    (setf (get which 'cannibals) (+ (get which 'cannibals) (second move)))
)


(defun successors () 
;debug
  (print "test")
  (cond
    ; if state as entered is not valid, return
    ((not (valid) ) (return-from successors nil))
    ((eq turns 'left)
    (loop for moves in *leftmoves* do 
        ; lunch can't happen so...
        ;check next state
        (minuslist moves turns); to update people
        (pluslist moves (other turns)); update other bank 
      (cond
        ; switch who's turn, track and go again
        ((valid)
          (setf turns (other turns))
          (track)
          (sail!?)
          (backtrack)
          (setf turns (other turns));reverse as backing out
          (pluslist moves turns); repair last move
          (minuslist moves (other turns))
          (print "backedup"))); end test
        ; else, restore state and don't track
        (t (setf turns (other turns));reverse as backing out
        (pluslist moves turns); repair last move
        (minuslist moves (other turns)));end else
    )
  );end left successors
    (t (let;else
      (loop for moves in *rightmoves* do ; Right bank
        (minuslist moves turns); to update people
        (pluslist moves (other turns)); update other bank 
        (cond
          ; check resulting change for validity
          (( valid)
            (setf turns (other turns))
            (track)
            (sail!?)
            (backtrack)
            (setf turns (other turns));reverse as backing out
            (pluslist moves turns); repair last move
            (minuslist moves (other turns))
            (print "backedup"))); end test
          ; else, restore state and don't track
          (t (setf turns (other turns));reverse as backing out
          (pluslist moves turns); repair last move
          (minuslist moves (other turns)));end else
      ))
    );end right
  );end cond
  (return-from successors nil)
)

; remove some duplicates
(defun duplicates ()
; Modify this for repeat chex
; repeat of earlier state?
  (cond
    ((> (length *beenthere*) 1)
        ; loop over entries in *beenthere* looking for any repeats
      (loop for test in *beenthere* do; move to 0
          (cond
            ; break if we pass size of been there (no inf loops)
            ((not (eq nil (position (list (get 'left 'missionaries) 
            (get 'left 'cannibals)) *beenthere* :test #'equalp))) 
            (return-from duplicates t)); end repeat check
            ; else
            ;((t) (return nil))
          ); end complete /repeat checks
        )
      (return-from duplicates nil)
    )
  )
)

;Logic Chex
(defun valid ()
;Short circuit on number of m to c
    (cond 
      ((and (<= (get 'left 'missionaries) (get 'left 'cannibals))
          ( <= (get 'right 'missionaries) (get 'right 'cannibals))
	  (>= (get turns 'missionaries) -1) (> (get turns 'cannibals) -1)
          (>= (get (other turns) 'missionaries) -1) 
          (>= (get (other turns) 'cannibals) -1);end and
        (not (duplicates))) (return-from valid t));end repeat check
      ;FALSE
      (t  (return-from valid nil))
    )
)
